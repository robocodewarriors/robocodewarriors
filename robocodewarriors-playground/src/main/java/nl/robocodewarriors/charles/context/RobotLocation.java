package nl.robocodewarriors.charles.context;

import org.springframework.stereotype.Component;

@Component
public class RobotLocation {

    private String robotLocation = System.getProperty("executor.robot.location");

    public String getRobotLocation() {
        return robotLocation;
    }

    public void setRobotLocation(String robotLocation) {
        this.robotLocation = robotLocation;
    }
}
