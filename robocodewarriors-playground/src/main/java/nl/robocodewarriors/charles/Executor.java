package nl.robocodewarriors.charles;

import com.google.common.io.Files;

import nl.robocodewarriors.charles.context.RobotLocation;
import nl.robocodewarriors.charles.robotbuilder.RobotCompileException;
import nl.robocodewarriors.charles.robotbuilder.RobotCompiler;
import nl.robocodewarriors.utils.FileManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;

import robocode.control.BattleSpecification;
import robocode.control.BattlefieldSpecification;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

class Executor {

    private static final Logger logger = LoggerFactory.getLogger(Executor.class);

    public static final int FIELD_WIDTH = 800;
    public static final int FIELD_HEIGHT = 600;

    public static void main(String... args) throws RobotCompileException, IOException {
        ApplicationContext context = new AnnotationConfigApplicationContext("nl.robocodewarriors");

        RobotCompiler robotCompiler = context.getBean(RobotCompiler.class);

        RobotLocation robotLocation = context.getBean(RobotLocation.class);
        FileManager fileManager = context.getBean(FileManager.class);

        createBossFiles(robotLocation, fileManager);

        robotCompiler.compileRobotProject();

        String[] robotNames = new String[]{
                System.getProperty("controlling.robot"),
                System.getProperty("opponent.robot")
        };

        RobocodeEngine robocodeEngine = context.getBean(RobocodeEngine.class);
        robocodeEngine.setVisible(true);

        RobotSpecification[] selectedRobots = robocodeEngine.getLocalRepository(StringUtils.arrayToDelimitedString(robotNames, ","));

        BattleSpecification specification = new BattleSpecification(1, new BattlefieldSpecification(FIELD_WIDTH, FIELD_HEIGHT), selectedRobots);
        robocodeEngine.runBattle(specification, true);
    }

    private static void createBossFiles(RobotLocation robotLocation, FileManager fileManager) throws IOException {
        File sourceFile = fileManager.getFile("src/main/resources/NeuralRobotBoss.txt");
        File propertiesFile = fileManager.getFile("src/main/resources/NeuralRobotBoss.properties");
        String sourceLocation = robotLocation.getRobotLocation() + "/src/main/java/nl/robocodewarriors/robot/neuralrobot/impl/NeuralRobotBoss.java";
        String propertiesLocation = robotLocation.getRobotLocation() + "/src/main/resources/nl/robocodewarriors/robot/neuralrobot/impl/NeuralRobotBoss.properties";

        File targetSourceFile = fileManager.getFile(sourceLocation);
        File targetPropertiesFile = fileManager.getFile(propertiesLocation);

        fileManager.createDirectories(targetSourceFile);
        fileManager.createDirectories(targetPropertiesFile);

        targetPropertiesFile.createNewFile();
        targetSourceFile.createNewFile();

        Files.copy(sourceFile, targetSourceFile);
        Files.copy(propertiesFile, targetPropertiesFile);
    }
}
