package nl.robocodewarriors.charles.robotbuilder;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import nl.robocodewarriors.charles.context.RobotLocation;
import nl.robocodewarriors.utils.FileManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class RobotBuilder {

    private final String robotImplLocation;
    private final String robotPropertiesLocation;
    private final Configuration freemarker;
    private final FileManager fileManager;

    @Autowired
    public RobotBuilder(Configuration freemarker, FileManager fileManager, RobotLocation robotLocation) {
        this.freemarker = freemarker;
        this.fileManager = fileManager;
        this.robotImplLocation = robotLocation.getRobotLocation() + "/src/main/java/nl/robocodewarriors/robot/neuralrobot/impl/";
        this.robotPropertiesLocation = robotLocation.getRobotLocation() + "/src/main/resources/nl/robocodewarriors/robot/neuralrobot/impl/";
    }

    public void buildRobotsSources(String[] networks) throws IOException, TemplateException {
        for (int identifier = 0; identifier < networks.length; identifier++) {
            Map<String, Object> variables = new HashMap<String, Object>();
            variables.put("identifier", identifier);

            variables.put("json", networks[identifier].replace("\"", "\\\""));

            createClassFile(identifier, variables);
            createPropertiesFile(identifier, variables);
        }
    }

    private void createClassFile(int identifier, Map<String, Object> variables) throws TemplateException, IOException, FileNotFoundException {
        createFileFromTemplate(robotImplLocation + "NeuralRobotImpl" + identifier + ".java", "templates/NeuralRobotImpl.java.ftl", variables);
    }

    private void createPropertiesFile(int identifier, Map<String, Object> variables) throws IOException, FileNotFoundException, TemplateException {
        createFileFromTemplate(robotPropertiesLocation + "NeuralRobotImpl" + identifier + ".properties", "templates/NeuralRobotImpl.properties.ftl", variables);
    }

    private void createFileFromTemplate(String fileLocation, String templateLocation, Map<String, Object> variables) throws IOException, TemplateException {
        File file = fileManager.getFile(fileLocation);
        fileManager.createDirectories(file);
        file.createNewFile();

        Template template = freemarker.getTemplate(templateLocation);

        template.process(variables, fileManager.getOutputStreamWriter(file));
    }
}
