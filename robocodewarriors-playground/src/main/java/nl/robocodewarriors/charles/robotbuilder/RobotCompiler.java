package nl.robocodewarriors.charles.robotbuilder;

import com.google.common.io.ByteStreams;
import nl.robocodewarriors.charles.context.RobotLocation;
import nl.robocodewarriors.utils.FileManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class RobotCompiler {

    private final File executionDir;
    private final FileManager fileManager;
    private final Runtime runtime;
    private final RobotLocation robotLocation;
    private static final String ROBOT_NAME = "neural-robot.jar";
    private static final String JAR_LOCATION = "/target/" + ROBOT_NAME;

    @Autowired
    public RobotCompiler(@Qualifier("robocodeExecutionDirectory") File executionDir, RobotLocation robotLocation, FileManager fileManager, Runtime runtime) {
        this.executionDir = executionDir;
        this.fileManager = fileManager;
        this.runtime = runtime;
        this.robotLocation = robotLocation;
    }

    public File compileRobotProject() throws RobotCompileException {
        try {
            File robotFile = fileManager.getFile(robotLocation.getRobotLocation());
            String executable = (System.getProperty("os.name").toLowerCase().contains("win")) ? "mvn.bat" : "mvn";
            Process process = runtime.exec(new String[]{executable, "install", "-DskipTests"}, null, robotFile);

            String output = new String(ByteStreams.toByteArray(process.getInputStream()));
            if (output.contains("BUILD FAILURE")) {
                throw new RobotCompileException("Running 'mvn clean install' failed");
            }

            File compiledRobot = fileManager.getFile(executionDir, "robots" + File.separator + ROBOT_NAME);
            return fileManager.move(fileManager.getFile(robotLocation.getRobotLocation() + JAR_LOCATION), compiledRobot);
        } catch (IOException e) {
            throw new RobotCompileException("Could not run 'mvn clean install'", e);
        }
    }
}
