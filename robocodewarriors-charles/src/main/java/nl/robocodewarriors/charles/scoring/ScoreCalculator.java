package nl.robocodewarriors.charles.scoring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import java.util.Map;

@Component
public class ScoreCalculator {

    private ScriptEngine scriptEngine;

    @Autowired
    public ScoreCalculator(ScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
    }

    public Integer calculateScore(String calculationString, Map<String, Integer> data) throws ScoreCalculatorException {
        //TODO Replace Data in calculationString
        String resultString = buildString(calculationString, data);
        try {
            return ((Double)Double.parseDouble(scriptEngine.eval(resultString).toString())).intValue();
        } catch (ScriptException e) {
            throw new ScoreCalculatorException(resultString);
        }
    }

    private String buildString(String calculationString, Map<String, Integer> data) {
        String target = calculationString;
        for(Map.Entry<String, Integer> entry : data.entrySet()) {
            target = target.replaceAll("\\{"+entry.getKey()+"\\}", entry.getValue().toString());
        }
        return target;
    }
}
