package nl.robocodewarriors.charles.scoring;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class RobocodeScoreNormalizer {

    public void normalize(List<IndividualScore> individualScores) {
        Map<Integer, List<IndividualScore>> mappedScores = Maps.newHashMap();
        List<Integer> values = Lists.newArrayList();

        for (IndividualScore individualScore : individualScores) {
            Integer score = individualScore.getIndividual().getScore();
            values.add(score);

            List<IndividualScore> collection = mappedScores.get(score);
            if (collection == null) {
                collection = Lists.newArrayList();
                mappedScores.put(score, collection);
            }
            collection.add(individualScore);
        }
        Collections.sort(values);
        Integer min = values.get(0);

        for (Map.Entry<Integer, List<IndividualScore>> entry : mappedScores.entrySet()) {
            Integer normalized = normalizeScore(entry.getKey(), min);

            for (IndividualScore individualScore : entry.getValue()) {
                individualScore.setScore(normalized);
            }
        }
    }

    private Integer normalizeScore(Integer value, Integer min) {
        return value - min + 1;
    }
}
