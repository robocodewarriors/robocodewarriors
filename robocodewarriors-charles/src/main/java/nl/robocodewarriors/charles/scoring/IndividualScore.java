package nl.robocodewarriors.charles.scoring;

import nl.robocodewarriors.database.domain.Individual;

public class IndividualScore {

    private final Individual individual;
    private Integer score;

    public IndividualScore(Individual individual, Integer score) {
        this.individual = individual;
        this.score = score;
    }

    public Individual getIndividual() {
        return individual;
    }

    public Integer getScore() {
        return score;
    }

    @Override
    public boolean equals(Object other) {
        IndividualScore otherIndividualScore = (IndividualScore) other;
        return otherIndividualScore.individual.equals(individual) && otherIndividualScore.score == score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
