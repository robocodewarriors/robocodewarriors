package nl.robocodewarriors.charles.scoring;

public class ScoreCalculatorException extends RuntimeException {

    private String calculationString;

    public ScoreCalculatorException(String calculationString) {
        this.calculationString = calculationString;
    }

    public String getCalculationString() {
        return calculationString;
    }
}
