package nl.robocodewarriors.charles.genetics.mutationcrossover;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.Dna;
import nl.robocodewarriors.database.domain.GeneticAlgorithm;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Random;

public abstract class BaseCrossOverMutationAlgorithm<T extends CrossoverResult<U>, U extends Dna> implements GeneticAlgorithm<U> {

    private final Random random;
    private final Crossover<T, U> crossover;
    private final Mutation<U> mutation;

    public BaseCrossOverMutationAlgorithm(Crossover<T, U> crossoverApplier, Mutation<U> mutationApplier) {
        this.crossover = crossoverApplier;
        this.mutation = mutationApplier;
        random = new Random(DateTime.now().getMillis());
    }

    @Override
    public List<U> nextGeneration(List<U> oldGeneration, int crossoverRate, int mutationRate) {
        List<U> nextGeneration = Lists.newArrayList();
        int cumulativeScore = calculateCumulativeScore(oldGeneration);

        while (nextGeneration.size() < oldGeneration.size()) {
            U firstSelected = selectDna(oldGeneration, cumulativeScore);
            U secondSelected = selectDna(oldGeneration, cumulativeScore);

            T result = crossover.apply(firstSelected, secondSelected, random, crossoverRate);

            mutation.apply(result.getResultA(), random, mutationRate);
            mutation.apply(result.getResultB(), random, mutationRate);

            nextGeneration.add(result.getResultA());
            nextGeneration.add(result.getResultB());
        }

        while (nextGeneration.size() > oldGeneration.size()) {
            nextGeneration.remove(nextGeneration.size() - 1);
        }

        return nextGeneration;
    }

    protected int calculateCumulativeScore(List<U> generation) {
        int total = 0;
        for (U dna : generation) {
            total += dna.getScore();
        }
        return total;
    }

    protected final U selectDna(List<U> initialDna, int cumulativeScore) {
        if (cumulativeScore == 0) {
            return initialDna.get(0);
        }
        int offset = random.nextInt(cumulativeScore);
        int count = 0;
        for (U dna : initialDna) {
            if (offset < count + dna.getScore()) {
                return dna;
            }
            count += dna.getScore();
        }
        throw new InvalidCumulativeScoreException((List<Dna>) initialDna, cumulativeScore, offset);
    }

    public final Random getRandom() {
        return random;
    }
}
