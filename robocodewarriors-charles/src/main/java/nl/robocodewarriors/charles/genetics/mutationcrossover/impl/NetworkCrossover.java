package nl.robocodewarriors.charles.genetics.mutationcrossover.impl;

import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.charles.genetics.mutationcrossover.Crossover;
import nl.robocodewarriors.neuralnetwork.Neuron;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NetworkCrossover implements Crossover<NetworkCrossoverResult, NetworkDna> {

    @Override
    public NetworkCrossoverResult apply(NetworkDna networkDnaA, NetworkDna networkDnaB, Random random, int crossoverRate) {
        if (shouldCrossover(random, crossoverRate)) {
            int crossoverPoint = random.nextInt(networkDnaA.getTotalSize());
            return crossover(networkDnaA.getNeurons(), networkDnaB.getNeurons(), crossoverPoint);
        }

        return noCrossover(networkDnaA, networkDnaB);
    }

    private NetworkCrossoverResult crossover(List<List<Neuron>> neuronsA, List<List<Neuron>> neuronsB, int crossoverPoint) {
        ArrayList<List<Neuron>> newNeuronsA = new ArrayList<List<Neuron>>();
        ArrayList<List<Neuron>> newNeuronsB = new ArrayList<List<Neuron>>();
        int currentPos = 0;

        for (int i = 0; i < neuronsA.size(); i++) {
            ArrayList<Neuron> neuronsInLayerA = new ArrayList<Neuron>();
            ArrayList<Neuron> neuronsInLayerB = new ArrayList<Neuron>();
            for (int j = 0; j < neuronsA.get(i).size(); j++) {
                if (currentPos < crossoverPoint) {
                    neuronsInLayerA.add(neuronsA.get(i).get(j));
                    neuronsInLayerB.add(neuronsB.get(i).get(j));
                } else {
                    neuronsInLayerA.add(neuronsB.get(i).get(j));
                    neuronsInLayerB.add(neuronsA.get(i).get(j));
                }

                currentPos++;
            }

            newNeuronsA.add(neuronsInLayerA);
            newNeuronsB.add(neuronsInLayerB);
        }

        return new NetworkCrossoverResult(new NetworkDna(newNeuronsA), new NetworkDna(newNeuronsB));
    }

    private NetworkCrossoverResult noCrossover(NetworkDna dnaA, NetworkDna dnaB) {
        return crossover(dnaA.getNeurons(), dnaB.getNeurons(), 0);
    }

    private Boolean shouldCrossover(Random random, int crossoverRate) {
        return (random.nextInt(101) <= crossoverRate) ? true : false;
    }
}
