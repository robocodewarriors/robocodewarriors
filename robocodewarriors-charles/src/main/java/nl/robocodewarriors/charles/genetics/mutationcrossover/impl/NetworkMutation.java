package nl.robocodewarriors.charles.genetics.mutationcrossover.impl;

import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.charles.genetics.mutationcrossover.Mutation;
import nl.robocodewarriors.neuralnetwork.Neuron;
import nl.robocodewarriors.neuralnetwork.NeuronBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NetworkMutation implements Mutation<NetworkDna> {

    private final NeuronBuilder neuronBuilder;

    public NetworkMutation(NeuronBuilder neuronBuilder) {
        this.neuronBuilder = neuronBuilder;
    }

    @Override
    public void apply(NetworkDna networkDna, Random random, int mutationRate) {
        for (int i = 0; i < networkDna.getNeurons().size(); i++) {
            for (int j = 0; j < networkDna.getNeurons().get(i).size(); j++) {
                networkDna.getNeurons().get(i).set(j, mutate(networkDna.getNeurons().get(i).get(j), random, mutationRate));
            }
        }
    }

    private Neuron mutate(Neuron neuron, Random random, int mutationRate) {
        Double threshold = mutateThreshold(neuron.getThreshold(), random, mutationRate);
        List<Double> weights = mutateWeight(neuron.getWeights(), random, mutationRate);

        return neuronBuilder.build(neuron.getKey(), threshold, weights);
    }

    /**
     * Checks if it should mutate and takes care of the mutation
     */
    private Double mutateThreshold(Double threshold, Random random, int mutationRate) {
        return (shouldMutate(random, mutationRate)) ? (smallMutation(threshold, random)) : threshold;
    }

    /**
     * Checks if it should mutate and takes care of the mutation
     */
    private List<Double> mutateWeight(List<Double> weights, Random random, int mutationRate) {
        List<Double> newWeights = new ArrayList<Double>();

        for (int i = 0; i < weights.size(); i++) {
            if (shouldMutate(random, mutationRate)) {
                newWeights.add(smallMutation(weights.get(i), random));
            } else {
                newWeights.add(weights.get(i));
            }
        }

        return newWeights;
    }

    private boolean shouldMutate(Random random, int mutationRate) {
        return (random.nextInt(101) <= mutationRate) ? true : false;
    }

    private Double smallMutation(Double d, Random random) {
        Double result = d + (random.nextDouble() * 0.4 - 0.2);
        return (result < -1) ? -1 : result;
    }
}
