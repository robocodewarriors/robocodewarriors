package nl.robocodewarriors.charles.genetics.mutationcrossover.impl;

import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.charles.genetics.mutationcrossover.BaseCrossOverMutationAlgorithm;
import nl.robocodewarriors.neuralnetwork.NeuronBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NetworkCrossOverMutationAlgorithm extends BaseCrossOverMutationAlgorithm<NetworkCrossoverResult, NetworkDna> {

    @Autowired
    public NetworkCrossOverMutationAlgorithm(NeuronBuilder neuronBuilder) {
        super(
                new NetworkCrossover(),
                new NetworkMutation(neuronBuilder));
    }
}
