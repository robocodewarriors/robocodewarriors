package nl.robocodewarriors.charles.genetics.mutationcrossover;


import nl.robocodewarriors.database.domain.Dna;

import java.util.List;

public class InvalidCumulativeScoreException extends RuntimeException {

    private final List<Dna> dna;
    private final int cumulativeScore;
    private final int offset;

    public InvalidCumulativeScoreException(List<Dna> dna, int cumulativeScore, int offset) {
        this.dna = dna;
        this.cumulativeScore = cumulativeScore;
        this.offset = offset;
    }

    public List<Dna> getDna() {
        return dna;
    }

    public int getCumulativeScore() {
        return cumulativeScore;
    }

    public int getOffset() {
        return offset;
    }
}
