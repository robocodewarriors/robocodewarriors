package nl.robocodewarriors.charles.genetics.mutationcrossover.impl;

import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.charles.genetics.mutationcrossover.CrossoverResult;

public class NetworkCrossoverResult implements CrossoverResult<NetworkDna> {

    private final NetworkDna dnaImplA, dnaImplB;

    public NetworkCrossoverResult(final NetworkDna dnaImplA, final NetworkDna dnaImplB) {
        this.dnaImplA = dnaImplA;
        this.dnaImplB = dnaImplB;
    }

    @Override
    public NetworkDna getResultA() {
        return dnaImplA;
    }

    @Override
    public NetworkDna getResultB() {
        return dnaImplB;
    }
}
