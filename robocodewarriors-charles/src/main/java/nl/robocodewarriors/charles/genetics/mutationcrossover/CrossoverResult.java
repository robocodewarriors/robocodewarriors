package nl.robocodewarriors.charles.genetics.mutationcrossover;

import nl.robocodewarriors.database.domain.Dna;

public interface CrossoverResult<T extends Dna> {

    T getResultA();

    T getResultB();
}
