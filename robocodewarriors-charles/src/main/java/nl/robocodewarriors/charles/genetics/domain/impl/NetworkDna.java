package nl.robocodewarriors.charles.genetics.domain.impl;

import nl.robocodewarriors.database.domain.Dna;
import nl.robocodewarriors.neuralnetwork.Neuron;

import java.util.List;

public class NetworkDna implements Dna {

    private int score;
    private final List<List<Neuron>> neurons;

    public NetworkDna(List<List<Neuron>> neurons) {
        this.neurons = neurons;
    }

    @Override
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public List<List<Neuron>> getNeurons() {
        return neurons;
    }

    public int getTotalSize() {
        int size = 0;
        for (List<Neuron> list : neurons) {
            size += list.size();
        }
        return size;
    }
}
