package nl.robocodewarriors.charles.genetics.mutationcrossover;

import nl.robocodewarriors.database.domain.Dna;

import java.util.Random;

public interface Crossover<T extends CrossoverResult<U>, U extends Dna> {

    T apply(U firstDna, U otherDna, Random random, int crossoverRate);
}
