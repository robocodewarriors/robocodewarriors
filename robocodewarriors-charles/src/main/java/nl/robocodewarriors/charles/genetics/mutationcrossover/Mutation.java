package nl.robocodewarriors.charles.genetics.mutationcrossover;


import nl.robocodewarriors.database.domain.Dna;

import java.util.Random;

public interface Mutation<T extends Dna> {

    void apply(T dna, Random random, int mutationRate);

}
