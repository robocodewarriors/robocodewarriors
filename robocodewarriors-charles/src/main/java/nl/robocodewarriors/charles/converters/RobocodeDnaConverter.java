package nl.robocodewarriors.charles.converters;

import com.google.common.collect.Lists;

import nl.robocodewarriors.charles.domain.NetworkScore;
import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.neuralnetwork.*;
import nl.robocodewarriors.neuralnetwork.InputLayerImpl;
import nl.robocodewarriors.neuralnetwork.NetworkImpl;
import nl.robocodewarriors.neuralnetwork.OutputLayerImpl;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RobocodeDnaConverter implements DnaConverter<NetworkDna> {


    @Override
    public List<NetworkDna> convertToDna(List<NetworkScore> networkScores) {
        List<NetworkDna> networkDnaList = Lists.newArrayList();
        for (NetworkScore networkScore : networkScores) {
            networkDnaList.add(convertToDna(networkScore));
        }
        return networkDnaList;
    }

    @Override
    public NetworkDna convertToDna(NetworkScore networkScore) {
        List<List<Neuron>> neurons = Lists.newArrayList();
        for (Layer layer : networkScore.getNetwork().getLayers()) {
            neurons.add(layer.getNeurons());
        }
        NetworkDna networkDna = new NetworkDna(neurons);
        networkDna.setScore(networkScore.getScore());
        return networkDna;
    }

    @Override
    public List<Network> convertToNetwork(List<NetworkDna> dnaList) {
        List<Network> networkList= Lists.newArrayList();
        for (NetworkDna networkDna: dnaList) {
            networkList.add(convertToNetwork(networkDna));
        }
        return networkList;
    }

    @Override
    public Network convertToNetwork(NetworkDna networkDna) {
        List<Layer> layers = Lists.newArrayList();
        layers.add(new InputLayerImpl(networkDna.getNeurons().get(0)));
        for (int i = 1; i < networkDna.getNeurons().size() -1; i++) {
            layers.add(new HiddenLayerImpl(networkDna.getNeurons().get(i)));
        }
        layers.add(new OutputLayerImpl(networkDna.getNeurons().get(networkDna.getNeurons().size() - 1)));
        return new NetworkImpl(layers);
    }

}
