package nl.robocodewarriors.charles.converters;

import nl.robocodewarriors.charles.domain.NetworkScore;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.neuralnetworkutils.serialization.NetworkSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NetworkScoreIndividualConverter {

    private final NetworkSerializer networkSerializer;

    @Autowired
    public NetworkScoreIndividualConverter(NetworkSerializer networkSerializer) {
        this.networkSerializer = networkSerializer;
    }

    public Individual convert(NetworkScore networkScore) {
        Individual individual = new Individual();
        individual.setNetworkString(networkSerializer.serializeNetwork(networkScore.getNetwork()));
        individual.setScore(networkScore.getScore());
        return individual;
    }
}
