package nl.robocodewarriors.charles.converters;

import nl.robocodewarriors.charles.domain.NetworkScore;
import nl.robocodewarriors.database.domain.Dna;
import nl.robocodewarriors.neuralnetwork.Network;

import java.util.List;

public interface DnaConverter<T extends Dna> {

    List<T> convertToDna(List<NetworkScore> networkList);

    T convertToDna(NetworkScore networkList);

    List<Network> convertToNetwork(List<T> dnaList);

    Network convertToNetwork(T networkDna);
}
