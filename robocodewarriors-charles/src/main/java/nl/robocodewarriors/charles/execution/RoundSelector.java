package nl.robocodewarriors.charles.execution;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RoundSelector<T> {

    private List<T> typeObjectList;
    private List<List<T>> groups;
    private int groupOffset;

    public RoundSelector(List<T> typeObjects, int groupSize, int occurrences) {
        groupOffset = 0;
        typeObjectList = typeObjects;
        groups = new ArrayList<List<T>>();

        initGroups(occurrences, groupSize);
    }

    public List<T> getNextRobotGroup() {
        if (!hasMoreGroups()) {
            throw new NoGroupException();
        }

        return groups.get(groupOffset++);
    }

    public boolean hasMoreGroups() {
        return (groupOffset < groups.size());
    }

    private void initGroups(int occurrences, int groupSize) {
        List<T> typeObjectListCopy = new ArrayList<T>(typeObjectList);

        for (int i = 0; i < occurrences; i++) {
            Collections.shuffle(typeObjectListCopy);

            List<T> subList = Lists.newArrayList();
            for (T element : typeObjectListCopy) {
                subList.add(element);
                if (subList.size() == groupSize) {
                    groups.add(subList);
                    subList = Lists.newArrayList();
                }
            }
            if (subList.size() > 0) {
                groups.add(subList);
            }
        }

    }
}
