package nl.robocodewarriors.charles.execution;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoundSelectorFactory {

    public <T> RoundSelector<T> create(List<T> objects, int groupSize, int occurrences) {
        return new RoundSelector<T>(objects, groupSize, occurrences);
    }
}