package nl.robocodewarriors.charles.resources;

import nl.robocodewarriors.charles.services.EvolutionService;
import nl.robocodewarriors.charles.services.PendingExecutionGroupCleanupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/tasks")
public class TaskResource {

    @Autowired
    private PendingExecutionGroupCleanupService pendingExecutionGroupCleanupService;

    @Autowired
    private EvolutionService evolutionService;

    @RequestMapping(value = "/pending", method = RequestMethod.GET)
    @ResponseBody
    public void pending() {
        pendingExecutionGroupCleanupService.cleanAllPendingGroups();
    }

    @RequestMapping(value = "/evolve", method = RequestMethod.GET)
    @ResponseBody
    public void evolve() {
        evolutionService.evolveGenerations();
    }

}
