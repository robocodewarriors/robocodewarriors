package nl.robocodewarriors.charles.context;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;

import nl.robocodewarriors.neuralnetwork.NeuronBuilder;
import nl.robocodewarriors.neuralnetwork.NeuronBuilderImpl;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.sql.DataSource;

import java.sql.SQLException;

@EnableTransactionManagement
@Configuration
@EnableWebMvc
@PropertySource("classpath:datasource.properties")
public class ApplicationContext {

    @Autowired
    private Environment environment;

    @Bean
    public NeuronBuilder neuronBuilder() {
        return new NeuronBuilderImpl();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(5);
        threadPoolTaskExecutor.setMaxPoolSize(10);
        threadPoolTaskExecutor.setQueueCapacity(25);
        return threadPoolTaskExecutor;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public DataSource dataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUrl(environment.getProperty("datasource.url"));
        dataSource.setUser(environment.getProperty("datasource.username"));
        dataSource.setPassword(environment.getProperty("datasource.password"));

        return dataSource;
    }

    @Bean
    public SimpleJDBCConnectionPool simpleJDBCConnectionPool() throws SQLException {
        return new SimpleJDBCConnectionPool(
                "com.mysql.jdbc.Driver",
                environment.getProperty("datasource.url"),
                environment.getProperty("datasource.username"),
                environment.getProperty("datasource.password"),
                2,
                5
        );
    }

    @Bean
    public SessionFactory sessionFactory(DataSource dataSource) {
        return new LocalSessionFactoryBuilder(dataSource)
                .scanPackages("nl.robocodewarriors.database.domain")
                .buildSessionFactory();
    }

    @Bean
    public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }

    @Bean
    public ScriptEngine scriptEngine() {
        ScriptEngineManager mgr = new ScriptEngineManager();
        return mgr.getEngineByName("JavaScript");
    }
}
