package nl.robocodewarriors.charles.context;

import nl.robocodewarriors.charles.jobs.CharlesTaskExecutor;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import ru.xpoft.vaadin.SpringApplicationContext;
import ru.xpoft.vaadin.SpringVaadinServlet;

public class CharlesInitializer implements WebApplicationInitializer {
    
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        final AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.setServletContext(servletContext);
        rootContext.scan("nl.robocodewarriors");
        rootContext.refresh();
        SpringApplicationContext.setApplicationContext(rootContext);
        
        final ServletRegistration.Dynamic vaadin = servletContext.addServlet("vaadin", new SpringVaadinServlet());
        final ServletRegistration.Dynamic foundation = servletContext.addServlet("tasks", new DispatcherServlet(rootContext));
        vaadin.setInitParameter("beanName", "userInterface");
        foundation.addMapping("/rest/*");
        vaadin.addMapping("/*");
        vaadin.setLoadOnStartup(1);
        foundation.setLoadOnStartup(1);
        
        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.addListener(new RequestContextListener());
        
        rootContext.getBean("charlesTaskExecutor", CharlesTaskExecutor.class).executeTasks();
    }
}
