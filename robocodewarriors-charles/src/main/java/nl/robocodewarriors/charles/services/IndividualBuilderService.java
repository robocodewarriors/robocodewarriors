package nl.robocodewarriors.charles.services;

import com.google.common.collect.Lists;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.services.IndividualService;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.neuralnetworkutils.serialization.NetworkSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IndividualBuilderService {

    private final NetworkSerializer networkSerializer;
    private IndividualService individualService;

    @Autowired
    public IndividualBuilderService(NetworkSerializer networkSerializer, IndividualService individualService) {
        this.networkSerializer = networkSerializer;
        this.individualService = individualService;
    }

    public Individual createIndividual(Network network) {
        Individual individual = new Individual();
        individual.setNetworkString(networkSerializer.serializeNetwork(network));
        individualService.save(individual);
        return individual;
    }

    public List<Individual> createIndividuals(List<Network> networks) {
        List<Individual> individualList = Lists.newArrayList();

        for (Network network : networks) {
            individualList.add(createIndividual(network));
        }
        return individualList;
    }
}
