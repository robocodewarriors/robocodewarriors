package nl.robocodewarriors.charles.services;

import com.google.common.collect.Lists;
import nl.robocodewarriors.charles.converters.RobocodeDnaConverter;
import nl.robocodewarriors.charles.domain.NetworkScore;
import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.charles.genetics.mutationcrossover.impl.NetworkCrossOverMutationAlgorithm;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.neuralnetworkutils.serialization.NetworkSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MutationService {

    private NetworkCrossOverMutationAlgorithm algorithm;
    private RobocodeDnaConverter converter;
    private NetworkSerializer networkSerializer;

    @Autowired
    public MutationService(NetworkCrossOverMutationAlgorithm algorithm, RobocodeDnaConverter converter, NetworkSerializer networkSerializer) {
        this.algorithm = algorithm;
        this.converter = converter;
        this.networkSerializer = networkSerializer;
    }

    public List<Network> mutateNetworks(List<Individual> individuals, Training training) {
        List<NetworkScore> networkScores = Lists.newArrayList();

        for (Individual individual : individuals) {
            NetworkScore networkScore = new NetworkScore(networkSerializer.createNetwork(individual.getNetworkString()), individual.getScore());
            networkScores.add(networkScore);
        }

        List<NetworkDna> dna = converter.convertToDna(networkScores);
        List<NetworkDna> newDna = algorithm.nextGeneration(dna, training.getCrossoverRate(), training.getMutationRate());

        return converter.convertToNetwork(newDna);
    }
}
