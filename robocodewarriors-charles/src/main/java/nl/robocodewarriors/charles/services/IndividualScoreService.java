package nl.robocodewarriors.charles.services;

import com.google.common.collect.Maps;
import nl.robocodewarriors.charles.scoring.ScoreCalculator;
import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.services.ExecutionGroupIndividualService;
import nl.robocodewarriors.database.services.IndividualService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class IndividualScoreService {

    private final IndividualService individualService;
    private final ScoreCalculator scoreCalculator;
    private final ExecutionGroupIndividualService executionGroupIndividualService;

    @Autowired
    public IndividualScoreService(IndividualService individualService, ScoreCalculator scoreCalculator, ExecutionGroupIndividualService executionGroupIndividualService) {
        this.individualService = individualService;
        this.scoreCalculator = scoreCalculator;
        this.executionGroupIndividualService = executionGroupIndividualService;
    }

    public void calculateScore(Individual individual, Training training) {
        List<ExecutionGroupIndividual> executionGroupIndividuals = executionGroupIndividualService.getForIndividual(individual);
        Map<String, Integer> scoreData = createScoreDataMap(executionGroupIndividuals);

        Integer score = scoreCalculator.calculateScore(training.getScoreCalculation() , scoreData);
        individual.setScore(score);
        individualService.save(individual);
    }

    public void calculateScores(List<Individual> individuals, Training training) {
        for(Individual individual : individuals) {
            calculateScore(individual, training);
        }
    }
    private Map<String, Integer> createScoreDataMap(List<ExecutionGroupIndividual> executionGroupIndividuals) {
        //TODO: I will take the blame for this. -xxx- Mats
        Map<String, Integer> scoreData = Maps.newHashMap();
        scoreData.put("bulletDamage", 0);
        scoreData.put("bulletDamageBonus", 0);
        scoreData.put("lastSurvivorBonus", 0);
        scoreData.put("ramDamage", 0);
        scoreData.put("ramDamageBonus", 0);
        scoreData.put("rank", 0);
        scoreData.put("score", 0);
        scoreData.put("survival", 0);

        for (ExecutionGroupIndividual executionGroupIndividual : executionGroupIndividuals) {
            scoreData.put("bulletDamage", scoreData.get("bulletDamage") + notNull(executionGroupIndividual.getBulletDamage()));
            scoreData.put("bulletDamageBonus", scoreData.get("bulletDamageBonus") + notNull(executionGroupIndividual.getBulletDamageBonus()));
            scoreData.put("lastSurvivorBonus", scoreData.get("lastSurvivorBonus") + notNull(executionGroupIndividual.getLastSurvivorBonus()));
            scoreData.put("ramDamage", scoreData.get("ramDamage") + notNull(executionGroupIndividual.getRamDamage()));
            scoreData.put("ramDamageBonus", scoreData.get("ramDamageBonus") + notNull(executionGroupIndividual.getRamDamageBonus()));
            scoreData.put("rank", scoreData.get("rank") + notNull(executionGroupIndividual.getRank()));
            scoreData.put("score", scoreData.get("score") + notNull(executionGroupIndividual.getScore()));
            scoreData.put("survival", scoreData.get("survival") + notNull(executionGroupIndividual.getSurvival()));
        }

        return scoreData;
    }

    private Integer notNull(Integer value) {
        if(value == null) {
            return 0;
        }
        return value;
    }
}
