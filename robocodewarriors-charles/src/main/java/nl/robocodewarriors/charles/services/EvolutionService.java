package nl.robocodewarriors.charles.services;

import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.services.ExecutionGroupService;
import nl.robocodewarriors.database.services.GenerationService;
import nl.robocodewarriors.database.services.IndividualService;
import nl.robocodewarriors.database.services.TrainingService;
import nl.robocodewarriors.neuralnetwork.Network;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class EvolutionService {

    @Autowired
    private GenerationService generationService;

    @Autowired
    private ExecutionGroupService executionGroupService;

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private ExecutionGroupBuilderService executionGroupBuilderService;

    @Autowired
    private IndividualScoreService individualScoreService;

    @Autowired
    private IndividualService individualService;

    @Autowired
    private MutationService mutationService;

    @Autowired
    private IndividualBuilderService individualBuilderService;

    //Let Franks Laptop smoke... Challenge accepted
    //What do we say to clean code? Not today!
    @Transactional
    public void evolveGenerations() {
        List<Generation> unfinishedGenerations = generationService.getUnfinishedGenerations();
        System.out.println("Found Unfinished Generations: " + unfinishedGenerations.size());
        for (Generation unfinishedGeneration : unfinishedGenerations) {

            if (executionGroupService.hasFinishedGeneration(unfinishedGeneration)) {
                System.out.println("Finished Generation: " + unfinishedGeneration.getId());

                Training training = unfinishedGeneration.getTraining();
                generationService.finishGeneration(unfinishedGeneration);

                if (training.getGenerationCount().equals(unfinishedGeneration.getGenerationIndex())) {
                    System.out.println("Training Finished");
                    trainingService.finishTraining(training);
                    //Training has finished //TODO
                } else {
                    System.out.println("Has to build next generation");
                    Generation nextGeneration = generationService.createGeneration(training, unfinishedGeneration.getGenerationIndex() + 1);
                    List<Individual> individuals = evolveIndividuals(unfinishedGeneration, training);

                    executionGroupBuilderService.buildExecutionGroups(individuals, nextGeneration, training.getRoundSize(), training.getRoundOccurrences(), training.getOtherRobots());
                    generationService.startGeneration(nextGeneration);
                }
            }
        }
    }

    private List<Individual> evolveIndividuals(Generation oldGeneration, Training training) {
        List<Individual> oldGenerationIndividuals = individualService.getByGeneration(oldGeneration);
        individualScoreService.calculateScores(oldGenerationIndividuals, oldGeneration.getTraining());

        List<Network> newNetworks = mutationService.mutateNetworks(oldGenerationIndividuals, training);
        return individualBuilderService.createIndividuals(newNetworks);
    }
}
