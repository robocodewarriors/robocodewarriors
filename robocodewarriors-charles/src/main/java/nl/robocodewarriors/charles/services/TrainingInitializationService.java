package nl.robocodewarriors.charles.services;

import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.services.GenerationService;
import nl.robocodewarriors.database.services.TrainingService;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.neuralnetworkutils.builder.NetworkBuilder;
import nl.robocodewarriors.utils.DateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


@Component
public class TrainingInitializationService {

    @Autowired
    private TrainingService trainingService;
    @Autowired
    private GenerationService generationService;
    @Autowired
    private IndividualBuilderService individualBuilderService;
    @Autowired
    private ExecutionGroupBuilderService executionGroupBuilderService;
    @Autowired
    private DateManager dateManager;
    @Autowired
    private NetworkBuilder networkBuilder;

    @Transactional
    public Training saveTraining(Training training, Set<String> inputs, Set<String> outputs) {
        training.setStartDateTime(dateManager.now());
        trainingService.save(training);

        List<Network> networks = networkBuilder.buildNetworks(inputs, outputs, training.getPopulationSize(), training.getHiddenLayerSize(), training.getNeuronsPerLayer());
        List<Individual> individuals = individualBuilderService.createIndividuals(networks);

        Generation generation = generationService.createGeneration(training, 1);
        executionGroupBuilderService.buildExecutionGroups(individuals, generation, training.getRoundSize(), training.getRoundOccurrences(), training.getOtherRobots());
        generationService.startGeneration(generation);
        return training;
    }

}
