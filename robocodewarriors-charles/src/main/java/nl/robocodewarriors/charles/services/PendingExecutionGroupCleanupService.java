package nl.robocodewarriors.charles.services;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.State;
import nl.robocodewarriors.database.services.ExecutionGroupService;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PendingExecutionGroupCleanupService {

    private static final Logger logger = LoggerFactory.getLogger(PendingExecutionGroupCleanupService.class);

    @Autowired
    private ExecutionGroupService executionGroupService;


    public void cleanAllPendingGroups() {
        List<ExecutionGroup> pendingGroups = executionGroupService.allPending();
        List<ExecutionGroup> changedGroups = Lists.newArrayList();
        Long millis = DateTime.now().minusSeconds(300).getMillis(); //HAHA frank http://testingwithfrank.com/images/frank.png
        for(ExecutionGroup pendingGroup : pendingGroups) {
            if (pendingGroup.getStartMillis() == null || pendingGroup.getStartMillis() < millis) {
                pendingGroup.setState(State.NOT_STARTED);
                pendingGroup.setStartMillis(null);
                changedGroups.add(pendingGroup);
            }
        }
        if (changedGroups.size() > 0 ) {
            logger.info("Updating " + changedGroups.size() + " groups!");
            executionGroupService.saveAll(changedGroups);
        }
    }
}
