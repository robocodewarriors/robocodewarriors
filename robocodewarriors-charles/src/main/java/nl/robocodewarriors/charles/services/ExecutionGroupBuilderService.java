package nl.robocodewarriors.charles.services;

import nl.robocodewarriors.charles.execution.RoundSelector;
import nl.robocodewarriors.charles.execution.RoundSelectorFactory;
import nl.robocodewarriors.database.domain.*;
import nl.robocodewarriors.database.services.ExecutionGroupIndividualService;
import nl.robocodewarriors.database.services.ExecutionGroupService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExecutionGroupBuilderService {

    private final ExecutionGroupService executionGroupService;
    private final ExecutionGroupIndividualService executionGroupIndividualService;
    private final RoundSelectorFactory roundSelectorFactory;

    @Autowired
    public ExecutionGroupBuilderService(ExecutionGroupService executionGroupService, ExecutionGroupIndividualService executionGroupIndividualService, RoundSelectorFactory roundSelectorFactory) {
        this.executionGroupService = executionGroupService;
        this.executionGroupIndividualService = executionGroupIndividualService;
        this.roundSelectorFactory = roundSelectorFactory;
    }


    public void buildExecutionGroups(List<Individual> individuals, Generation generation, int groupSize, int occurences, String otherRobots) {
        RoundSelector<Individual> roundSelector = roundSelectorFactory.create(individuals, groupSize, occurences);
        while (roundSelector.hasMoreGroups()) {
            List<Individual> roundIndividuals = roundSelector.getNextRobotGroup();
            ExecutionGroup executionGroup = new ExecutionGroup();
            executionGroup.setOtherRobots(otherRobots);
            executionGroup.setGeneration(generation);
            executionGroup.setState(State.NOT_STARTED);
            executionGroupService.save(executionGroup);
            createExecutionGroupIndividuals(roundIndividuals, executionGroup);
        }
    }

    private void createExecutionGroupIndividuals(List<Individual> roundIndividuals, ExecutionGroup executionGroup) {
        for (Individual individual : roundIndividuals) {
            ExecutionGroupIndividual executionGroupIndividual = new ExecutionGroupIndividual();
            executionGroupIndividual.setIndividual(individual);
            executionGroupIndividual.setExecutionGroup(executionGroup);
            executionGroupIndividualService.save(executionGroupIndividual);

        }
    }
}
