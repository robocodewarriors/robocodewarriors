package nl.robocodewarriors.charles.ui.table;

import com.google.common.eventbus.Subscribe;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.Reindeer;

import nl.robocodewarriors.charles.ui.container.SqlTrainingContainer;
import nl.robocodewarriors.database.domain.Training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TrainingTable extends Panel{

    private final Table table;

    @Autowired
    public TrainingTable(SqlTrainingContainer sqlTrainingContainer) throws SQLException {
        table = new Table("generations", sqlTrainingContainer);
        setContent(table);
        setCaption("Generations");
        table.setStyleName(Reindeer.TABLE_BORDERLESS);
    }

    @Subscribe
    public void addNewTraining(Training training) {
        table.refreshRowCache();
    }
}
