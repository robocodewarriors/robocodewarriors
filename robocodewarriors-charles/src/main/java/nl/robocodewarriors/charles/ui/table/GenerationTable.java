package nl.robocodewarriors.charles.ui.table;

import com.vaadin.ui.Table;

import nl.robocodewarriors.charles.ui.container.SqlGenerationContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenerationTable extends Table {

    private final SqlGenerationContainer sqlGenerationContainer;

    @Autowired
    public GenerationTable(SqlGenerationContainer sqlGenerationContainer) throws SQLException {
        super("Training", sqlGenerationContainer);
        this.sqlGenerationContainer = sqlGenerationContainer;
    }

    public void setTrainingId(Long trainingId) {
        sqlGenerationContainer.setTrainingId(trainingId);
    }
}
