package nl.robocodewarriors.charles.ui.container;

import com.google.common.collect.Lists;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.AbstractContainer;
import com.vaadin.data.util.BeanItem;

import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.repository.TrainingRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

@Component
public class TrainingContainer extends AbstractContainer implements Container.Ordered {

    private final TrainingRepository trainingRepository;


    @Autowired
    public TrainingContainer(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    @Override
    public Object nextItemId(Object itemId) {
        return trainingRepository.getIdOfNextRecord((Integer) itemId);
    }

    @Override
    public Object prevItemId(Object itemId) {
        return trainingRepository.getIdOfPreviousRecord((Integer) itemId);
    }

    @Override
    public Object firstItemId() {
        return trainingRepository.getIdOfFirstRecord();
    }

    @Override
    public Object lastItemId() {
        return trainingRepository.getIdOfLastRecord();
    }

    @Override
    public boolean isFirstId(Object itemId) {
        return firstItemId().equals(itemId);
    }

    @Override
    public boolean isLastId(Object itemId) {
        return lastItemId().equals(itemId);
    }

    @Override
    public Object addItemAfter(Object previousItemId) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Item addItemAfter(Object previousItemId, Object newItemId) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Item getItem(Object itemId) {
        return new BeanItem<Training>(trainingRepository.find((Integer) itemId));
    }

    @Override
    public Collection<?> getContainerPropertyIds() {
        Field[] fields = Training.class.getDeclaredFields();
        List<String> containerPropertyIds = Lists.newArrayList();
        for (Field field : fields) {
            containerPropertyIds.add(field.getName());
        }
        return containerPropertyIds;
    }

    @Override
    public Collection<?> getItemIds() {
        return trainingRepository.retrieveAllIds();
    }

    @Override
    public Property getContainerProperty(Object itemId, Object propertyId) {
        return getItem(itemId).getItemProperty(propertyId);
    }

    @Override
    public Class<?> getType(Object propertyId) {
        try {
            Field field = Training.class.getDeclaredField(propertyId.toString());
            return field.getType();
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    @Override
    public int size() {
        return trainingRepository.size();
    }

    @Override
    public boolean containsId(Object itemId) {
        return (trainingRepository.find((Integer) itemId) != null);
    }

    @Override
    public Item addItem(Object itemId) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object addItem() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeItem(Object itemId) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addContainerProperty(Object propertyId, Class<?> type, Object defaultValue) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeContainerProperty(Object propertyId) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAllItems() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    private class Page {
        private Integer index;
        private List<Training> trainingList = Lists.newArrayList();


    }
}
