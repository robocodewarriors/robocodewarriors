package nl.robocodewarriors.charles.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.Reindeer;

import nl.robocodewarriors.charles.ui.view.TrainingView;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.DiscoveryNavigator;


@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Theme(Reindeer.THEME_NAME)
public class UserInterface extends UI{

    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle("Charles");

        HorizontalLayout wrapper = new HorizontalLayout();
        wrapper.setSpacing(true);
        wrapper.setSizeFull();

        HorizontalLayout content = new HorizontalLayout();
        wrapper.setMargin(true);
        wrapper.setSpacing(true);
        setStyleName(Reindeer.LAYOUT_WHITE);

        wrapper.addComponent(content);
        setContent(wrapper);

        DiscoveryNavigator navigator = new DiscoveryNavigator(UI.getCurrent(), content);
        if(getPage().getUriFragment() != null) {
            navigator.navigateTo(getPage().getUriFragment().substring(1));
        } else {
            navigator.navigateTo(TrainingView.NAME);
        }

    }

}
