package nl.robocodewarriors.charles.ui.container;

public interface CacheItem {

    Long getId();
}
