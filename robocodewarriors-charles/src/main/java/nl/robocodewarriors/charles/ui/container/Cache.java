package nl.robocodewarriors.charles.ui.container;

import java.util.List;

public class Cache<T extends CacheItem> {

    private Long start;
    private Long end;
    private List<T> elements;

    public Cache(List<T> elements) {
        this.elements = elements;
        this.start = elements.get(0).getId();
        this.end = elements.get(elements.size() - 1).getId();
    }

    public Boolean containsIndex(Long index) {
        return (index >= start && index <= end);
    }

    public T getElement(Long index) {
        if(!containsIndex(index)) {
            return null;
        }
        for (T element : elements) {
            if(element.getId().equals(index)) {
                return element;
            }
        }
        return null;
    }

    public Long getStart() {
        return start;
    }

    public Long getEnd() {
        return end;
    }

    public List<T> getElements() {
        return elements;
    }

}
