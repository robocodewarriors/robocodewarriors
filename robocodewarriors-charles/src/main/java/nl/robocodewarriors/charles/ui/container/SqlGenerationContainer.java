package nl.robocodewarriors.charles.ui.container;

import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.sqlcontainer.SQLContainer;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.query.TableQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SqlGenerationContainer extends SQLContainer{

    @Autowired
    public SqlGenerationContainer(SimpleJDBCConnectionPool simpleJDBCConnectionPool) throws SQLException {
        super(new TableQuery("generation", simpleJDBCConnectionPool));
    }

    public void setTrainingId(Long id) {
        this.removeAllContainerFilters();
        this.addContainerFilter(new Compare.Equal("training", id));
    }
}
