package nl.robocodewarriors.charles.ui.component;

import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.VerticalLayout;
import nl.robocodewarriors.charles.ui.table.ExecutionGroupTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExecutionGroupOverview extends VerticalLayout {

    private final ExecutionGroupTable executionGroupTable;
    private Long generationId;

    @Autowired
    public ExecutionGroupOverview(ExecutionGroupTable executionGroupTable) {
        this.executionGroupTable = executionGroupTable;
        addComponent(executionGroupTable);
    }

    public void setGenerationId(Long id) {
        executionGroupTable.setGenerationId(id);
        generationId = id;
    }

}
