package nl.robocodewarriors.charles.ui.form;

import com.google.common.eventbus.EventBus;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import nl.robocodewarriors.charles.genetics.mutationcrossover.impl.NetworkCrossOverMutationAlgorithm;
import nl.robocodewarriors.charles.services.TrainingInitializationService;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TrainingForm extends Panel{

    @Autowired
    private TrainingInitializationService trainingInitializationService;

    @PropertyId("scoreCalculation")
    TextArea scoreCalculation = new TextArea("Score Calculation Algorithm");

    @PropertyId("generationCount")
    TextField generationCount = new TextField("Amount of generations to train");

    @PropertyId("populationSize")
    TextField populationSize = new TextField("Total population per generation");

    @PropertyId("roundSize")
    TextField roundSize = new TextField("How many robots per round");

    @PropertyId("hiddenLayerSize")
    TextField hiddenLayerSize = new TextField("Amount of hidden layers");

    @PropertyId("neuronsPerLayer")
    TextField neuronsPerLayer = new TextField("Neurons per layer");

    @PropertyId("roundOccurrences")
    TextField roundOccurrences = new TextField("How many rounds should a robot play per generation");

    @PropertyId("mutationRate")
    TextField mutationRate = new TextField("Mutation rate");

    @PropertyId("crossoverRate")
    TextField crossoverRate = new TextField("Crossover rate");

    @PropertyId("otherRobots")
    TextField otherRobots = new TextField("Other Robots");

    TwinColSelect inputKeysSelect = new TwinColSelect("Choose input keys");

    TwinColSelect outputKeysSelect = new TwinColSelect("Choose output keys");

    private static final String[] variables = new String[] { "bulletDamage", "bulletDamageBonus", "lastSurvivorBonus", "ramDamage", "ramDamageBonus", "rank", "score", "survival" };


    private Training item = new Training();
    private EventBus bus;

    public TrainingForm() {
        setStyleName(Reindeer.LAYOUT_WHITE);
        setCaption("Configure new training");
        final FormLayout layout = new FormLayout();
        setContent(layout);
        layout.setSpacing(true);
        layout.setMargin(true);
        ComboBox scoreCalculationVariables = new ComboBox("Choose calculation variables");
        scoreCalculationVariables.setInputPrompt("calculation variables");
        scoreCalculationVariables.setImmediate(true);
        scoreCalculationVariables.setFilteringMode(FilteringMode.CONTAINS);

        for (String variable : variables) {
            scoreCalculationVariables.addItem(variable);
        }

        scoreCalculationVariables.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (event.getProperty().getValue() != null) {
                    scoreCalculation.setValue(scoreCalculation.getValue() + " {" + event.getProperty().getValue() + "}");
                }
            }
        });

        for (String key : NeuralRobot.getInputKeys()) {
            inputKeysSelect.addItem(key);
        }

        for (String key : NeuralRobot.getOutputKeys()) {
            outputKeysSelect.addItem(key);
        }
        inputKeysSelect.setRows(NeuralRobot.getInputKeys().size());
        inputKeysSelect.setLeftColumnCaption("disabled");
        inputKeysSelect.setRightColumnCaption("enabled");

        outputKeysSelect.setRows(NeuralRobot.getOutputKeys().size());
        outputKeysSelect.setLeftColumnCaption("disabled");
        outputKeysSelect.setRightColumnCaption("enabled");

        outputKeysSelect.setWidth(800, Unit.PIXELS);
        inputKeysSelect.setWidth(800, Unit.PIXELS);

        crossoverRate.setWidth(150, Unit.PIXELS);
        mutationRate.setWidth(150, Unit.PIXELS);
        ;
        layout.addComponents(scoreCalculation, scoreCalculationVariables, generationCount, populationSize, roundSize, hiddenLayerSize,
                neuronsPerLayer, roundOccurrences, crossoverRate, mutationRate, otherRobots, inputKeysSelect, outputKeysSelect);

        generationCount.addValidator(new BeanValidator(Training.class, "generationCount"));
        populationSize.addValidator(new BeanValidator(Training.class, "populationSize"));
        roundSize.addValidator(new BeanValidator(Training.class, "roundSize"));
        scoreCalculation.addValidator(new BeanValidator(Training.class, "scoreCalculation"));
        neuronsPerLayer.addValidator(new BeanValidator(Training.class, "neuronsPerLayer"));
        hiddenLayerSize.addValidator(new BeanValidator(Training.class, "hiddenLayerSize"));
        roundOccurrences.addValidator(new BeanValidator(Training.class, "roundOccurrences"));
        roundOccurrences.addValidator(new BeanValidator(Training.class, "crossoverRate"));
        roundOccurrences.addValidator(new BeanValidator(Training.class, "mutationRate"));
        otherRobots.addValidator(new BeanValidator(Training.class, "otherRobots"));

        generationCount.setImmediate(true);
        populationSize.setImmediate(true);
        roundSize.setImmediate(true);
        scoreCalculation.setImmediate(true);
        neuronsPerLayer.setImmediate(true);
        hiddenLayerSize.setImmediate(true);
        roundOccurrences.setImmediate(true);
        crossoverRate.setImmediate(true);
        mutationRate.setImmediate(true);
        inputKeysSelect.setImmediate(true);
        outputKeysSelect.setImmediate(true);
        otherRobots.setImmediate(true);

        final FieldGroup binder = new FieldGroup(new BeanItem<Training>(item));
        binder.bindMemberFields(this);
        setDefaults();

        Button button = new Button("Submit training", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    scoreCalculation.focus();
                    event.getButton().setEnabled(true);
                    binder.commit();
                } catch (FieldGroup.CommitException e) {
                    Notification.show("Your training could not be submitted, please fix your validation errors");
                    return;
                }
                item.setLearningAlgorithm(NetworkCrossOverMutationAlgorithm.class.getName());
                Collection<String> inputs = (Collection<String>) inputKeysSelect.getValue();
                Collection<String> outputs = (Collection<String>) outputKeysSelect.getValue();
                Training training = null;
                try {
                    training = trainingInitializationService.saveTraining(item, new HashSet<String>(inputs), new HashSet<String>(outputs));
                } catch (Exception e) {
                    Notification.show(e.toString());
                }

                if(training != null) {
                    bus.post(training);
                }

                item = new Training();
                binder.setItemDataSource(new BeanItem<Training>(item));
                setDefaults();
                Notification.show("The training has been submitted");
            }
        });
        button.setDisableOnClick(true);
        layout.addComponent(button);
    }

    private void setDefaults() {
        generationCount.setValue("1000");
        populationSize.setValue("1000");
        roundSize.setValue("2");
        scoreCalculation.setValue("{score}");
        neuronsPerLayer.setValue(String.valueOf(NeuralRobot.getInputKeys().size()));
        hiddenLayerSize.setValue("1");
        roundOccurrences.setValue("3");
        mutationRate.setValue("30");
        crossoverRate.setValue("70");
        otherRobots.setValue("");
    }

    public void setEventBus(EventBus bus) {
        this.bus = bus;
    }
}
