package nl.robocodewarriors.charles.ui.view;

import com.google.common.eventbus.EventBus;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.HorizontalLayout;

import com.vaadin.ui.VerticalLayout;
import nl.robocodewarriors.charles.ui.form.TrainingForm;
import nl.robocodewarriors.charles.ui.table.TrainingTable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.VaadinView;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@VaadinView(TrainingView.NAME)
public class TrainingView extends VerticalLayout implements View {
    public static final String NAME = "training";

    private final TrainingForm trainingFormView;
    private final TrainingTable trainingTable;
    private final EventBus bus;

    @Autowired
    public TrainingView(TrainingForm trainingFormView, TrainingTable trainingTable) {
        this.trainingFormView = trainingFormView;
        this.trainingTable = trainingTable;
        bus = new EventBus();
        trainingFormView.setEventBus(bus);
        bus.register(trainingTable);
        addComponent(trainingFormView);
        addComponent(trainingTable);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
