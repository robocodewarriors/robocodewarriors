package nl.robocodewarriors.charles.ui.table;

import com.vaadin.ui.Table;

import nl.robocodewarriors.charles.ui.container.SqlExecutionGroupContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExecutionGroupTable extends Table {

    private final SqlExecutionGroupContainer sqlExecutionGroupContainer;

    @Autowired
    public ExecutionGroupTable(SqlExecutionGroupContainer sqlExecutionGroupContainer) {
        super("Execution groups", sqlExecutionGroupContainer);
        this.sqlExecutionGroupContainer = sqlExecutionGroupContainer;
    }

    public void setGenerationId(Long id) {
        sqlExecutionGroupContainer.setGenerationId(id);
    }
}
