package nl.robocodewarriors.charles.ui.view;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.HorizontalLayout;

import nl.robocodewarriors.charles.ui.component.ExecutionGroupOverview;
import nl.robocodewarriors.charles.ui.table.GenerationTable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.VaadinView;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@VaadinView(GenerationView.NAME)
public class GenerationView extends HorizontalLayout implements View {

    public static final String NAME = "generations";

    private final GenerationTable generationTable;
    private final ExecutionGroupOverview overview;

    @Autowired
    public GenerationView(GenerationTable generationTable, ExecutionGroupOverview executionGroupOverview) {
        this.generationTable = generationTable;
        this.overview = executionGroupOverview;
        generationTable.setSelectable(true);
        addComponent(generationTable);
        addComponent(executionGroupOverview);

        generationTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                overview.setGenerationId(Long.parseLong(String.valueOf(event.getItemId())) );
            }
        });
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        if(event.getParameters().length() > 0) {
            generationTable.setTrainingId(Long.parseLong(event.getParameters()));
        }

    }
}
