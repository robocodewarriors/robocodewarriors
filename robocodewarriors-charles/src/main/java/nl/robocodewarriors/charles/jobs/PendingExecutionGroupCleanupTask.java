package nl.robocodewarriors.charles.jobs;

import org.joda.time.Seconds;
import org.springframework.stereotype.Component;

@Component
public class PendingExecutionGroupCleanupTask
        extends CharlesJob {

    public PendingExecutionGroupCleanupTask() {
        super(Seconds.seconds(5), "/tasks/pending");
    }
}
