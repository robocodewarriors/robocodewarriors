package nl.robocodewarriors.charles.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CharlesTaskExecutor {

    private final TaskExecutor taskExecutor;
    private final List<CharlesJob> jobs;

    @Autowired
    public CharlesTaskExecutor(TaskExecutor taskExecutor, List<CharlesJob> jobs) {
        this.taskExecutor = taskExecutor;
        this.jobs = jobs;
    }

    public void executeTasks() {
        for (CharlesJob job : jobs) {
            taskExecutor.execute(job);
        }
    }
}