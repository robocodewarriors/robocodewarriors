package nl.robocodewarriors.charles.jobs;

import org.joda.time.Seconds;
import org.springframework.stereotype.Component;

@Component
public class IndividualEvolver extends CharlesJob {

    public IndividualEvolver() {
        super(Seconds.seconds(10), "/tasks/evolve");
    }
    
}
