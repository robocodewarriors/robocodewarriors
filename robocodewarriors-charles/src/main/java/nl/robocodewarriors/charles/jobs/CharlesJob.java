package nl.robocodewarriors.charles.jobs;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public abstract class CharlesJob implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(CharlesJob.class);

    private final Seconds timeoutSeconds;
    private final String path;

    public CharlesJob(Seconds timeoutSeconds, String path) {
        this.timeoutSeconds = timeoutSeconds;
        this.path = path;
    }

    @Override
    public final void run() {
        while (true) {
            try {
                logger.info("Start CharlesJob for class: " + getClass().getSimpleName());
                Thread.sleep(timeoutSeconds.toStandardDuration().getMillis());

                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet("http://localhost:9090/rest" + path);
                HttpResponse response = client.execute(get);
                EntityUtils.consume(response.getEntity());
                logger.info("Finished CharlesJob for class: " + getClass().getSimpleName());
            } catch (InterruptedException e) {
                logger.info("Failed Sleep for CharlesJob of class: " + getClass().getSimpleName());
            } catch (IOException e) {
                logger.info("Failed Http Request for CharlesJob of class: " + getClass().getSimpleName());
            }
        }
    }
}
