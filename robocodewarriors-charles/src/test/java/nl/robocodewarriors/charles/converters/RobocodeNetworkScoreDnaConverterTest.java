package nl.robocodewarriors.charles.converters;

import com.google.common.collect.Lists;

import nl.robocodewarriors.charles.domain.NetworkScore;
import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.neuralnetwork.Layer;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.neuralnetwork.Neuron;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RobocodeNetworkScoreDnaConverterTest  {


    private RobocodeDnaConverter robocodeDnaConverter;

    @Before
    public void before() {
        robocodeDnaConverter = new RobocodeDnaConverter();
    }

    @Test
    public void testListToDna() {
        List<Neuron> layerOneNeurons = Lists.newArrayList(mock(Neuron.class), mock(Neuron.class));
        List<Neuron> layerTwoNeurons = Lists.newArrayList(mock(Neuron.class), mock(Neuron.class));

        Network network = mock(Network.class);
        Layer layer1 = mock(Layer.class);
        when(layer1.getNeurons()).thenReturn(layerOneNeurons);
        Layer layer2 = mock(Layer.class);
        when(layer2.getNeurons()).thenReturn(layerTwoNeurons);

        List<Layer> layers = Lists.newArrayList(layer1, layer2);
        when(network.getLayers()).thenReturn(layers);
        NetworkScore first = mock(NetworkScore.class);
        when(first.getScore()).thenReturn(10);
        when(first.getNetwork()).thenReturn(network);

        List<NetworkDna> networkDnaList = robocodeDnaConverter.convertToDna(Lists.newArrayList(first));
        assertThat(networkDnaList.size(), is(1));
        assertThat(networkDnaList.get(0).getScore(), is(10));
        assertThat(networkDnaList.get(0).getTotalSize(), is(4));
        assertThat(networkDnaList.get(0).getNeurons().get(0).containsAll(layerOneNeurons), is(true));
        assertThat(networkDnaList.get(0).getNeurons().get(1).containsAll(layerTwoNeurons), is(true));

    }
    @Test
    public void testToDna() {
        List<Neuron> layerOneNeurons = Lists.newArrayList(mock(Neuron.class), mock(Neuron.class));
        List<Neuron> layerTwoNeurons = Lists.newArrayList(mock(Neuron.class), mock(Neuron.class));

        Network network = mock(Network.class);
        Layer layer1 = mock(Layer.class);
        when(layer1.getNeurons()).thenReturn(layerOneNeurons);
        Layer layer2 = mock(Layer.class);
        when(layer2.getNeurons()).thenReturn(layerTwoNeurons);

        List<Layer> layers = Lists.newArrayList(layer1, layer2);
        when(network.getLayers()).thenReturn(layers);
        NetworkScore first = mock(NetworkScore.class);
        when(first.getScore()).thenReturn(10);
        when(first.getNetwork()).thenReturn(network);

        NetworkDna networkDna = robocodeDnaConverter.convertToDna(first);
        assertThat(networkDna.getScore(), is(10));
        assertThat(networkDna.getTotalSize(), is(4));
        assertThat(networkDna.getNeurons().get(0).containsAll(layerOneNeurons), is(true));
        assertThat(networkDna.getNeurons().get(1).containsAll(layerTwoNeurons), is(true));
    }

    @Test
    public void testToNetwork() {
        List<List<Neuron>> neuronList = Lists.newArrayList();
        neuronList.add(Lists.newArrayList(mock(Neuron.class)));
        neuronList.add(Lists.newArrayList(mock(Neuron.class), mock(Neuron.class)));
        neuronList.add(Lists.newArrayList(mock(Neuron.class), mock(Neuron.class), mock(Neuron.class)));

        NetworkDna networkDna = mock(NetworkDna.class);
        when(networkDna.getNeurons()).thenReturn(neuronList);
        Network network = robocodeDnaConverter.convertToNetwork(networkDna);

        assertThat(network.getLayers().size(), is(3));
        assertThat(network.getLayers().get(0).getNeurons().size(), is(1));
        assertThat(network.getLayers().get(0).getNeurons().containsAll(neuronList.get(0)), is(true));
        assertThat(network.getLayers().get(1).getNeurons().size(), is(2));
        assertThat(network.getLayers().get(1).getNeurons().containsAll(neuronList.get(1)), is(true));
        assertThat(network.getLayers().get(2).getNeurons().size(), is(3));
        assertThat(network.getLayers().get(2).getNeurons().containsAll(neuronList.get(2)), is(true));
    }

    @Test
    public void testToNetworkList() {
        List<List<Neuron>> neuronList = Lists.newArrayList();
        neuronList.add(Lists.newArrayList(mock(Neuron.class)));
        neuronList.add(Lists.newArrayList(mock(Neuron.class), mock(Neuron.class)));
        neuronList.add(Lists.newArrayList(mock(Neuron.class), mock(Neuron.class), mock(Neuron.class)));

        NetworkDna networkDna = mock(NetworkDna.class);
        when(networkDna.getNeurons()).thenReturn(neuronList);
        List<Network> networkList = robocodeDnaConverter.convertToNetwork(Lists.newArrayList(networkDna));

        assertThat(networkList.size(), is(1));
        assertThat(networkList.get(0).getLayers().size(), is(3));
        assertThat(networkList.get(0).getLayers().get(0).getNeurons().size(), is(1));
        assertThat(networkList.get(0).getLayers().get(0).getNeurons().containsAll(neuronList.get(0)), is(true));
        assertThat(networkList.get(0).getLayers().get(1).getNeurons().size(), is(2));
        assertThat(networkList.get(0).getLayers().get(1).getNeurons().containsAll(neuronList.get(1)), is(true));
        assertThat(networkList.get(0).getLayers().get(2).getNeurons().size(), is(3));
        assertThat(networkList.get(0).getLayers().get(2).getNeurons().containsAll(neuronList.get(2)), is(true));
    }


}
