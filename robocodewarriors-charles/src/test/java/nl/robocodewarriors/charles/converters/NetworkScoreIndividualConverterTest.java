package nl.robocodewarriors.charles.converters;

import nl.robocodewarriors.charles.domain.NetworkScore;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.neuralnetworkutils.serialization.NetworkSerializer;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NetworkScoreIndividualConverterTest {

    private NetworkScoreIndividualConverter converter;
    private NetworkSerializer serializer;

    @Before
    public void before() {
        serializer = mock(NetworkSerializer.class);
        converter = new NetworkScoreIndividualConverter(serializer);
    }

    @Test
    public void testConvert() {
        int score = 12345;
        NetworkScore networkScore = mock(NetworkScore.class);
        Network network = mock(Network.class);

        when(networkScore.getScore()).thenReturn(score);
        when(networkScore.getNetwork()).thenReturn(network);
        when(serializer.serializeNetwork(network)).thenReturn("abc");

        Individual individual = converter.convert(networkScore);
        assertThat(individual.getNetworkString(), is("abc"));
        assertThat(individual.getId(), is(nullValue()));
        assertThat(individual.getScore(), is(score));
    }
}
