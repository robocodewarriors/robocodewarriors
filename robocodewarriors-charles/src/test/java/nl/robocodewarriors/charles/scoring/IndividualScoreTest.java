package nl.robocodewarriors.charles.scoring;

import nl.robocodewarriors.database.domain.Individual;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class IndividualScoreTest {

    private IndividualScore individualScore;
    private Individual individual;

    @Before
    public void before() {
        individual = mock(Individual.class);
        individualScore = new IndividualScore(individual, 5);
    }

    @Test
    public void testGetIndividual() {
        assertThat(individualScore.getIndividual(), is(individual));
    }

    @Test
    public void testGetScore() {
        assertThat(individualScore.getScore(), is(5));
    }

    @Test
    public void testSetScore() {
        individualScore.setScore(6);
        assertThat(individualScore.getScore(), is(6));
    }

    @Test
    public void testEquals() {
        IndividualScore other1 = new IndividualScore(individual, 6);
        IndividualScore other2 = new IndividualScore(mock(Individual.class), 5);
        IndividualScore other3 = new IndividualScore(mock(Individual.class), 6);
        IndividualScore other4 = new IndividualScore(individual, 5);

        assertThat(individualScore.equals(other1), is(false));
        assertThat(individualScore.equals(other2), is(false));
        assertThat(individualScore.equals(other3), is(false));
        assertThat(individualScore.equals(other4), is(true));
    }


}
