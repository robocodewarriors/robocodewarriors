package nl.robocodewarriors.charles.scoring;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.Individual;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;

public class RobocodeScoreNormalizerTest {

    private RobocodeScoreNormalizer robocodeScoreNormalizer;

    @Before
    public void before() {
        robocodeScoreNormalizer = new RobocodeScoreNormalizer();
    }

    @Test
    public void testNormalize() {
        Individual individual1 = mock(Individual.class);
        Individual individual2 = mock(Individual.class);
        Individual individual3 = mock(Individual.class);
        when(individual1.getScore()).thenReturn(320);
        when(individual2.getScore()).thenReturn(280);
        when(individual3.getScore()).thenReturn(280);

        IndividualScore individualScore1 = mock(IndividualScore.class);
        IndividualScore individualScore2 = mock(IndividualScore.class);
        IndividualScore individualScore3 = mock(IndividualScore.class);
        when(individualScore1.getIndividual()).thenReturn(individual1);
        when(individualScore2.getIndividual()).thenReturn(individual2);
        when(individualScore3.getIndividual()).thenReturn(individual3);

        List<IndividualScore> individualScores = Lists.newArrayList(individualScore1, individualScore2, individualScore3);
        robocodeScoreNormalizer.normalize(individualScores);

        verify(individualScore1).setScore(41);
        verify(individualScore2).setScore(1);
        verify(individualScore3).setScore(1);

    }
}
