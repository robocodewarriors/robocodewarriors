package nl.robocodewarriors.charles.scoring;

import com.google.common.collect.Maps;

import org.junit.Before;
import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ScoreCalculatorTest {

    private ScoreCalculator calculator;
    private ScriptEngine scriptEngine;

    @Before
    public void before() {
        scriptEngine = mock(ScriptEngine.class);
        calculator =new ScoreCalculator(scriptEngine);
    }

    @Test
    public void testCalculate() throws ScriptException, ScoreCalculatorException {
        String calculation = "{a} + {b}";
        Map<String, Integer> data = Maps.newHashMap();
        data.put("a", 3);
        data.put("b", 4);
        when(scriptEngine.eval("3 + 4")).thenReturn(7);

        Integer result = calculator.calculateScore(calculation, data);
        assertThat(result, is(7));
    }

    @Test
    public void testThrows() throws ScriptException {
        when(scriptEngine.eval(isA(String.class))).thenThrow(ScriptException.class);
        try {
            Map<String, Integer> data = Maps.newHashMap();
            calculator.calculateScore("calculation", data);
            fail();
        } catch (ScoreCalculatorException e) {
            assertThat(e.getCalculationString(), is("calculation"));
        }
    }
}
