package nl.robocodewarriors.charles.context;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ConfigurationExceptionTest {

    @Test
    public void testMessageConstructor() {
        ConfigurationException e = new ConfigurationException("message");
        assertThat(e.getMessage(), is("message"));
    }

    @Test
    public void testMessageAndThrowableConstructor() {
        Throwable t =  mock(Throwable.class);
        ConfigurationException e = new ConfigurationException("message", t);
        assertThat(e.getMessage(), is("message"));
        assertThat(e.getCause(), is(t));
    }

}
