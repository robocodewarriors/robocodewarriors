package nl.robocodewarriors.charles.genetics.mutationcrossover;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.Dna;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class InvalidCumulativeScoreExceptionTest {

    @Test
    public void testConstructor() {
        List<Dna> list = Lists.newArrayList(mock(Dna.class), mock(Dna.class));

        InvalidCumulativeScoreException e = new InvalidCumulativeScoreException(list, 20, 30);
        assertThat(e.getCumulativeScore(), is(20));
        assertThat(e.getOffset(), is(30));
        assertThat(e.getDna(), is(list));
    }
}
