package nl.robocodewarriors.charles.genetics.domain.impl;

import com.google.common.collect.Lists;

import nl.robocodewarriors.neuralnetwork.Neuron;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NetworkDnaTest {

    @Test
    public void testGetNetwork() {
        List<List<Neuron>> neurons = new ArrayList<List<Neuron>>();
        NetworkDna dna = new NetworkDna(neurons);
        Assert.assertEquals(neurons, dna.getNeurons());
    }

    @Test
    public void testSetAndGetScore() {
        NetworkDna dna = new NetworkDna(null);
        assertEquals(0, dna.getScore());
        dna.setScore(1);
        assertEquals(1, dna.getScore());
    }

    @Test
    public void testGetTotalSize() {
        assertEquals(3, createDna().getTotalSize());
    }

    private Neuron createNeuronMock(Double threshold, String key, List<Double> weights) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.getKey()).thenReturn(key);
        when(neuron.getThreshold()).thenReturn(threshold);
        when(neuron.getWeights()).thenReturn(weights);
        return neuron;
    }

    @SuppressWarnings("unchecked")
    private NetworkDna createDna() {
        List<Neuron> inputNeurons = Arrays.asList(createNeuronMock(0.5, "test", new ArrayList<Double>()));
        List<Neuron> hiddenNeurons = Arrays.asList(createNeuronMock(0.6, null, new ArrayList<Double>()));
        List<Neuron> outputNeurons = Arrays.asList(createNeuronMock(0.7, "test", new ArrayList<Double>()));

        NetworkDna dna = mock(NetworkDna.class);
        when(dna.getNeurons()).thenReturn(Arrays.asList(inputNeurons, hiddenNeurons, outputNeurons));
        when(dna.getTotalSize()).thenReturn(3);

        return dna;
    }

    @Test
    public void testTotalSize() {
        List<Neuron> first = Lists.newArrayList(mock(Neuron.class), mock(Neuron.class));
        List<Neuron> second = Lists.newArrayList(mock(Neuron.class), mock(Neuron.class), mock(Neuron.class));
        List<List<Neuron>> total = Lists.newArrayList(first, second);
        NetworkDna networkDna = new NetworkDna(total);
        assertThat(networkDna.getTotalSize(), is(5));
    }
}
