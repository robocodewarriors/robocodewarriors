package nl.robocodewarriors.charles.genetics.mutationcrossover;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.Dna;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class BaseCrossOverMutationAlgorithmTest {

    private MockAlgorithm mockAlgorithm;
    private Random random;
    private Crossover<CrossoverResult<Dna>, Dna> crossoverApplier;
    private Mutation<Dna> mutationApplier;
    private int crossoverRate = 60;
    private int mutationRate = 50;

    @Before
    public void before() {
        crossoverApplier = mock(Crossover.class);
        mutationApplier = mock(Mutation.class);

        mockAlgorithm = new MockAlgorithm(crossoverApplier, mutationApplier);

        assertThat(mockAlgorithm.getRandom(), is(notNullValue()));

        random = mock(Random.class);
        Whitebox.setInternalState(mockAlgorithm, "random", random);
        assertThat(mockAlgorithm.getRandom(), is(random));
    }

    @Test
    public void testNextGeneration() {
        Dna first = mock(Dna.class);
        Dna second = mock(Dna.class);

        Dna crossoverOutputA = mock(Dna.class);
        Dna crossoverOutputB = mock(Dna.class);

        CrossoverResult crossoverResult = mock(CrossoverResult.class);
        when(crossoverResult.getResultA()).thenReturn(crossoverOutputA);
        when(crossoverResult.getResultB()).thenReturn(crossoverOutputB);

        when(first.getScore()).thenReturn(5);
        when(second.getScore()).thenReturn(2);
        List<Dna> dnaList = Lists.newArrayList(first, second, first, second);

        when(random.nextInt(7)).thenReturn(3, 2, 1, 6);

        when(crossoverApplier.apply(first, first, random, crossoverRate)).thenReturn(crossoverResult);
        when(crossoverApplier.apply(first, second, random, crossoverRate)).thenReturn(crossoverResult);

        List<Dna> newList = mockAlgorithm.nextGeneration(dnaList, crossoverRate, mutationRate);

        verify(mutationApplier, times(2)).apply(crossoverOutputA, random, mutationRate);
        verify(mutationApplier, times(2)).apply(crossoverOutputB, random, mutationRate);

        assertThat(newList.size(), is(4));
        assertThat(newList.get(0), is(crossoverOutputA));
        assertThat(newList.get(1), is(crossoverOutputB));
        assertThat(newList.get(2), is(crossoverOutputA));
        assertThat(newList.get(3), is(crossoverOutputB));
    }

    @Test
    public void testWithCumulativeScoreThatIsZero() {
        Dna first = mock(Dna.class);
        Dna second = mock(Dna.class);

        Dna crossoverOutputA = mock(Dna.class);
        Dna crossoverOutputB = mock(Dna.class);

        CrossoverResult crossoverResult = mock(CrossoverResult.class);
        when(crossoverResult.getResultA()).thenReturn(crossoverOutputA);
        when(crossoverResult.getResultB()).thenReturn(crossoverOutputB);

        when(first.getScore()).thenReturn(0);
        when(second.getScore()).thenReturn(0);
        List<Dna> dnaList = Arrays.asList(first, second);

        when(crossoverApplier.apply(first, first, random, crossoverRate)).thenReturn(crossoverResult);

        List<Dna> newList = mockAlgorithm.nextGeneration(dnaList, crossoverRate, mutationRate);

        verify(crossoverApplier).apply(first, first, random, crossoverRate);
        verify(random, never()).nextInt(isA(Integer.class));

        verify(mutationApplier).apply(crossoverOutputA, random, mutationRate);
        verify(mutationApplier).apply(crossoverOutputB, random, mutationRate);

        assertThat(newList.size(), is(2));
        assertThat(newList.get(0), is(crossoverOutputA));
        assertThat(newList.get(1), is(crossoverOutputB));
    }

    @Test
    public void testExceptionOnErrorWithCumulative() {
        Dna first = mock(Dna.class);
        Dna second = mock(Dna.class);

        when(first.getScore()).thenReturn(5);
        when(second.getScore()).thenReturn(2);
        List<Dna> dnaList = Lists.newArrayList(first, second);

        when(random.nextInt(7)).thenReturn(8);
        try {
            mockAlgorithm.nextGeneration(dnaList, crossoverRate, mutationRate);
        } catch (InvalidCumulativeScoreException e) {
            verify(random, times(1)).nextInt(isA(Integer.class));
            assertThat(e.getCumulativeScore(), is(7));
            assertThat(e.getOffset(), is(8));
            assertThat(e.getDna(), is(dnaList));
        }

    }

    private class MockAlgorithm extends BaseCrossOverMutationAlgorithm<CrossoverResult<Dna>, Dna> {

        public MockAlgorithm(Crossover<CrossoverResult<Dna>, Dna> crossoverApplier, Mutation<Dna> mutationApplier) {
            super(crossoverApplier, mutationApplier);
        }
    }
}
