package nl.robocodewarriors.charles.genetics.mutationcrossover.impl;

import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.charles.genetics.mutationcrossover.Mutation;
import nl.robocodewarriors.neuralnetwork.Neuron;
import nl.robocodewarriors.neuralnetwork.NeuronBuilder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class NetworkMutationTest {

    @Mock
    private NeuronBuilder neuronBuilder;
    @Mock
    private Random random;
    @Captor
    private ArgumentCaptor<ArrayList<Double>> weightsCaptor;
    @Captor
    private ArgumentCaptor<Double> thresholdCaptor;
    @Captor
    private ArgumentCaptor<String> keyCaptor;
    private Mutation<NetworkDna> instance;
    private List<Double> weights = Arrays.asList(0.1, 0.2, 0.3);

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        instance = new NetworkMutation(neuronBuilder);
    }

    @Test
    public void testMutate() {
        int mutationRate = 50;

        when(random.nextInt(anyInt())).thenReturn(100, 50, 51, 0, 40, 23, 23, 65, 69);
        when(random.nextDouble()).thenReturn(0.3, 0.8, 0.8, 0.8, 0.8);

        NetworkDna dna = createDna();
        instance.apply(dna, random, mutationRate);

        verify(neuronBuilder, times(3)).build(keyCaptor.capture(), thresholdCaptor.capture(), weightsCaptor.capture());
        assertStringList(keyCaptor.getAllValues(), "test", null, "test");
        assertDoubleList(thresholdCaptor.getAllValues(), 0.5, .52, 0.82);
        List<ArrayList<Double>> weightsResults = weightsCaptor.getAllValues();
        assertDoubleList(weightsResults.get(0));
        assertDoubleList(weightsResults.get(1), 0.1, 0.32, 0.42);
        assertDoubleList(weightsResults.get(2), 0.22, 0.2, 0.3);
    }

    private Neuron createNeuronMock(Double threshold, String key, List<Double> weights) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.getKey()).thenReturn(key);
        when(neuron.getThreshold()).thenReturn(threshold);
        when(neuron.getWeights()).thenReturn(weights);
        return neuron;
    }

    @SuppressWarnings("unchecked")
    private NetworkDna createDna() {
        List<Neuron> inputNeurons = Arrays.asList(createNeuronMock(0.5, "test", new ArrayList<Double>()));
        List<Neuron> hiddenNeurons = Arrays.asList(createNeuronMock(0.6, null, weights));
        List<Neuron> outputNeurons = Arrays.asList(createNeuronMock(0.7, "test", weights));

        NetworkDna dna = mock(NetworkDna.class);
        when(dna.getNeurons()).thenReturn(Arrays.asList(inputNeurons, hiddenNeurons, outputNeurons));
        when(dna.getTotalSize()).thenReturn(3);

        return dna;
    }

    private void assertStringList(List<String> stringList, String... expected) {
        List<String> expectedList = Arrays.asList(expected);
        for (int i = 0; i < stringList.size(); i++) {
            assertEquals(expectedList.get(i), stringList.get(i));
        }
    }

    private void assertDoubleList(List<Double> doubleList, Double... expected) {
        List<Double> expectedResult = Arrays.asList(expected);
        for (int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), roundDouble(doubleList.get(i)));

        }
    }

    private Double roundDouble(Double d) {
        int temp = (int) Math.round(d * 100);
        return ((double) temp) / 100.0;
    }
}
