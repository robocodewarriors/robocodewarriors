package nl.robocodewarriors.charles.genetics.mutationcrossover.impl;

import nl.robocodewarriors.charles.genetics.domain.impl.NetworkDna;
import nl.robocodewarriors.neuralnetwork.Neuron;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NetworkCrossoverTest {

    private NetworkCrossover instance;
    Random random;

    @Before
    public void setUp() {
        random = mock(Random.class);
        instance = new NetworkCrossover();
    }

    @Test
    public void testCrossover() {
        when(random.nextInt(anyInt())).thenReturn(40, 2);

        NetworkDna dnaA = createDnaA();
        NetworkDna dnaB = createDnaB();

        NetworkCrossoverResult result = instance.apply(dnaA, dnaB, random, 60);

        assertEquals(dnaA.getNeurons().get(0).get(0), result.getResultA().getNeurons().get(0).get(0));
        assertEquals(dnaA.getNeurons().get(1).get(0), result.getResultA().getNeurons().get(1).get(0));
        assertEquals(dnaB.getNeurons().get(2).get(0), result.getResultA().getNeurons().get(2).get(0));

        assertEquals(dnaB.getNeurons().get(0).get(0), result.getResultB().getNeurons().get(0).get(0));
        assertEquals(dnaB.getNeurons().get(1).get(0), result.getResultB().getNeurons().get(1).get(0));
        assertEquals(dnaA.getNeurons().get(2).get(0), result.getResultB().getNeurons().get(2).get(0));
    }
    
    @Test
    public void testCrossoverNoCrossover() {
        when(random.nextInt(anyInt())).thenReturn(80);

        NetworkDna dnaA = createDnaA();
        NetworkDna dnaB = createDnaB();

        NetworkCrossoverResult result = instance.apply(dnaA, dnaB, random, 60);

        assertEquals(dnaB.getNeurons().get(0).get(0), result.getResultA().getNeurons().get(0).get(0));
        assertEquals(dnaB.getNeurons().get(1).get(0), result.getResultA().getNeurons().get(1).get(0));
        assertEquals(dnaB.getNeurons().get(2).get(0), result.getResultA().getNeurons().get(2).get(0));

        assertEquals(dnaA.getNeurons().get(0).get(0), result.getResultB().getNeurons().get(0).get(0));
        assertEquals(dnaA.getNeurons().get(1).get(0), result.getResultB().getNeurons().get(1).get(0));
        assertEquals(dnaA.getNeurons().get(2).get(0), result.getResultB().getNeurons().get(2).get(0));
    }

    private Neuron createNeuronMock(Double threshold, String key, List<Double> weights) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.getKey()).thenReturn(key);
        when(neuron.getThreshold()).thenReturn(threshold);
        when(neuron.getWeights()).thenReturn(weights);
        return neuron;
    }

    @SuppressWarnings("unchecked")
    private NetworkDna createDnaA() {
        List<Double> weights = Arrays.asList(0.1, 0.2, 0.3);

        List<Neuron> inputNeurons = Arrays.asList(createNeuronMock(0.5, "test", new ArrayList<Double>()));
        List<Neuron> hiddenNeurons = Arrays.asList(createNeuronMock(0.6, null, weights));
        List<Neuron> outputNeurons = Arrays.asList(createNeuronMock(0.7, "test", weights));

        NetworkDna dna = mock(NetworkDna.class);
        when(dna.getNeurons()).thenReturn(Arrays.asList(inputNeurons, hiddenNeurons, outputNeurons));
        when(dna.getTotalSize()).thenReturn(3);

        return dna;
    }

    @SuppressWarnings("unchecked")
    private NetworkDna createDnaB() {
        List<Double> weights = Arrays.asList(0.1, 0.2, 0.3);

        List<Neuron> inputNeurons = Arrays.asList(createNeuronMock(0.8, "test", new ArrayList<Double>()));
        List<Neuron> hiddenNeurons = Arrays.asList(createNeuronMock(0.9, null, weights));
        List<Neuron> outputNeurons = Arrays.asList(createNeuronMock(1.0, "test", weights));

        NetworkDna dna = mock(NetworkDna.class);
        when(dna.getNeurons()).thenReturn(Arrays.asList(inputNeurons, hiddenNeurons, outputNeurons));
        when(dna.getTotalSize()).thenReturn(3);

        return dna;
    }
}
