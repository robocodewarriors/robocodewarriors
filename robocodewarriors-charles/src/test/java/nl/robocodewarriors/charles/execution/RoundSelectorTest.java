package nl.robocodewarriors.charles.execution;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RoundSelectorTest {

    private RoundSelector<Integer> roundSelector;

    @Test
    public void testGetNextRobotGroup() {
        List<Integer> input = Arrays.asList(new Integer[] { 1, 2, 3, 4, 5 });
        
        roundSelector = new RoundSelector<Integer>(input, 5, 5);
        
        for(int i = 0; i < 5; i++) {
            List<Integer> group = roundSelector.getNextRobotGroup();
            assertTrue(group.containsAll(input));
            assertEquals(input.size(), group.size());
        }
        
        assertFalse(roundSelector.hasMoreGroups());
    }
    
    @Test(expected=NoGroupException.class)
    public void testGetToMuchGroups() {
        List<Integer> input = Arrays.asList(new Integer[] { 1, 2, 3 });
        
        roundSelector = new RoundSelector<Integer>(input, 3, 3);
        
        for(int i = 0; i < 4; i++) {
            List<Integer> group = roundSelector.getNextRobotGroup();
            assertTrue(group.containsAll(input));
            assertEquals(input.size(), group.size());
        }
    }
}
