package nl.robocodewarriors.charles.execution;

import com.google.common.collect.Lists;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RoundSelectorFactoryTest {

    @Test
    public void testCreate() {
        RoundSelectorFactory factory = new RoundSelectorFactory();
        ArrayList<String> list = Lists.newArrayList("string5", "string4", "string3", "string2", "string1");
        RoundSelector<String> roundSelector = factory.create(list, 5, 5);
        assertThat(roundSelector.getNextRobotGroup().contains("string1"), is(true));
    }

}
