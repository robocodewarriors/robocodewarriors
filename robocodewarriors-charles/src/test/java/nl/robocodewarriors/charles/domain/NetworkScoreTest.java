package nl.robocodewarriors.charles.domain;

import nl.robocodewarriors.neuralnetwork.Network;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class NetworkScoreTest {

    @Test
    public void testConstructor() {
        Network network = mock(Network.class);
        Integer score = 50;
        NetworkScore networkScore = new NetworkScore(network, score);
        assertThat(networkScore.getNetwork(),is(network));
        assertThat(networkScore.getScore(),is(score));
    }

    @Test
    public void testEquals() {
        Network network = mock(Network.class);
        Integer score = 50;
        NetworkScore networkScore = new NetworkScore(network, score);

        NetworkScore other1 = new NetworkScore(network, 51);
        NetworkScore other2 = new NetworkScore(mock(Network.class), 51);
        NetworkScore other3 = new NetworkScore(mock(Network.class), 50);
        NetworkScore other4 = new NetworkScore(network, 50);

        assertThat(networkScore.equals(other1), is(false));
        assertThat(networkScore.equals(other2), is(false));
        assertThat(networkScore.equals(other3), is(false));
        assertThat(networkScore.equals(other4), is(true));

    }
}
