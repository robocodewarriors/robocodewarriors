apt-get update

apt-get install openssh-server openjdk-7-jdk maven git

cp charles_dsa.pub /home/charles/.ssh/authorized_keys

ntpdate ntp.ubuntu.com

update-alternatives --config java /usr/lib/jvm/java-7-openjdk-i386/jre/bin/java 

mkdir /etc/executor
wget http://192.168.1.1:9090/rest/public/datasource.properties -O /etc/executor/datasource.properties

cp executor /etc/init.d/executor
update-rc.d executor defaults 

cp crontab /var/spool/cron/crontabs/root

service executor start
