package nl.robocodewarriors.neuralnetworkutils.serialization;

import nl.robocodewarriors.neuralnetwork.Network;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class NetworkSerializerTest {

    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private Network network;
    private NetworkSerializer instance;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        instance = new NetworkSerializer(objectMapper);
    }

    @Test
    public void testSerializeNetwork() throws IOException {
        when(objectMapper.writeValueAsString(network)).thenReturn("JSON");

        assertEquals("JSON", instance.serializeNetwork(network));
    }

    @Test(expected = NetworkSerializationException.class)
    public void testSerializeNetworkFailed() throws IOException {
        when(objectMapper.writeValueAsString(network)).thenThrow(JsonParseException.class);

        instance.serializeNetwork(network);
    }
}