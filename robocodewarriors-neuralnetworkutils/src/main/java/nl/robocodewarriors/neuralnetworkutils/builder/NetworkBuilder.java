package nl.robocodewarriors.neuralnetworkutils.builder;

import com.google.common.collect.Lists;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.neuralnetwork.NetworkBuilderImpl;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class NetworkBuilder {

    public List<Network> buildNetworks(Set<String> inputs, Set<String> outputs, Integer numberOfNetworks, Integer numberOrHiddenLayers, Integer hiddenLayerSize) {

        List<Network> networkList = new ArrayList<Network>(numberOfNetworks);

        List<Integer> hiddenLayers = Lists.newArrayList();
        for (int i = 0; i < numberOrHiddenLayers; i++) {
            hiddenLayers.add(hiddenLayerSize);
        }
        Integer[] integerArray = new Integer[numberOrHiddenLayers];

        for (int i = 0; i < numberOfNetworks; i++) {
            networkList.add(new NetworkBuilderImpl()
                    .inputs(inputs.toArray(new String[inputs.size()]))
                    .hiddenLayers(hiddenLayers.toArray(integerArray))
                    .outputs(outputs.toArray(new String[outputs.size()])).build());


        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValueAsString(networkList.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return networkList;
    }
}
