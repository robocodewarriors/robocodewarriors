package nl.robocodewarriors.neuralnetworkutils.serialization;

import nl.robocodewarriors.neuralnetwork.Network;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NetworkSerializer {

    private final ObjectMapper objectMapper;

    @Autowired
    public NetworkSerializer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String serializeNetwork(Network network) {
        try {
            return objectMapper.writeValueAsString(network);
        } catch (IOException ex) {
            throw new NetworkSerializationException(ex);
        }
    }

    public Network createNetwork(String network) {
        try {
            return objectMapper.readValue(network, Network.class);
        } catch (JsonMappingException e) {
            throw new NetworkSerializationException(e);
        } catch (JsonParseException e) {
            throw new NetworkSerializationException(e);
        } catch (IOException e) {
            throw new NetworkSerializationException(e);
        }
    }
}
