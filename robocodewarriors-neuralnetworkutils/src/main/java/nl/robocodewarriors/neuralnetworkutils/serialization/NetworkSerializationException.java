package nl.robocodewarriors.neuralnetworkutils.serialization;

public class NetworkSerializationException extends RuntimeException {

    public NetworkSerializationException(Throwable cause) {
        super(cause);
    }

}
