package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public abstract class BaseValueActor implements ValueActor {

    protected final ValueNormalizer valueNormalizer;

    public BaseValueActor(ValueNormalizer valueNormalizer) {
        this.valueNormalizer = valueNormalizer;
    }

    protected Double valueOrZero(Map<String, Double> map, String key) {
        if (map.containsKey(key)) {
            return map.get(key);
        }
        return 0.0;
    }
}
