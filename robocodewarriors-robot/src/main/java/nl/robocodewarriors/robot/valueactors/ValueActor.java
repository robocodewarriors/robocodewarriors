package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;

import java.util.Map;

public interface ValueActor {

    void act(NeuralRobot robot, Map<String, Double> data);
}
