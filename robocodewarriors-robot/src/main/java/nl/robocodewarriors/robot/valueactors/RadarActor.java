package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public class RadarActor extends BaseValueActor {


    public RadarActor(ValueNormalizer valueNormalizer) {
        super(valueNormalizer);
    }

    @Override
    public void act(NeuralRobot robot, Map<String, Double> data) {
        if (valueOrZero(data, NeuralRobot.OUTPUT_RADAR_TURN_LEFT) > 0 && valueOrZero(data, NeuralRobot.OUTPUT_RADAR_TURN_RIGHT) == 0) {
            robot.setTurnRadarRight(-45.0);
        } else if (valueOrZero(data, NeuralRobot.OUTPUT_RADAR_TURN_RIGHT) > 0 && valueOrZero(data, NeuralRobot.OUTPUT_RADAR_TURN_LEFT) == 0) {
            robot.setTurnRadarRight(45.0);
        } else {
            robot.setTurnRadarRight(0.0);
        }
    }
}
