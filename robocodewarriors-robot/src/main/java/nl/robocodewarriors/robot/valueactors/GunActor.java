package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public class GunActor extends BaseValueActor {

    public GunActor(ValueNormalizer valueNormalizer) {
        super(valueNormalizer);
    }

    @Override
    public void act(NeuralRobot robot, Map<String, Double> data) {
        if (valueOrZero(data, NeuralRobot.OUTPUT_GUN_TURN_LEFT) > 0 && valueOrZero(data, NeuralRobot.OUTPUT_GUN_TURN_RIGHT) == 0) {
            robot.setTurnGunRight(-10.0);
        } else if (valueOrZero(data, NeuralRobot.OUTPUT_GUN_TURN_RIGHT) > 0 && valueOrZero(data, NeuralRobot.OUTPUT_GUN_TURN_LEFT) == 0) {
            robot.setTurnGunRight(10.0);
        } else {
            robot.setTurnGunRight(0.0);
        }

        if (valueOrZero(data, NeuralRobot.OUTPUT_FIRE) > 0) {
            robot.fire(1.0);
        }
    }

}