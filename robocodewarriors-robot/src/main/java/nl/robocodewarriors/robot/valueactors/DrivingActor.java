package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public class DrivingActor extends BaseValueActor {

    private double maximumVelocity;

    public DrivingActor(ValueNormalizer valueNormalizer) {
        super(valueNormalizer);
    }

    @Override
    public void act(NeuralRobot robot, Map<String, Double> data) {
        actOnDrive(robot, data);
        actOnTurning(robot, data);
        actOnVelocity(robot, data);
    }

    private void actOnVelocity(NeuralRobot robot, Map<String, Double> data) {
        if (valueOrZero(data, NeuralRobot.OUTPUT_SHOULD_ACCELERATE) > 0.0 && valueOrZero(data, NeuralRobot.OUTPUT_SHOULD_DECELERATE) == 0.0) {
            increaseVelocity();
        } else if (valueOrZero(data, NeuralRobot.OUTPUT_SHOULD_DECELERATE) > 0.0 && valueOrZero(data, NeuralRobot.OUTPUT_SHOULD_ACCELERATE) == 0.0) {
            decreaseVelocity();
        }
        robot.setMaxVelocity(maximumVelocity);
    }

    private void decreaseVelocity() {
        maximumVelocity -= 1.0;
        if (maximumVelocity < 0) {
            maximumVelocity = 0.0;
        }
    }

    private void increaseVelocity() {
        maximumVelocity += 1.0;
        if (maximumVelocity > 8.0) {
            maximumVelocity = 8.0;
        }

    }

    private void actOnDrive(NeuralRobot neuralRobot, Map<String, Double> data) {
        if (valueOrZero(data, NeuralRobot.OUTPUT_DRIVE_AHEAD) > 0.0 && valueOrZero(data, NeuralRobot.OUTPUT_DRIVE_BACKWARDS) == 0.0) {
            neuralRobot.setAhead(100.0);
        } else if (valueOrZero(data, NeuralRobot.OUTPUT_DRIVE_BACKWARDS) > 0.0 && valueOrZero(data, NeuralRobot.OUTPUT_DRIVE_AHEAD) == 0.0) {
            neuralRobot.setAhead(-100.0);
        } else {
            neuralRobot.setAhead(0.0);
        }
    }

    private void actOnTurning(NeuralRobot neuralRobot, Map<String, Double> data) {
        if (valueOrZero(data, NeuralRobot.OUTPUT_ROBOT_TURN_LEFT) > 0.0 && valueOrZero(data, NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT) == 0.0) {
            neuralRobot.setTurnRight(-20.0);
        } else if (valueOrZero(data, NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT) > 0.0 && valueOrZero(data, NeuralRobot.OUTPUT_ROBOT_TURN_LEFT) == 0.0) {
            neuralRobot.setTurnRight(20.0);
        } else {
            neuralRobot.setTurnRight(0.0);
        }
    }

    public void setMaximumVelocity(double maximumVelocity) {
        this.maximumVelocity = maximumVelocity;
    }

    public double getMaximumVelocity() {
        return maximumVelocity;
    }
}
