package nl.robocodewarriors.robot.context;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.neuralrobot.RobotRunner;
import nl.robocodewarriors.robot.valueactors.DrivingActor;
import nl.robocodewarriors.robot.valueactors.GunActor;
import nl.robocodewarriors.robot.valueactors.RadarActor;
import nl.robocodewarriors.robot.valueactors.ValueActor;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import nl.robocodewarriors.robot.valuescalculators.*;

import java.util.ArrayList;
import java.util.List;

public class Container {

    private RobotRunner runner;
    private List<ValueActor> actors;
    private List<BaseValuesCalculator> calculators;
    private ValueNormalizer valueNormalizer;
    private DegreesNormalizer degreesNormalizer;
    private NeuralRobot robot;

    public Container(NeuralRobot robot) {
        this.robot = robot;

        valueNormalizer = new ValueNormalizer();
        degreesNormalizer = new DegreesNormalizer(valueNormalizer);
        runner = new RobotRunner(actors(), calculators(), robot);
    }

    public RobotRunner getRobotRunner() {
        return runner;
    }

    private List<BaseValuesCalculator> calculators() {
        calculators = new ArrayList<BaseValuesCalculator>();

        calculators.add(new HitRobotCalculator(valueNormalizer, degreesNormalizer));
        calculators.add(new HitWallCalculator(valueNormalizer, degreesNormalizer));
        calculators.add(new NearWallCalculator(valueNormalizer, degreesNormalizer));

        calculators.add(new PhysicsCalculator(valueNormalizer, degreesNormalizer));
        calculators.add(new GunCalculator(valueNormalizer, degreesNormalizer));
        calculators.add(new ScannedRobotCalculator(valueNormalizer, degreesNormalizer));
        calculators.add(new HitByBulletCalculator(valueNormalizer, degreesNormalizer));

        return calculators;
    }

    private List<ValueActor> actors() {
        actors = new ArrayList<ValueActor>();
        actors.add(new DrivingActor(valueNormalizer));
        actors.add(new RadarActor(valueNormalizer));
        actors.add(new GunActor(valueNormalizer));

        return actors;
    }
}
