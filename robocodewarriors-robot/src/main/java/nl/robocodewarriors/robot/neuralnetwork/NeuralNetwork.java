package nl.robocodewarriors.robot.neuralnetwork;

/**
 * Contains the information from the neural network. The class implementation can be retrieved with NeuralNetworkFactory. This implementation is
 * created by the Robot Builder.
 */
public interface NeuralNetwork {

    /**
     * @return JSON string containing the serialised neural network.
     */
    String getJson();
}
