package nl.robocodewarriors.robot.neuralnetwork;

public class NeuralNetworkFactory {

    public NeuralNetwork getNeuralNetwork() {
        return new NeuralNetworkImpl();
    }
}