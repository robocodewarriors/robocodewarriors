package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public class PreviousActionCalculator extends BaseValuesCalculator {

    private double previousRadarHeading = 0.0;
    private double previousGunHeading = 0.0;
    private double previousHeading = 0.0;
    private double previousVelocity = 0.0;

    public PreviousActionCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        setAllScopeInputsToZero(map);

        calculateRadarHeading(robot, map);
        calculateGunHeading(robot, map);
        calculateRobotHeading(robot, map);
        calculateDirection(robot, map);
        previousRadarHeading = robot.getRadarHeading();
        previousHeading = robot.getHeading();
        previousGunHeading = robot.getGunHeading();
        previousVelocity= robot.getVelocity();
    }

    private void calculateDirection(NeuralRobot robot, Map<String, Double> map) {
        if (Math.abs(robot.getVelocity()) < Math.abs(previousVelocity)) {
            map.put(NeuralRobot.INPUT_ROBOT_DECELERATED, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (Math.abs(robot.getVelocity()) > Math.abs(previousVelocity)) {
            map.put(NeuralRobot.INPUT_ROBOT_ACCELERATED, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }

        if (robot.getVelocity() < 0) {
            map.put(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (robot.getVelocity() > 0) {
            map.put(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }

    private void setAllScopeInputsToZero(Map<String, Double> map) {
        map.put(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_LEFT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_RIGHT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_LEFT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_RIGHT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT, ValueNormalizer.ZERO);

        map.put(NeuralRobot.INPUT_ROBOT_ACCELERATED, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_ROBOT_DECELERATED, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD, ValueNormalizer.ZERO);

    }

    private void calculateRobotHeading(NeuralRobot robot, Map<String, Double> map) {
        int result = leftOrRight(robot.getHeading(), previousHeading);

        if (result == -1) {
            map.put(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_LEFT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (result == 1) {
            map.put(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_RIGHT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }

    private void calculateGunHeading(NeuralRobot robot, Map<String, Double> map) {
        int result = leftOrRight(robot.getGunHeading(), previousGunHeading);

        if (result == -1) {
            map.put(NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_LEFT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (result == 1) {
            map.put(NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_RIGHT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }

    public void calculateRadarHeading(NeuralRobot robot, Map<String, Double> map) {
        int result = leftOrRight(robot.getRadarHeading(), previousRadarHeading);
        if (result == -1) {
            map.put(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (result == 1) {
            map.put(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }

    }

    protected int leftOrRight(double currentHeading, double previousHeading) {
        int bearing = ((int)currentHeading - (int)previousHeading + 360);
        int rest =  bearing % 360;
        if (rest > 0 && rest < 180 ) {
            return 1;
        } else if (rest > 180 ) {
            return -1;
        }
        return 0;
    }


}
