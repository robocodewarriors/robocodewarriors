package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import robocode.ScannedRobotEvent;

import java.util.Map;

public class ScannedRobotCalculator extends BaseValuesCalculator {
    
    public ScannedRobotCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        ScannedRobotEvent event = robot.getLastScannedRobotEvent();
        setAllScopeInputsToZero(map);
        if (event == null) {
            return;
        }
        calculateGunPosition(robot, map, event);
        calculateRadarPosition(robot, map, event);
        calculateRobotPosition(map, event);
        calculateOtherRobotLife(robot, map, event);
    }

    protected void calculateOtherRobotLife(NeuralRobot robot, Map<String, Double> map, ScannedRobotEvent event) {
        if (event.getEnergy() < robot.getEnergy()) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }

    private void calculateRobotPosition(Map<String, Double> map, ScannedRobotEvent event) {
        if (event.getBearing() >= 150 || event.getBearing() <= -150) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (event.getBearing() < 150 && event.getBearing() > 30) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (event.getBearing() > -150 && event.getBearing() < -30) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (event.getBearing() <= 30 || event.getBearing() >= -30) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }

    private void setAllScopeInputsToZero(Map<String, Double> map) {
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN, ValueNormalizer.ZERO);
    }

    private void calculateRadarPosition(NeuralRobot robot, Map<String, Double> map, ScannedRobotEvent event) {
        double radarBearing = robot.getRadarHeading() - (robot.getHeading() + event.getBearing());
        if (radarBearing > 180) {
            radarBearing = (360 - radarBearing) * -1;
        }

        if (radarBearing < 0) { //Radar is left from scanned robot. So inverted.
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (radarBearing > 0) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }

    private void calculateGunPosition(NeuralRobot robot, Map<String, Double> map, ScannedRobotEvent event) {
        double gunBearing = robot.getGunHeading() - (robot.getHeading() + event.getBearing());
        if (gunBearing > 180) {
            gunBearing = (360 - gunBearing) * -1;
        }

        if (gunBearing < -0.1) { //Gun is left from scanned robot. So inverted.
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (gunBearing > 0.1) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else if (gunBearing >= -0.1 && gunBearing <= 0.1) {
            map.put(NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }
}
