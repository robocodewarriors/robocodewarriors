package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import robocode.HitByBulletEvent;

import java.util.Map;

public class HitByBulletCalculator extends BaseValuesCalculator {

    public HitByBulletCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        setAllScopeInputsToZero(map);
        HitByBulletEvent event = robot.getLastHitByBulletEvent();
        calculateHitByBullet(robot, map, event);
    }

    private void calculateHitByBullet(NeuralRobot robot, Map<String, Double> map, HitByBulletEvent event) {
        if (event != null) {
            if (event.getBearing() >= -30.0 && event.getBearing() <= 30.0) {
                map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT, ValueNormalizer.DEFAULT_HIGH_VALUE);
            } else if (event.getBearing() <= -150.0 || event.getBearing() >= 150.0) {
                map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR, ValueNormalizer.DEFAULT_HIGH_VALUE);
            } else if (event.getBearing() > -150.0 && event.getBearing() < -30.0) {
                map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT, ValueNormalizer.DEFAULT_HIGH_VALUE);
            } else if (event.getBearing() < 150.0 && event.getBearing() > 30.0) {
                map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT, ValueNormalizer.DEFAULT_HIGH_VALUE);
            }
        }
    }

    private void setAllScopeInputsToZero(Map<String, Double> map) {
        map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT, ValueNormalizer.ZERO);
    }

}
