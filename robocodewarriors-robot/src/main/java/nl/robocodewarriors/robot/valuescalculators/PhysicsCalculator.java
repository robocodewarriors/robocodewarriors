package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.NormalizedDegrees;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

import robocode.AdvancedRobot;

public class PhysicsCalculator extends BaseValuesCalculator {

    public PhysicsCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        this.calculateSpeed(robot, map);
        this.calculateEnergy(robot, map);
        this.calculateHeading(robot, map);
    }

    protected void calculateSpeed(AdvancedRobot robot, Map<String, Double> map) {
        Double velocity = robot.getVelocity();
//        /map.put(NeuralRobot.VELOCITY, this.getNormalizer().scaleValue(velocity, 0.0, Rules.MAX_VELOCITY, 0.0, ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    protected void calculateEnergy(AdvancedRobot robot, Map<String, Double> map) {
        Double energy = robot.getEnergy();
//        map.put(NeuralRobot.ENERGY, this.getNormalizer().scaleValue(energy, 0.0, 100.0, ValueNormalizer.DEFAULT_HIGH_VALUE, 0.0));
    }

    protected void calculateHeading(AdvancedRobot robot, Map<String, Double> map) {
        NormalizedDegrees heading = this.getDegreesNormalizer().normalize(robot.getHeading());

        if (heading != null) {
//            map.put(NeuralRobot.HEADING_UP_DOWN, heading.getUpDownValue());
//            map.put(NeuralRobot.HEADING_LEFT_RIGHT, heading.getLeftRightValue());
        }
    }
}
