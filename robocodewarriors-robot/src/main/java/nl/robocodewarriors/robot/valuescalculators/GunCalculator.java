package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

import robocode.AdvancedRobot;

public class GunCalculator extends BaseValuesCalculator {

    private static final Double MAX_BULLET_POWER = 3.0;

    public GunCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        calculateReadyToFire(robot, map);
        calculateEnoughHealthToFire(robot, map);
    }

    private void calculateReadyToFire(AdvancedRobot robot, Map<String, Double> map) {
        if (robot.getGunHeat() == 0.0) {
            map.put(NeuralRobot.INPUT_GUN_IS_READY, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else {
            map.put(NeuralRobot.INPUT_GUN_IS_READY, ValueNormalizer.ZERO);
        }
    }

    private void calculateEnoughHealthToFire(AdvancedRobot robot, Map<String, Double> map) {
        if (robot.getEnergy() > MAX_BULLET_POWER) {
            map.put(NeuralRobot.INPUT_ENOUGH_HEALTH_TO_FIRE, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else {
            map.put(NeuralRobot.INPUT_ENOUGH_HEALTH_TO_FIRE, ValueNormalizer.ZERO);
        }
    }
}
