package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import robocode.ScannedRobotEvent;

import java.util.Map;

public class RobotDistanceCalculator extends BaseValuesCalculator {

    public RobotDistanceCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        ScannedRobotEvent event = robot.getLastScannedRobotEvent();
        setAllScopeInputsToZero(map);


        if (event != null) {
            double normalizedDistance = getNormalizer().scaleValue(event.getDistance(), 200, 0, ValueNormalizer.ZERO, ValueNormalizer.DEFAULT_HIGH_VALUE);
            if (event.getBearing() > -90 && event.getBearing() < 90) {
                map.put(NeuralRobot.INPUT_NEAR_ROBOT_FRONT, normalizedDistance);
            }
            if (event.getBearing() < -90 || event.getBearing() > 90) {
                map.put(NeuralRobot.INPUT_NEAR_ROBOT_REAR, normalizedDistance);
            }

            if (event.getBearing() < 0) {
                map.put(NeuralRobot.INPUT_NEAR_ROBOT_LEFT, normalizedDistance);

            }
            if (event.getBearing() > 0) {
                map.put(NeuralRobot.INPUT_NEAR_ROBOT_RIGHT, normalizedDistance);
            }
        }
    }

    private void setAllScopeInputsToZero(Map<String, Double> map) {
        map.put(NeuralRobot.INPUT_NEAR_ROBOT_REAR, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_NEAR_ROBOT_LEFT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_NEAR_ROBOT_RIGHT, ValueNormalizer.ZERO);
        map.put(NeuralRobot.INPUT_NEAR_ROBOT_FRONT, ValueNormalizer.ZERO);
    }
}
