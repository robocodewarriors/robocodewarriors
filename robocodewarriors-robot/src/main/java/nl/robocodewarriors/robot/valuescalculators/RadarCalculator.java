package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.NormalizedDegrees;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import robocode.AdvancedRobot;

import java.util.Map;

public class RadarCalculator extends BaseValuesCalculator {

    public RadarCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        calculateRadarHeading(robot, map);
    }

    protected void calculateRadarHeading(AdvancedRobot robot, Map<String, Double> map) {
        NormalizedDegrees heading = this.getDegreesNormalizer().normalize(robot.getRadarHeading());

//        map.put(NeuralRobot.RADAR_HEADING_UP_DOWN, heading.getUpDownValue());
//        map.put(NeuralRobot.RADAR_HEADING_LEFT_RIGHT, heading.getLeftRightValue());
    }
}
