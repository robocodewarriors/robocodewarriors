package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public class NearWallCalculator extends BaseValuesCalculator {

    public NearWallCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        double heading = robot.getHeading();
        double northDistance = getNorthDistance(robot);
        double eastDistance = getEastDistance(robot);
        double southDistance = getSouthDistance(robot);
        double westDistance = getWestDistance(robot);

        if (heading == 0) {
            storeValues(map, northDistance, eastDistance, southDistance, westDistance);
        } else if (heading == 90) {
            storeValues(map, eastDistance, southDistance, westDistance, northDistance);
        } else if (heading == 180) {
            storeValues(map, southDistance, westDistance, northDistance, eastDistance);
        } else if (heading == 270) {
            storeValues(map, westDistance, northDistance, eastDistance, southDistance);
        } else if (heading > 0 && heading < 90) {
            storeValues(map,
                    distanceToWall(northDistance, eastDistance, heading),
                    distanceToWall(eastDistance, southDistance, heading),
                    distanceToWall(southDistance, westDistance, heading),
                    distanceToWall(westDistance, northDistance, heading));
        } else if (heading > 90 && heading < 180) {
            storeValues(map,
                    distanceToWall(eastDistance, southDistance, heading - 90),
                    distanceToWall(southDistance, westDistance, heading - 90),
                    distanceToWall(westDistance, northDistance, heading - 90),
                    distanceToWall(northDistance, eastDistance, heading - 90));
        } else if (heading > 180 && heading < 270) {
            storeValues(map,
                    distanceToWall(southDistance, westDistance, heading - 180),
                    distanceToWall(westDistance, northDistance, heading - 180),
                    distanceToWall(northDistance, eastDistance, heading - 180),
                    distanceToWall(eastDistance, southDistance, heading - 180));
        } else if (heading > 270 && heading < 360) {
            storeValues(map,
                    distanceToWall(westDistance, northDistance, heading - 270),
                    distanceToWall(northDistance, eastDistance, heading - 270),
                    distanceToWall(eastDistance, southDistance, heading - 270),
                    distanceToWall(southDistance, westDistance, heading - 270));
        }

    }

    private double distanceToWall(double first, double second, double angleToFirst) {
        return Math.min(first / Math.cos(angleToFirst / 180.0 * Math.PI), second / Math.cos((90.0 - angleToFirst) / 180.0 * Math.PI));
    }
    
    private void storeValue(Map<String, Double> map, String key, double value) {
        map.put(key, getNormalizer().scaleValue(value, 100, 0, ValueNormalizer.ZERO, ValueNormalizer.DEFAULT_HIGH_VALUE));
    }
    
    private void storeValues(Map<String, Double> map, double northDistance, double eastDistance, double southDistance, double westDistance) {
        storeValue(map, NeuralRobot.INPUT_NEAR_WALL_FRONT, northDistance);
        storeValue(map, NeuralRobot.INPUT_NEAR_WALL_RIGHT, eastDistance);
        storeValue(map, NeuralRobot.INPUT_NEAR_WALL_REAR, southDistance);
        storeValue(map, NeuralRobot.INPUT_NEAR_WALL_LEFT, westDistance);
    }

    private double getWestDistance(NeuralRobot robot) {
        return robot.getX();
    }

    private double getEastDistance(NeuralRobot robot) {
        return robot.getBattleFieldWidth() - robot.getX();
    }

    private double getNorthDistance(NeuralRobot robot) {
        return robot.getBattleFieldHeight() - robot.getY();
    }

    private double getSouthDistance(NeuralRobot robot) {
        return robot.getY();
    }
}
