package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

public abstract class BaseValuesCalculator {

    private ValueNormalizer normalizer;
    private DegreesNormalizer degreesNormalizer;

    public BaseValuesCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        this.normalizer = normalizer;
        this.degreesNormalizer = degreesNormalizer;
    }

    public ValueNormalizer getNormalizer() {
        return this.normalizer;
    }

    public DegreesNormalizer getDegreesNormalizer() {
        return this.degreesNormalizer;
    }

    public abstract void calculate(NeuralRobot robot, Map<String, Double> map);
}
