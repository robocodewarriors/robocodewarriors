package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

import robocode.HitRobotEvent;

public class HitRobotCalculator extends BaseValuesCalculator {

    public HitRobotCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        HitRobotEvent event = robot.getLastHitRobotEvent();

        if(event == null) {
            map.put(NeuralRobot.INPUT_HIT_ROBOT_IS_MY_FAULT, ValueNormalizer.ZERO);
            map.put(NeuralRobot.INPUT_HIT_ROBOT_LESS_ENERGY, ValueNormalizer.ZERO);
            return;
        }

        calculateInputHitRobotLessEnergy(robot, map, event);
        calculateInputHitRobotIsMyFault(map, event);
    }

    private void calculateInputHitRobotIsMyFault(Map<String, Double> map, HitRobotEvent event) {
        if(event.isMyFault()) {
            map.put(NeuralRobot.INPUT_HIT_ROBOT_IS_MY_FAULT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        } else {
            map.put(NeuralRobot.INPUT_HIT_ROBOT_IS_MY_FAULT, ValueNormalizer.ZERO);
        }
    }

    private void calculateInputHitRobotLessEnergy(NeuralRobot robot, Map<String, Double> map, HitRobotEvent event) {
        if(event.getEnergy() < robot.getEnergy()) {
            map.put(NeuralRobot.INPUT_HIT_ROBOT_HAS_LESS_ENERGY, ValueNormalizer.DEFAULT_HIGH_VALUE);
            map.put(NeuralRobot.INPUT_HIT_ROBOT_HAS_EQUAL_OR_MORE_ENERGY, ValueNormalizer.ZERO);
        } else {
            map.put(NeuralRobot.INPUT_HIT_ROBOT_HAS_LESS_ENERGY, ValueNormalizer.ZERO);
            map.put(NeuralRobot.INPUT_HIT_ROBOT_HAS_EQUAL_OR_MORE_ENERGY, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }
}
