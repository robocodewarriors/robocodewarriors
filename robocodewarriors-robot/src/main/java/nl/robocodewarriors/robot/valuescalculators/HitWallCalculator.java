package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import java.util.Map;

import robocode.HitWallEvent;

public class HitWallCalculator extends BaseValuesCalculator {

    public HitWallCalculator(ValueNormalizer normalizer, DegreesNormalizer degreesNormalizer) {
        super(normalizer, degreesNormalizer);
    }

    @Override
    public void calculate(NeuralRobot robot, Map<String, Double> map) {
        HitWallEvent event = robot.getLastHitWallEvent();

        if (event == null) {
            map.put(NeuralRobot.INPUT_HIT_WALL_FRONT, ValueNormalizer.ZERO);
            map.put(NeuralRobot.INPUT_HIT_WALL_REAR, ValueNormalizer.ZERO);
            map.put(NeuralRobot.INPUT_HIT_WALL_LEFT, ValueNormalizer.ZERO);
            map.put(NeuralRobot.INPUT_HIT_WALL_RIGHT, ValueNormalizer.ZERO);
            return;
        }

        double bearing = event.getBearing();
        if (Math.abs(bearing) > 90) {
            map.put(NeuralRobot.INPUT_HIT_WALL_REAR, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
        if (Math.abs(bearing) < 90) {
            map.put(NeuralRobot.INPUT_HIT_WALL_FRONT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
        if (bearing < 0) {
            map.put(NeuralRobot.INPUT_HIT_WALL_LEFT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
        if (bearing > 0) {
            map.put(NeuralRobot.INPUT_HIT_WALL_RIGHT, ValueNormalizer.DEFAULT_HIGH_VALUE);
        }
    }
}
