package nl.robocodewarriors.robot.valuenormalizers;

public class DegreesDenormalizer {

    private static final double MIN_VALUE = -1.0;
    private static final double MAX_VALUE = 1.0;
    private static final double DEGREES_0 = 0;
    private static final double DEGREES_180 = 180;
    private ValueNormalizer valueNormalizer;

    public DegreesDenormalizer(ValueNormalizer valueNormalizer) {
        this.valueNormalizer = valueNormalizer;
    }

    public double calculateDegrees(NormalizedDegrees normalizedDegrees) {
        double leftRight = normalizedDegrees.getLeftRightValue();
        double upDown = normalizedDegrees.getUpDownValue();

        if (upDown == MAX_VALUE) {
            return DEGREES_180;
        } else if (upDown == MIN_VALUE) {
            return DEGREES_0;
        }

        double degreesRight = valueNormalizer.scaleValue(upDown, MIN_VALUE, MAX_VALUE, DEGREES_0, DEGREES_180);
        double degreesLeft = (DEGREES_180 + (DEGREES_180 - degreesRight));

        if (leftRight >= 0) {
            return degreesLeft;
        } else {
            return degreesRight;
        }
    }
}
