package nl.robocodewarriors.robot.valuenormalizers;

public class NormalizedDegrees {

    private double leftRightValue;
    private double upDownValue;

    public double getUpDownValue() {
        return upDownValue;
    }

    public void setUpDownValue(double upDownValue) {
        this.upDownValue = upDownValue;
    }

    public double getLeftRightValue() {
        return leftRightValue;
    }

    public void setLeftRightValue(double leftRightValue) {
        this.leftRightValue = leftRightValue;
    }
}
