package nl.robocodewarriors.robot.valuenormalizers;

public class DegreesNormalizer {

    private static final double DEGREES_360 = 360.0;
    private static final double DEGREES_270 = 270.0;
    private static final double DEGREES_180 = 180.0;
    private static final double DEGREES_90 = 90.0;
    private static final double DEGREES_0 = 0.0;
    private ValueNormalizer valueNormalizer;

    public DegreesNormalizer(ValueNormalizer valueNormalizer) {
        this.valueNormalizer = valueNormalizer;
    }

    public NormalizedDegrees normalize(double degrees) {
        NormalizedDegrees normalizedDegrees = new NormalizedDegrees();
        double upDown;
        if (degrees > 180) {
            upDown = valueNormalizer.scaleValueByDefault(degrees, DEGREES_360, DEGREES_180);
        } else {
            upDown = valueNormalizer.scaleValueByDefault(degrees, DEGREES_0, DEGREES_180);
        }

        double leftRight;
        if (degrees > 90 && degrees <= 270) {
            leftRight = valueNormalizer.scaleValueByDefault(degrees, DEGREES_90, DEGREES_270);
        } else {
            if (degrees <= 90) {
                leftRight = valueNormalizer.scaleValue(degrees, DEGREES_0, DEGREES_90, 0.0, ValueNormalizer.DEFAULT_LOW_VALUE);
            } else {
                leftRight = valueNormalizer.scaleValue(degrees, DEGREES_270, DEGREES_360, ValueNormalizer.DEFAULT_HIGH_VALUE, 0.0);

            }
        }

        normalizedDegrees.setLeftRightValue(leftRight);
        normalizedDegrees.setUpDownValue(upDown);
        return normalizedDegrees;
    }
}
