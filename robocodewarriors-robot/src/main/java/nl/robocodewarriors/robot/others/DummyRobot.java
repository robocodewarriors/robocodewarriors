package nl.robocodewarriors.robot.others;


import robocode.Robot;

import java.awt.*;

public class DummyRobot extends Robot {

    boolean peek; // Don't turn if there's a robot there
    double moveAmount; // How much to move

    /**
     * run: Move around the walls
     */
    public void run() {
        // Set colors
        setBodyColor(Color.pink);
        setGunColor(Color.pink);
        setRadarColor(Color.pink);
        setBulletColor(Color.pink);
        setScanColor(Color.pink);

        while (true) {
            turnRight(90);
        }
    }

}