package nl.robocodewarriors.robot.neuralrobot;

import nl.robocodewarriors.robot.context.Container;
import robocode.*;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class NeuralRobot extends AdvancedRobot {

    // inputs
    // RAM
    public static final String INPUT_HIT_ROBOT_IS_MY_FAULT = "INPUT_HIT_ROBOT_IS_MY_FAULT";
    public static final String INPUT_HIT_ROBOT_LESS_ENERGY = "INPUT_HIT_ROBOT_LESS_ENERGY";
    public static final String INPUT_HIT_ROBOT_HAS_LESS_ENERGY = "INPUT_HIT_ROBOT_HAS_LESS_ENERGY";
    public static final String INPUT_HIT_ROBOT_HAS_EQUAL_OR_MORE_ENERGY = "INPUT_HIT_ROBOT_HAS_EQUAL_OR_MORE_ENERGY";
    // HIT WALL
    public static final String INPUT_HIT_WALL_FRONT = "INPUT_HIT_WALL_FRONT";
    public static final String INPUT_HIT_WALL_RIGHT = "INPUT_HIT_WALL_RIGHT";
    public static final String INPUT_HIT_WALL_LEFT = "INPUT_HIT_WALL_LEFT";
    public static final String INPUT_HIT_WALL_REAR = "INPUT_HIT_WALL_REAR";
    // NEAR WALL
    public static final String INPUT_NEAR_WALL_FRONT = "INPUT_NEAR_WALL_FRONT";
    public static final String INPUT_NEAR_WALL_RIGHT = "INPUT_NEAR_WALL_RIGHT";
    public static final String INPUT_NEAR_WALL_LEFT = "INPUT_NEAR_WALL_LEFT";
    public static final String INPUT_NEAR_WALL_REAR = "INPUT_NEAR_WALL_REAR";
    // SCANNED ROBOT
    public static final String INPUT_SCANNED_ROBOT_LEFT_TO_GUN = "INPUT_SCANNED_ROBOT_LEFT_TO_GUN";
    public static final String INPUT_SCANNED_ROBOT_RIGHT_TO_GUN = "INPUT_SCANNED_ROBOT_RIGHT_TO_GUN";
    public static final String INPUT_SCANNED_ROBOT_LEFT_TO_RADAR = "INPUT_SCANNED_ROBOT_LEFT_TO_RADAR";
    public static final String INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR = "INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR";
    public static final String INPUT_SCANNED_ROBOT_REAR_TO_ROBOT = "INPUT_SCANNED_ROBOT_REAR_TO_ROBOT";
    public static final String INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT = "INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT";
    public static final String INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT = "INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT";
    public static final String INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT = "INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT";
    public static final String INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY = "INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY";
    public static final String INPUT_SCANNED_ROBOT_AHEAD_OF_GUN = "INPUT_SCANNED_ROBOT_AHEAD_OF_GUN";

    // HIT BY BULLET
    public static final String INPUT_HIT_BY_BULLET_ON_FRONT = "INPUT_HIT_BY_BULLET_ON_FRONT";
    public static final String INPUT_HIT_BY_BULLET_ON_RIGHT = "INPUT_HIT_BY_BULLET_ON_RIGHT";
    public static final String INPUT_HIT_BY_BULLET_ON_REAR = "INPUT_HIT_BY_BULLET_ON_REAR";
    public static final String INPUT_HIT_BY_BULLET_ON_LEFT = "INPUT_HIT_BY_BULLET_ON_LEFT";
    //PREVIOUS ACTIONS
    public static final String INPUT_PREVIOUS_RADAR_TURNED_LEFT = "INPUT_PREVIOUS_RADAR_TURNED_LEFT";
    public static final String INPUT_PREVIOUS_RADAR_TURNED_RIGHT = "INPUT_PREVIOUS_RADAR_TURNED_RIGHT";
    public static final String INPUT_PREVIOUS_BODY_TURNED_LEFT = "INPUT_PREVIOUS_BODY_TURNED_LEFT";
    public static final String INPUT_PREVIOUS_BODY_TURNED_RIGHT = "INPUT_PREVIOUS_BODY_TURNED_RIGHT";
    public static final String INPUT_PREVIOUS_GUN_TURNED_LEFT = "INPUT_PREVIOUS_GUN_TURNED_LEFT";
    public static final String INPUT_PREVIOUS_GUN_TURNED_RIGHT = "INPUT_PREVIOUS_GUN_TURNED_RIGHT";

    // GUN
    public static final String INPUT_GUN_IS_READY = "INPUT_GUN_IS_READY";
    public static final String INPUT_ENOUGH_HEALTH_TO_FIRE = "INPUT_ENOUGH_HEALTH_TO_FIRE";
    public static final String INPUT_ROBOT_MOVING_BACKWARD = "INPUT_ROBOT_MOVING_BACKWARD";
    public static final String INPUT_ROBOT_DECELERATED = "INPUT_ROBOT_DECELERATED";
    public static final String INPUT_ROBOT_MOVING_AHEAD = "INPUT_ROBOT_MOVING_AHEAD";
    public static final String INPUT_ROBOT_ACCELERATED = "INPUT_ROBOT_ACCELERATED";

    // NEAR ROBOT
    public static final String INPUT_NEAR_ROBOT_FRONT = "INPUT_NEAR_ROBOT_FRONT";
    public static final String INPUT_NEAR_ROBOT_RIGHT = "INPUT_NEAR_ROBOT_RIGHT";
    public static final String INPUT_NEAR_ROBOT_LEFT = "INPUT_NEAR_ROBOT_LEFT";
    public static final String INPUT_NEAR_ROBOT_REAR = "INPUT_NEAR_ROBOT_REAR";

    // OUTPUTS
    public static final String OUTPUT_RADAR_TURN_LEFT = "OUTPUT_RADAR_TURN_LEFT";
    public static final String OUTPUT_RADAR_TURN_RIGHT = "OUTPUT_RADAR_TURN_RIGHT";

    public static final String OUTPUT_GUN_TURN_LEFT = "OUTPUT_GUN_TURN_LEFT";
    public static final String OUTPUT_GUN_TURN_RIGHT = "OUTPUT_GUN_TURN_RIGHT";

    public static final String OUTPUT_ROBOT_TURN_LEFT = "OUTPUT_ROBOT_TURN_LEFT";
    public static final String OUTPUT_ROBOT_TURN_RIGHT = "OUTPUT_ROBOT_TURN_RIGHT";

    public static final String OUTPUT_DRIVE_AHEAD = "OUTPUT_DRIVE_AHEAD";
    public static final String OUTPUT_DRIVE_BACKWARDS = "OUTPUT_DRIVE_BACKWARDS";

    public static final String OUTPUT_SHOULD_ACCELERATE = "OUTPUT_SHOULD_ACCELERATE";
    public static final String OUTPUT_SHOULD_DECELERATE = "OUTPUT_SHOULD_DECELERATE";

    public static final String OUTPUT_FIRE = "OUTPUT_FIRE";

    // Scanned event
    private ScannedRobotEvent lastScannedRobotEvent;
    private int lastScannedRobotCounter = 0;
    private static final int BUFFER_TIME_SCANNED_ROBOT_EVENT = 5;
    // Hitrobot event
    private HitRobotEvent lastHitRobotEvent;
    private int lastHitRobotCounter = 0;
    private static final int BUFFER_TIME_HIT_ROBOT_EVENT = 20;
    // Hit by bullet event
    private HitByBulletEvent lastHitByBulletEvent;
    private int lastHitByBulletCounter = 0;
    private static final int BUFFER_TIME_HIT_BY_BULLET_EVENT = 5;
    // Hit wall event
    private HitWallEvent lastWallHitEvent;
    private int lastWallHitCounter = 0;
    private static final int BUFFER_TIME_HIT_WALL_EVENT = 20;

    @Override
    public void run() {
        Container container = new Container(this);
        RobotRunner robotRunner = container.getRobotRunner();

        while (true) {
            robotRunner.run();
            lowerCounters();
        }
    }

    private void lowerCounters() {
        lastWallHitCounter--;
        lastHitRobotCounter--;
        lastHitByBulletCounter--;
        lastScannedRobotCounter--;
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        lastScannedRobotCounter = BUFFER_TIME_SCANNED_ROBOT_EVENT;
        lastScannedRobotEvent = event;
    }

    @Override
    public void onHitRobot(HitRobotEvent event) {
        lastHitRobotCounter = BUFFER_TIME_HIT_ROBOT_EVENT;
        lastHitRobotEvent = event;
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        lastWallHitCounter = BUFFER_TIME_HIT_WALL_EVENT;
        lastWallHitEvent = event;
    }

    @Override
    public void onHitByBullet(HitByBulletEvent event) {
        lastHitByBulletCounter = BUFFER_TIME_HIT_BY_BULLET_EVENT;
        lastHitByBulletEvent = event;
    }

    public HitWallEvent getLastHitWallEvent() {
        if (lastWallHitCounter > 0) {
            return lastWallHitEvent;
        }
        return null;
    }

    public HitRobotEvent getLastHitRobotEvent() {
        if (lastHitRobotCounter > 0) {
            return lastHitRobotEvent;
        }
        return null;
    }

    public ScannedRobotEvent getLastScannedRobotEvent() {
        if (lastScannedRobotCounter > 0) {
            return lastScannedRobotEvent;
        }
        return null;
    }

    public HitByBulletEvent getLastHitByBulletEvent() {
        if (lastHitByBulletCounter > 0) {
            return lastHitByBulletEvent;
        }
        return null;
    }

    public abstract String getNetworkJson();

    public static Set<String> getInputKeys() {
        HashSet<String> inputs = new HashSet<String>();
        String[] inputKeys = new String[]{
                NeuralRobot.INPUT_HIT_ROBOT_IS_MY_FAULT,
                NeuralRobot.INPUT_HIT_ROBOT_LESS_ENERGY,
                NeuralRobot.INPUT_HIT_ROBOT_HAS_LESS_ENERGY,
                NeuralRobot.INPUT_HIT_ROBOT_HAS_EQUAL_OR_MORE_ENERGY,
                NeuralRobot.INPUT_HIT_WALL_FRONT,
                NeuralRobot.INPUT_HIT_WALL_RIGHT,
                NeuralRobot.INPUT_HIT_WALL_LEFT,
                NeuralRobot.INPUT_HIT_WALL_REAR,
                NeuralRobot.INPUT_NEAR_WALL_FRONT,
                NeuralRobot.INPUT_NEAR_WALL_RIGHT,
                NeuralRobot.INPUT_NEAR_WALL_LEFT,
                NeuralRobot.INPUT_NEAR_WALL_REAR,
                NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN,
                NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN,
                NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR,
                NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR,
                NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT,
                NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT,
                NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT,
                NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_LEFT,
                NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_RIGHT,
                NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_LEFT,
                NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_RIGHT,
                NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD,
                NeuralRobot.INPUT_ROBOT_DECELERATED,
                NeuralRobot.INPUT_ROBOT_MOVING_AHEAD,
                NeuralRobot.INPUT_ROBOT_ACCELERATED,
                NeuralRobot.INPUT_GUN_IS_READY,
                NeuralRobot.INPUT_ENOUGH_HEALTH_TO_FIRE,
                NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN,
                NeuralRobot.INPUT_NEAR_ROBOT_FRONT,
                NeuralRobot.INPUT_NEAR_ROBOT_RIGHT,
                NeuralRobot.INPUT_NEAR_ROBOT_LEFT,
                NeuralRobot.INPUT_NEAR_ROBOT_REAR
        };

        Collections.addAll(inputs, inputKeys);
        return inputs;
    }

    public static Set<String> getOutputKeys() {
        HashSet<String> outputs = new HashSet<String>();
        String[] outputKeys = new String[]{
                NeuralRobot.OUTPUT_RADAR_TURN_LEFT,
                NeuralRobot.OUTPUT_RADAR_TURN_RIGHT,
                NeuralRobot.OUTPUT_GUN_TURN_LEFT,
                NeuralRobot.OUTPUT_GUN_TURN_RIGHT,
                NeuralRobot.OUTPUT_ROBOT_TURN_LEFT,
                NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT,
                NeuralRobot.OUTPUT_DRIVE_AHEAD,
                NeuralRobot.OUTPUT_DRIVE_BACKWARDS,
                NeuralRobot.OUTPUT_SHOULD_ACCELERATE,
                NeuralRobot.OUTPUT_SHOULD_DECELERATE,
                NeuralRobot.OUTPUT_FIRE
        };

        Collections.addAll(outputs, outputKeys);
        return outputs;
    }
}
