package nl.robocodewarriors.robot.neuralrobot;

import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.robot.network.NetworkDeserializer;
import nl.robocodewarriors.robot.valueactors.ValueActor;
import nl.robocodewarriors.robot.valuescalculators.BaseValuesCalculator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RobotRunner {
    private List<ValueActor> actors;
    private List<BaseValuesCalculator> calculators;
    private NeuralRobot robot;
    private Network network;

    public RobotRunner(List<ValueActor> actors, List<BaseValuesCalculator> calculators, NeuralRobot robot) {
        this.actors = actors;
        this.calculators = calculators;
        this.robot = robot;

        setupNetwork();
    }

    public Map<String, Double> calculateInputValues() {
        Map<String, Double> map = new HashMap<String, Double>();
        for (BaseValuesCalculator valuesCalculator : calculators) {
            valuesCalculator.calculate(robot, map);
        }
        return map;
    }

    public void run() {
        Map<String, Double> map = calculateInputValues();

        robot.setTurnRadarLeft(10.0);
        if (network != null) {
            Map<String, Double> output = network.evaluate(map);
            actOnOutput(output);
        }
        robot.execute();
    }

    private void setupNetwork() {
        NetworkDeserializer networkDeserializer = new NetworkDeserializer(robot.getNetworkJson());
        try {
            this.network = networkDeserializer.getNetwork();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void actOnOutput(Map<String, Double> output) {
        for (ValueActor valueActor : actors) {
            valueActor.act(robot, output);
        }
    }
}
