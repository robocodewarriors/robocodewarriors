package nl.robocodewarriors.robot.network;

import nl.robocodewarriors.neuralnetwork.*;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NetworkDeserializer {

    private static final String INPUT_LAYER_CLASS = ".InputLayerImpl";
    private static final String NEURON_CLASS = ".NeuronImpl";
    private static final String HIDDEN_LAYER_CLASS = ".HiddenLayerImpl";
    private static final String OUTPUT_LAYER_CLASS = ".OutputLayerImpl";

    private static final String NEURONS_FIELD_NAME = "n";
    private static final String KEY_FIELD_NAME = "k";
    private static final String LAYERS_FIELD_NAME = "l";
    private static final String THRESHOLD_FIELD_NAME = "t";
    private static final String WEIGHTS_FIELD_NAME = "w";

    private final String json;

    public NetworkDeserializer(String json) {
        this.json = json;
    }

    public Network getNetwork() throws IOException {
        Network network = new NetworkImpl();
        List<Layer> networkLayers = new ArrayList<Layer>();
        JsonNode tree = new ObjectMapper().readTree(json);

        if (tree.has(LAYERS_FIELD_NAME)) {
            Iterator<JsonNode> layers = tree.get(LAYERS_FIELD_NAME).getElements();
            List<Neuron> neurons;

            while (layers.hasNext()) {
                JsonNode layer = layers.next();
                if (isType(INPUT_LAYER_CLASS, layer)) {
                    neurons = getInputs(layer);
                    networkLayers.add(new InputLayerImpl(neurons));
                } else if (isType(HIDDEN_LAYER_CLASS, layer)) {
                    neurons = getInputs(layer);
                    networkLayers.add(new HiddenLayerImpl(neurons));
                } else if (isType(OUTPUT_LAYER_CLASS, layer)) {
                    neurons = getInputs(layer);
                    networkLayers.add(new OutputLayerImpl(neurons));
                }
            }

            network.setLayers(networkLayers);
        }
        return network;
    }

    private List<Neuron> getInputs(JsonNode layer) {
        List<Neuron> neurons = new ArrayList<Neuron>();
        if (layer.has(NEURONS_FIELD_NAME)) {
            Iterator<JsonNode> jsonNodes = layer.get(NEURONS_FIELD_NAME).getElements();
            while (jsonNodes.hasNext()) {
                JsonNode node = jsonNodes.next();
                if (isType(NEURON_CLASS, node)) {
                    neurons.add(new NeuronImpl(node.get(KEY_FIELD_NAME).getTextValue(), node.get(THRESHOLD_FIELD_NAME).asDouble(), getWeights(node)));
                }
            }
        }
        return neurons;
    }

    private List<Double> getWeights(JsonNode neuron) {
        List<Double> weights = new ArrayList<Double>();
        Iterator<JsonNode> weightsNodes = neuron.get(WEIGHTS_FIELD_NAME).getElements();
        while (weightsNodes.hasNext()) {
            weights.add(weightsNodes.next().asDouble());
        }
        return weights;
    }

    private boolean isType(String type, JsonNode node) {
        return node.get("@").asText().equals(type);
    }
}
