package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class NearWallCalculatorTest {

    @Mock
    private DegreesNormalizer degreesNormalizer;
    @Mock
    private ValueNormalizer valueNormalizer;
    @Mock
    private NeuralRobot advancedRobot;
    private NearWallCalculator calculator;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        calculator = new NearWallCalculator(valueNormalizer, degreesNormalizer);
    }

    @Test
    public void testNorthEastCornerFacingNorth() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(720.0);
        when(advancedRobot.getY()).thenReturn(550.0);
        when(advancedRobot.getHeading()).thenReturn(0.0);

        when(valueNormalizer.scaleValue(eq(80.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.2);
        when(valueNormalizer.scaleValue(eq(50.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.5);
        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.5));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.2));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.0));
    }

    @Test
    public void testNorthEastCornerFacingEast() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(730.0);
        when(advancedRobot.getY()).thenReturn(510.0);
        when(advancedRobot.getHeading()).thenReturn(90.0);

        when(valueNormalizer.scaleValue(eq(90.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.1);
        when(valueNormalizer.scaleValue(eq(70.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.3);
        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.3));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.1));
    }

    @Test
    public void testNorthEastCornerFacingSouth() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(790.0);
        when(advancedRobot.getY()).thenReturn(560.0);
        when(advancedRobot.getHeading()).thenReturn(180.0);

        when(valueNormalizer.scaleValue(eq(10.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.9);
        when(valueNormalizer.scaleValue(eq(40.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.6);
        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.6));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.9));
    }

    @Test
    public void testNorthEastCornerFacingWest() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(705.0);
        when(advancedRobot.getY()).thenReturn(540.0);
        when(advancedRobot.getHeading()).thenReturn(270.0);

        when(valueNormalizer.scaleValue(eq(95.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.05);
        when(valueNormalizer.scaleValue(eq(60.0), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.4);
        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.4));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.05));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.0));
    }

    @Test
    public void closeToSouthEastCornerFacingLittleBitEast() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(750.0);
        when(advancedRobot.getY()).thenReturn(40.0);
        when(advancedRobot.getHeading()).thenReturn(120.0);

        when(valueNormalizer.scaleValue(eq(57.735026918962575), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.425);
        when(valueNormalizer.scaleValue(eq(46.18802153517006), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.538);
        when(valueNormalizer.scaleValue(eq(99.99999999999997), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.001);

        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.425));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.538));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.00));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.001));

    }

    @Test
    public void closeToSouthWestCornerFacingNorthEast() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(20.0);
        when(advancedRobot.getY()).thenReturn(10.0);
        when(advancedRobot.getHeading()).thenReturn(40.0);

        when(valueNormalizer.scaleValue(eq(15.557238268604122), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.844);
        when(valueNormalizer.scaleValue(eq(13.054072893322786), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.869);
        when(valueNormalizer.scaleValue(eq(26.108145786645572), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.739);

        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.844));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.869));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.739));
    }

    @Test
    public void closeToNorthWestCornerFacingWest() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(25.0);
        when(advancedRobot.getY()).thenReturn(560.0);
        when(advancedRobot.getHeading()).thenReturn(280.0);

        when(valueNormalizer.scaleValue(eq(25.385665297143625), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.746);
        when(valueNormalizer.scaleValue(eq(40.6170644754298), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.593);

        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.746));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.593));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.0));
    }

    @Test
    public void closeToNorthEastCornerFacingSouth() {
        when(advancedRobot.getBattleFieldHeight()).thenReturn(600.0);
        when(advancedRobot.getBattleFieldWidth()).thenReturn(800.0);
        when(advancedRobot.getX()).thenReturn(770.0);
        when(advancedRobot.getY()).thenReturn(585.0);
        when(advancedRobot.getHeading()).thenReturn(215.0);

        when(valueNormalizer.scaleValue(eq(26.151701934316474), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.738);
        when(valueNormalizer.scaleValue(eq(18.31161883142184), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.817);
        when(valueNormalizer.scaleValue(eq(36.62323766284368), eq(100.0), eq(0.0), eq(0.0), eq(1.0))).thenReturn(0.633);

        Map<String, Double> data = new HashMap<String, Double>();
        calculator.calculate(advancedRobot, data);

        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_FRONT), is(0.0));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_RIGHT), is(0.738));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_REAR), is(0.817));
        assertThat(data.get(NeuralRobot.INPUT_NEAR_WALL_LEFT), is(0.633));
    }
}
