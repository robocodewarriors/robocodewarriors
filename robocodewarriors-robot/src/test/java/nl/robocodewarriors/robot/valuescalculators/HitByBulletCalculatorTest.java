package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import robocode.HitByBulletEvent;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class HitByBulletCalculatorTest {

    @Mock
    private ValueNormalizer valueNormalizer;
    @Mock
    private DegreesNormalizer degreesNormalizer;
    @Mock
    private NeuralRobot advancedRobot;


    private HitByBulletCalculator hitByBulletCalculator;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        hitByBulletCalculator = new HitByBulletCalculator(valueNormalizer, degreesNormalizer);
    }

    @Test
    public void testNoHitEventsInData()  {
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(null);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testHitEventFront1() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        HitByBulletEvent event = new HitByBulletEvent(30.0/ 180.0 * Math.PI, null);
        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testHitEventFront2() {
        HitByBulletEvent event = new HitByBulletEvent(-30.0/ 180.0 * Math.PI, null);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testHitEventRight1() {
        HitByBulletEvent event = new HitByBulletEvent(31.0/ 180.0 * Math.PI, null);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }


    @Test
    public void testHitEventRight2() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        HitByBulletEvent event = new HitByBulletEvent(149.0/ 180.0 * Math.PI, null);

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testHitEventLeft1() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        HitByBulletEvent event = new HitByBulletEvent(-31.0/ 180.0 * Math.PI, null);

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testHitEventLeft2() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        HitByBulletEvent event = new HitByBulletEvent(-149.0/ 180.0 * Math.PI, null);

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testHitEventRear1() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        HitByBulletEvent event = new HitByBulletEvent(-150.0/ 180.0 * Math.PI, null);

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);

        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testHitEventRear2() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        HitByBulletEvent event = new HitByBulletEvent(150.0/ 180.0 * Math.PI, null);

        when(advancedRobot.getLastHitByBulletEvent()).thenReturn(event);
        hitByBulletCalculator.calculate(advancedRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT), is(ValueNormalizer.ZERO));
    }

}
