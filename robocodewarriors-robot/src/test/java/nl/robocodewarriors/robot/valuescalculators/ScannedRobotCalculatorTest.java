package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import robocode.ScannedRobotEvent;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ScannedRobotCalculatorTest {

    @Mock
    private ValueNormalizer valueNormalizer;
    @Mock
    private DegreesNormalizer degreesNormalizer;
    @Mock
    private NeuralRobot robot;

    private ScannedRobotCalculator scannedRobotCalculator;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        scannedRobotCalculator = new ScannedRobotCalculator(valueNormalizer, degreesNormalizer);
    }

    @Test
    public void testNoScannedRobotEvent() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        when(robot.getLastScannedRobotEvent()).thenReturn(null);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testScannedRobotEventLeftFromRobot() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(-40.0);
        when(robot.getHeading()).thenReturn(20.0);
        when(robot.getGunHeading()).thenReturn(330.0);
        when(robot.getRadarHeading()).thenReturn(355.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);
        when(robot.getEnergy()).thenReturn(80.0);
        when(event.getEnergy()).thenReturn(100.0);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY), is(ValueNormalizer.ZERO));

    }

    @Test
    public void testScannedRobotEventRightFromRobot() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(40.0);
        when(robot.getHeading()).thenReturn(-20.0);
        when(robot.getGunHeading()).thenReturn(30.0);
        when(robot.getRadarHeading()).thenReturn(5.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);
        when(robot.getEnergy()).thenReturn(100.0);
        when(event.getEnergy()).thenReturn(80.0);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testScannedRobotEventInFrontOfRobot() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(20.0);
        when(robot.getHeading()).thenReturn(-20.0);
        when(robot.getGunHeading()).thenReturn(30.0);
        when(robot.getRadarHeading()).thenReturn(5.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT), is(ValueNormalizer.ZERO));
    }

    @Test
     public void testScannedRobotEventInRearOfRobot() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(170.0);
        when(robot.getHeading()).thenReturn(-20.0);
        when(robot.getGunHeading()).thenReturn(30.0);
        when(robot.getRadarHeading()).thenReturn(5.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT), is(ValueNormalizer.ZERO));
    }


    @Test
    public void testScannedRobotEventInRearOfRobot2() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(-165.0);
        when(robot.getHeading()).thenReturn(-20.0);
        when(robot.getGunHeading()).thenReturn(30.0);
        when(robot.getRadarHeading()).thenReturn(5.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testRobotEventAheadOfGun() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(50.0);
        when(robot.getHeading()).thenReturn(-20.0);
        when(robot.getGunHeading()).thenReturn(30.0);
        when(robot.getRadarHeading()).thenReturn(0.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testRobotEventNearlyAheadOfGun() {
        Map<String, Double> dataMap = new HashMap<String, Double>();
        ScannedRobotEvent event = mock(ScannedRobotEvent.class);

        when(event.getBearing()).thenReturn(50.09);
        when(robot.getHeading()).thenReturn(-20.0);
        when(robot.getGunHeading()).thenReturn(30.0);
        when(robot.getRadarHeading()).thenReturn(0.0);
        when(robot.getLastScannedRobotEvent()).thenReturn(event);

        scannedRobotCalculator.calculate(robot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }



}