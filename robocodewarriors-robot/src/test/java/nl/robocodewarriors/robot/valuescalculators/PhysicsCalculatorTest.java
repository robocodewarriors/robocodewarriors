
package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.NormalizedDegrees;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

public class PhysicsCalculatorTest {

    private ValueNormalizer valueNormalizer;
    private DegreesNormalizer degreesNormalizer;
    private NeuralRobot advancedRobot;
    private PhysicsCalculator physicsCalculator;
    private NormalizedDegrees normalizedDegrees;

    @Before
    public void before() {
        advancedRobot = mock(NeuralRobot.class);
        valueNormalizer = mock(ValueNormalizer.class);
        degreesNormalizer = mock(DegreesNormalizer.class);
        normalizedDegrees = mock(NormalizedDegrees.class);

        physicsCalculator = new PhysicsCalculator(valueNormalizer, degreesNormalizer);
    }

    @Test
    public void testEnergy() {
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getEnergy()).thenReturn(80.0);
        when(valueNormalizer.scaleValue(80.0, 0.0, 100.0, 1.0, 0.0)).thenReturn(2.0);

        physicsCalculator.calculate(advancedRobot, dataMap);

        verify(advancedRobot, times(1)).getEnergy();
//        assertThat(dataMap.get(NeuralRobot.ENERGY), is(2.0));
    }

    @Test
    public void testVelocity() {
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getVelocity()).thenReturn(8.0);
        when(valueNormalizer.scaleValue(8.0, 0.0, 8.0, 0.0, 1.0)).thenReturn(3.0);

        physicsCalculator.calculate(advancedRobot, dataMap);

        verify(advancedRobot, times(1)).getVelocity();
//        assertThat(dataMap.get(NeuralRobot.VELOCITY), is(3.0));
    }

    @Test
    public void testRobotHeading() {
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getHeading()).thenReturn(200.3);

        when(degreesNormalizer.normalize(200.3)).thenReturn(normalizedDegrees);
        when(normalizedDegrees.getLeftRightValue()).thenReturn(2.0);
        when(normalizedDegrees.getUpDownValue()).thenReturn(3.0);

        physicsCalculator.calculate(advancedRobot, dataMap);

        verify(advancedRobot, times(1)).getHeading();
//        assertThat(dataMap.get(NeuralRobot.HEADING_UP_DOWN), is(3.0));
//        assertThat(dataMap.get(NeuralRobot.HEADING_LEFT_RIGHT), is(2.0));
    }
}
