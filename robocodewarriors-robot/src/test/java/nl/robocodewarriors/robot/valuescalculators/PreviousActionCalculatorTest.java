package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PreviousActionCalculatorTest {

    @Mock
    private ValueNormalizer normalizer;
    @Mock
    private DegreesNormalizer degreesNormalizer;

    @Mock
    private NeuralRobot neuralRobot;

    @Mock
    private NeuralRobot nextTurnRobot;

    private PreviousActionCalculator previousActionCalculator;


    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        previousActionCalculator = new PreviousActionCalculator(normalizer, degreesNormalizer);
    }

    @Test
    public void testLastTurnScannedRight() {
        when(neuralRobot.getRadarHeading()).thenReturn(10.0);
        when(nextTurnRobot.getRadarHeading()).thenReturn(20.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testLastTurnRobotRight() {
        when(neuralRobot.getHeading()).thenReturn(50.0);
        when(nextTurnRobot.getHeading()).thenReturn(100.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_RIGHT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testLastTurnRobotLeft() {
        when(neuralRobot.getHeading()).thenReturn(100.0);
        when(nextTurnRobot.getHeading()).thenReturn(50.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_LEFT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }



    @Test
    public void testLastTurnGunLeft() {
        when(neuralRobot.getGunHeading()).thenReturn(10.0);
        when(nextTurnRobot.getGunHeading()).thenReturn(300.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_LEFT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }


    @Test
    public void testLastTurnScannedLeft() {
        when(neuralRobot.getRadarHeading()).thenReturn(10.0);
        when(nextTurnRobot.getRadarHeading()).thenReturn(355.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));

    }


    @Test
    public void testLastTurnScannedRight2() {
        when(neuralRobot.getRadarHeading()).thenReturn(320.0);
        when(nextTurnRobot.getRadarHeading()).thenReturn(50.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testLastTurnScannedLeft2() {
        when(neuralRobot.getRadarHeading()).thenReturn(320.0);
        when(nextTurnRobot.getRadarHeading()).thenReturn(200.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testLeftRight() {
        assertThat(previousActionCalculator.leftOrRight(10, 350), is(1));
        assertThat(previousActionCalculator.leftOrRight(350, 10), is(-1));
        assertThat(previousActionCalculator.leftOrRight(350, 350), is(0));
        assertThat(previousActionCalculator.leftOrRight(350, 200), is(1));
        assertThat(previousActionCalculator.leftOrRight(200, 350), is(-1));
    }

    @Test
    public void testAcceleratedForward() {
        when(neuralRobot.getVelocity()).thenReturn(1.0);
        when(nextTurnRobot.getVelocity()).thenReturn(2.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_ACCELERATED), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_DECELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testDecelerationBackward() {
        when(neuralRobot.getVelocity()).thenReturn(-2.0);
        when(nextTurnRobot.getVelocity()).thenReturn(-1.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_ACCELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_DECELERATED), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testAcceleratedBackward() {
        when(neuralRobot.getVelocity()).thenReturn(-1.0);
        when(nextTurnRobot.getVelocity()).thenReturn(-2.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_ACCELERATED), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_DECELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testBackwardSameSpeed() {
        when(neuralRobot.getVelocity()).thenReturn(-2.0);
        when(nextTurnRobot.getVelocity()).thenReturn(-2.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_ACCELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_DECELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
    }

    @Test
    public void testForwardSameSpeed() {
        when(neuralRobot.getVelocity()).thenReturn(3.0);
        when(nextTurnRobot.getVelocity()).thenReturn(3.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_ACCELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_DECELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD), is(ValueNormalizer.DEFAULT_HIGH_VALUE));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD), is(ValueNormalizer.ZERO));
    }

    @Test
    public void testStandingStill() {
        when(neuralRobot.getVelocity()).thenReturn(0.0);
        when(nextTurnRobot.getVelocity()).thenReturn(0.0);

        Map<String, Double> mockMap = mock(Map.class);
        Map<String, Double> dataMap = new HashMap<String, Double>();

        previousActionCalculator.calculate(neuralRobot, mockMap);
        previousActionCalculator.calculate(nextTurnRobot, dataMap);

        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_ACCELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_DECELERATED), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_AHEAD), is(ValueNormalizer.ZERO));
        assertThat(dataMap.get(NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD), is(ValueNormalizer.ZERO));
    }

}
