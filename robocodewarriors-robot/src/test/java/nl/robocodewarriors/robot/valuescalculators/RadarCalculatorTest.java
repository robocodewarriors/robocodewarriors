package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.NormalizedDegrees;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

public class RadarCalculatorTest {

    private ValueNormalizer valueNormalizer;
    private DegreesNormalizer degreesNormalizer;
    private NeuralRobot advancedRobot;
    private RadarCalculator radarCalculator;
    private NormalizedDegrees normalizedDegrees;

    @Before
    public void before() {
        advancedRobot = mock(NeuralRobot.class);
        valueNormalizer = mock(ValueNormalizer.class);
        degreesNormalizer = mock(DegreesNormalizer.class);
        normalizedDegrees = mock(NormalizedDegrees.class);
        radarCalculator = new RadarCalculator(valueNormalizer, degreesNormalizer);
    }

    @Test
    public void testRadarHeading() {
        Map<String, Double> dataMap = new HashMap<String, Double>();

        when(advancedRobot.getRadarHeading()).thenReturn(200.3);

        when(degreesNormalizer.normalize(200.3)).thenReturn(normalizedDegrees);
        when(normalizedDegrees.getLeftRightValue()).thenReturn(2.0);
        when(normalizedDegrees.getUpDownValue()).thenReturn(3.0);

        radarCalculator.calculate(advancedRobot, dataMap);

        verify(advancedRobot, times(1)).getRadarHeading();
//        assertThat(dataMap.get(NeuralRobot.RADAR_HEADING_UP_DOWN), is(3.0));
//        assertThat(dataMap.get(NeuralRobot.RADAR_HEADING_LEFT_RIGHT), is(2.0));

    }
}
