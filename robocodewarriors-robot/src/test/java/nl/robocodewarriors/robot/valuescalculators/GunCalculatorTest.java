package nl.robocodewarriors.robot.valuescalculators;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.DegreesNormalizer;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class GunCalculatorTest {

    @Mock
    private ValueNormalizer valueNormalizer;
    @Mock
    private DegreesNormalizer degreesNormalizer;
    @Mock
    private NeuralRobot neuralRobot;
    private GunCalculator instance;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        instance = new GunCalculator(valueNormalizer, degreesNormalizer);
    }

    @Test
    public void testCalculateReadyToFire() {
        Map<String, Double> inputs = new HashMap<String, Double>();
        when(neuralRobot.getGunHeat()).thenReturn(0.0);

        instance.calculate(neuralRobot, inputs);

        assertEquals((Double) ValueNormalizer.DEFAULT_HIGH_VALUE, inputs.get(NeuralRobot.INPUT_GUN_IS_READY));
    }

    @Test
    public void testCalculateReadyToFireNotReady() {
        Map<String, Double> inputs = new HashMap<String, Double>();
        when(neuralRobot.getGunHeat()).thenReturn(4.0);

        instance.calculate(neuralRobot, inputs);

        assertEquals((Double) ValueNormalizer.ZERO, inputs.get(NeuralRobot.INPUT_GUN_IS_READY));
    }

    @Test
    public void testCalculateEnoughHealthToFire() {
        Map<String, Double> inputs = new HashMap<String, Double>();
        when(neuralRobot.getEnergy()).thenReturn(82.0);

        instance.calculate(neuralRobot, inputs);

        assertEquals((Double) ValueNormalizer.DEFAULT_HIGH_VALUE, inputs.get(NeuralRobot.INPUT_ENOUGH_HEALTH_TO_FIRE));
    }

    @Test
    public void testCalculateEnoughHealthToFireNotEnoughHealth() {
        Map<String, Double> inputs = new HashMap<String, Double>();
        when(neuralRobot.getEnergy()).thenReturn(1.0);

        instance.calculate(neuralRobot, inputs);

        assertEquals((Double) ValueNormalizer.ZERO, inputs.get(NeuralRobot.INPUT_ENOUGH_HEALTH_TO_FIRE));
    }
}
