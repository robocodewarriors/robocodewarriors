
package nl.robocodewarriors.robot.valuenormalizers;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ValueNormalizerTest {

    private ValueNormalizer valueNormalizer;

    @Before
    public void before() {
        valueNormalizer = new ValueNormalizer();
    }

    @Test
    public void testScaleValue() {
        double result = valueNormalizer.scaleValue(12, 10, 20, -1.0, 1.0);
        assertThat(result, is(-0.6));
    }

    @Test
    public void testScaleValueDefault() {
        double result = valueNormalizer.scaleValueByDefault(12, 10, 20);
        assertThat(result, is(-0.6));
    }

    @Test
    public void testScaleValueAboveZero() {
        double result = valueNormalizer.scaleValue(12, 10, 20, 0.0, 2.0);
        assertThat(result, is(0.4));
    }

    @Test
    public void testScaleValueInverted() {
        double result = valueNormalizer.scaleValue(13, 10, 20, 1.0, -1.0);
        assertEquals("", result, 0.4, 0.01);
    }

    @Test
    public void testScaleOutsideOfValueOnBottom() {
        double result = valueNormalizer.scaleValue(9, 10, 20, -1.0, 1.0);
        assertThat(result, is(-1.0));
    }

    @Test
    public void testScaleOutsideOfValueOnTop() {
        double result = valueNormalizer.scaleValue(21, 10, 20, -1.0, 1.0);
        assertThat(result, is(1.0));
    }

    @Test
    public void testScaleInsideOfValueInverted() {
        double result = valueNormalizer.scaleValue(20, 30, 10, -1.0, 1.0);
        assertThat(result, is(0.0));
    }
    
    @Test
    public void testUnscaleByDefault(){
        Double result = valueNormalizer.unScaleValueByDefault(0.5, 10, 30);
        assertEquals((Double)25.0, result);
    }

    @Test
    public void testWeirdCase() {
        double result = valueNormalizer.scaleValue(90, 0.0, 90.0, 0.0, ValueNormalizer.DEFAULT_LOW_VALUE);
        assertThat(result, is(-1.0));
    }

    @Test
    public void testScaleValueWithValueLowerThenInputHigh() {
        double result = valueNormalizer.scaleValue(5, 30, 10, ValueNormalizer.DEFAULT_LOW_VALUE, 4.0);
        assertThat(result, is(4.0));
    }

    @Test
    public void testScaleValueWithValueHigherThenInputLow() {
        double result = valueNormalizer.scaleValue(20, 10, 5, -3.0, 3.0);
        assertThat(result, is(-3.0));
    }
}
