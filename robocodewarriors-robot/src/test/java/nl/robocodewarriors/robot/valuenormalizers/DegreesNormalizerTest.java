package nl.robocodewarriors.robot.valuenormalizers;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class DegreesNormalizerTest {

    private DegreesNormalizer degreesNormalizer;

    @Before
    public void before() {
        ValueNormalizer valueNormalizer = new ValueNormalizer();
        degreesNormalizer = new DegreesNormalizer(valueNormalizer);
    }

    @Test
    public void test300() {
        NormalizedDegrees normalizedDegrees = degreesNormalizer.normalize(300);
        assertThat(normalizedDegrees.getLeftRightValue(), is(2.0/3.0));
     //  assertThat(normalizedDegrees.getUpDownValue(), is(-1.0/3.0), 0.01);
        assertEquals("", normalizedDegrees.getUpDownValue(), (-1.0/3.0), 0.01);
    }

    @Test
    public void test0() {
        NormalizedDegrees normalizedDegrees = degreesNormalizer.normalize(0);
        assertThat(normalizedDegrees.getLeftRightValue(), is(0.0));
        assertThat(normalizedDegrees.getUpDownValue(), is(-1.0));
    }

    @Test
    public void test90() {
        NormalizedDegrees normalizedDegrees = degreesNormalizer.normalize(90);
        assertThat(normalizedDegrees.getLeftRightValue(), is(-1.0));
        assertThat(normalizedDegrees.getUpDownValue(), is(0.0));
    }

    @Test
    public void test180() {
        NormalizedDegrees normalizedDegrees = degreesNormalizer.normalize(180);
        assertThat(normalizedDegrees.getLeftRightValue(), is(0.0));
        assertThat(normalizedDegrees.getUpDownValue(), is(1.0));
    }

    @Test
    public void test270() {
        NormalizedDegrees normalizedDegrees = degreesNormalizer.normalize(270);
        assertThat(normalizedDegrees.getLeftRightValue(), is(1.0));
        assertThat(normalizedDegrees.getUpDownValue(), is(0.0));
    }

    @Test
    public void test45() {
        NormalizedDegrees normalizedDegrees = degreesNormalizer.normalize(45);
        assertThat(normalizedDegrees.getLeftRightValue(), is(-0.5));
        assertThat(normalizedDegrees.getUpDownValue(), is(-0.5));
    }
}
