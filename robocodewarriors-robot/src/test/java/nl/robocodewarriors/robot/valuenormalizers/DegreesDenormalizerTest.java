package nl.robocodewarriors.robot.valuenormalizers;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DegreesDenormalizerTest {

    private DegreesDenormalizer degreesDenormalizer;

    private ValueNormalizer valueNormalizer;

    @Before
    public void before() {
        valueNormalizer = mock(ValueNormalizer.class);
        degreesDenormalizer = new DegreesDenormalizer(valueNormalizer);
    }

    @Test
    public void testNorth() {
        NormalizedDegrees normalizedDegrees = new NormalizedDegrees();
        normalizedDegrees.setLeftRightValue(0.0);
        normalizedDegrees.setUpDownValue(-1.0);

        assertEquals("", 0, degreesDenormalizer.calculateDegrees(normalizedDegrees), 0.01);
    }

    @Test
    public void testEast() {
        NormalizedDegrees normalizedDegrees = new NormalizedDegrees();
        normalizedDegrees.setLeftRightValue(-1.0);
        normalizedDegrees.setUpDownValue(0.0);
        when(valueNormalizer.scaleValue(0.0, -1.0, 1.0, 0.0, 180.0)).thenReturn(90.0);

        assertThat(degreesDenormalizer.calculateDegrees(normalizedDegrees), is(equalTo(90.0)));
    }

    @Test
    public void testSouth() {
        NormalizedDegrees normalizedDegrees = new NormalizedDegrees();
        normalizedDegrees.setLeftRightValue(0.0);
        normalizedDegrees.setUpDownValue(1.0);
        when(valueNormalizer.scaleValue(1.0, -1.0, 1.0, 0.0, 180.0)).thenReturn(180.0);

        assertThat(degreesDenormalizer.calculateDegrees(normalizedDegrees), is(equalTo(180.0)));
    }

    @Test
    public void testWest() {
        NormalizedDegrees normalizedDegrees = new NormalizedDegrees();
        normalizedDegrees.setLeftRightValue(1.0);
        normalizedDegrees.setUpDownValue(0.0);
        when(valueNormalizer.scaleValue(0.0, -1.0, 1.0, 0.0, 180.0)).thenReturn(90.0);

        assertThat(degreesDenormalizer.calculateDegrees(normalizedDegrees), is(equalTo(270.0)));
    }

    @Test
    public void testNorthEast() {
        NormalizedDegrees normalizedDegrees = new NormalizedDegrees();
        normalizedDegrees.setLeftRightValue(-0.5);
        normalizedDegrees.setUpDownValue(-0.5);
        when(valueNormalizer.scaleValue(-0.5, -1.0, 1.0, 0.0, 180.0)).thenReturn(45.0);

        assertThat(degreesDenormalizer.calculateDegrees(normalizedDegrees), is(equalTo(45.0)));
    }

}
