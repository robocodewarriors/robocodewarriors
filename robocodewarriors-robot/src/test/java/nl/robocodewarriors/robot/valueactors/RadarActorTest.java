package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RadarActorTest {
    
    private ValueNormalizer valueNormalizer = mock(ValueNormalizer.class);
    private NeuralRobot neuralRobot = mock(NeuralRobot.class);
    private RadarActor radarActor;

    @Before
    public void before() {
        radarActor = new RadarActor(valueNormalizer);
    }

    @Test
    public void testRadarIsNull() {
        radarActor.act(neuralRobot, new HashMap<String, Double>());
    }

    @Test
    public void testRadarRight() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_LEFT, 0.0);
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_RIGHT, 0.5);

        radarActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnRadarRight(45.0);
    }

    @Test
    public void testRadarLeft() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_LEFT, 0.4);
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_RIGHT, 0.0);

        radarActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnRadarRight(-45.0);
    }

    @Test
    public void testRadarLeftOrRight() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_LEFT, 0.4);
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_RIGHT, 0.4);

        radarActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnRadarRight(0.0);
    }

    @Test
    public void testRadarNothing() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_LEFT, 0.0);
        data.put(NeuralRobot.OUTPUT_RADAR_TURN_RIGHT, 0.0);

        radarActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnRadarRight(0.0);
    }
    
    @Test
    public void testNoTurning() {
    }

}
