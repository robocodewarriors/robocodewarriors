package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DrivingActorTest {

    private DrivingActor drivingActor;
    private ValueNormalizer valueNormalizer = mock(ValueNormalizer.class);
    private NeuralRobot advancedRobot = mock(NeuralRobot.class);

    @Before
    public void before() {
        drivingActor = new DrivingActor(valueNormalizer);
        drivingActor.setMaximumVelocity(4.0);
    }

    @Test
    public void testActOnAhead() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_DRIVE_AHEAD, 0.1);
        data.put(NeuralRobot.OUTPUT_DRIVE_BACKWARDS, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setAhead(100.0);
    }

    @Test
    public void testActOnBackwards() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_DRIVE_AHEAD, 0.0);
        data.put(NeuralRobot.OUTPUT_DRIVE_BACKWARDS, 0.2);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setAhead(-100.0);
    }

    @Test
    public void testActOnBackwardsAndForward() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_DRIVE_AHEAD, 0.2);
        data.put(NeuralRobot.OUTPUT_DRIVE_BACKWARDS, 0.2);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setAhead(0.0);
    }

    @Test
    public void testActOnNoInput() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_DRIVE_AHEAD, 0.0);
        data.put(NeuralRobot.OUTPUT_DRIVE_BACKWARDS, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setAhead(0.0);
    }

    @Test
    public void testActOnNulls() {
        Map<String, Double> data = new HashMap<String, Double>();
        drivingActor.act(advancedRobot, data);
    }

    @Test
    public void testTurnRight() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_LEFT, 0.0);
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT, 0.5);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setTurnRight(20.0);
    }

    @Test
    public void testTurnLeft() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_LEFT, 0.3);
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setTurnRight(-20.0);
    }

    @Test
    public void testTurnLeftAndRight() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_LEFT, 0.3);
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT, 0.4);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setTurnRight(0.0);
    }

    @Test
    public void testNotTurning() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_LEFT, 0.0);
        data.put(NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setTurnRight(0.0);
    }

    @Test
    public void testAccelerate() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_SHOULD_ACCELERATE, 0.6);
        data.put(NeuralRobot.OUTPUT_SHOULD_DECELERATE, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setMaxVelocity(5.0);

    }

    @Test
    public void testDecelerate() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_SHOULD_ACCELERATE, 0.0);
        data.put(NeuralRobot.OUTPUT_SHOULD_DECELERATE, 0.7);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setMaxVelocity(3.0);
    }

    @Test
    public void testDecelerateAndAccelerate() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_SHOULD_ACCELERATE, 0.4);
        data.put(NeuralRobot.OUTPUT_SHOULD_DECELERATE, 0.7);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setMaxVelocity(4.0);
    }

    @Test
    public void testNotDecelerateAndAccelerate() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_SHOULD_ACCELERATE, 0.0);
        data.put(NeuralRobot.OUTPUT_SHOULD_DECELERATE, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setMaxVelocity(4.0);
    }

    @Test
    public void testNotBelowZero() {
        drivingActor.setMaximumVelocity(0.5);

        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_SHOULD_ACCELERATE, 0.0);
        data.put(NeuralRobot.OUTPUT_SHOULD_DECELERATE, 0.7);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setMaxVelocity(0.0);
    }

    @Test
    public void testNotAboveMaxVelocity() {
        drivingActor.setMaximumVelocity(7.5);

        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_SHOULD_ACCELERATE, 0.7);
        data.put(NeuralRobot.OUTPUT_SHOULD_DECELERATE, 0.0);

        drivingActor.act(advancedRobot, data);
        verify(advancedRobot).setMaxVelocity(8.0);
    }


}
