package nl.robocodewarriors.robot.valueactors;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;
import nl.robocodewarriors.robot.valuenormalizers.ValueNormalizer;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class GunActorTest {
    private GunActor gunActor;
    private ValueNormalizer valueNormalizer = mock(ValueNormalizer.class);
    private NeuralRobot neuralRobot = mock(NeuralRobot.class);

    @Before
    public void before() {
        gunActor = new GunActor(valueNormalizer);
    }

    @Test
    public void testMoveTurretLeft() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_GUN_TURN_LEFT, 0.5);
        data.put(NeuralRobot.OUTPUT_GUN_TURN_RIGHT, 0.0);

        gunActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnGunRight(-10.0);
    }

    @Test
    public void testMoveTurretRight() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_GUN_TURN_LEFT, 0.0);
        data.put(NeuralRobot.OUTPUT_GUN_TURN_RIGHT, 0.4);

        gunActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnGunRight(10.0);
    }

    @Test
    public void testMoveTurretRightAndLeft() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_GUN_TURN_LEFT, 0.5);
        data.put(NeuralRobot.OUTPUT_GUN_TURN_RIGHT, 0.4);

        gunActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnGunRight(0.0);
    }

    @Test
    public void testNotMoveTurret() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_GUN_TURN_LEFT, 0.0);
        data.put(NeuralRobot.OUTPUT_GUN_TURN_RIGHT, 0.0);

        gunActor.act(neuralRobot, data);
        verify(neuralRobot).setTurnGunRight(0.0);
    }

    @Test
    public void testNullValues() {
        gunActor.act(neuralRobot, new HashMap<String, Double>());
    }

    @Test
    public void testFire() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_FIRE, 0.1);
        gunActor.act(neuralRobot, data);

        verify(neuralRobot).fire(isA(Double.class));
        verify(neuralRobot).fire(1.0);
    }

    @Test
    public void testNotFire() {
        Map<String, Double> data = new HashMap<String, Double>();
        data.put(NeuralRobot.OUTPUT_FIRE, 0.0);
        gunActor.act(neuralRobot, data);

        verify(neuralRobot, never()).fire(isA(Double.class));
    }

}
