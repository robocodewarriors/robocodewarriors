package nl.robocodewarriors.robot.neuralnetwork;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NeuralNetworkImplTest {

    @Test
    public void testGetJson() {
        NeuralNetworkImpl networkImpl = new NeuralNetworkImpl();
        assertEquals("{}", networkImpl.getJson());
    }
}
