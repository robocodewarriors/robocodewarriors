package nl.robocodewarriors.robot.neuralnetwork;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NeuralNetworkFactoryTest {
    
    @Test
    public void testGetNeuralNetwork() {
        NeuralNetworkFactory networkFactory = new NeuralNetworkFactory();
        NeuralNetwork result = networkFactory.getNeuralNetwork();
        assertTrue(result instanceof NeuralNetwork);
    }
}
