package nl.robocodewarriors.robot.neuralrobot;

import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class NeuralRobotTest {

    @Test
    public void testStaticMethodGetInput() {
        Set<String> inputs = new HashSet<String>();
        String[] inputKeys = new String[]{
                NeuralRobot.INPUT_HIT_ROBOT_IS_MY_FAULT,
                NeuralRobot.INPUT_HIT_ROBOT_LESS_ENERGY,
                NeuralRobot.INPUT_HIT_ROBOT_HAS_LESS_ENERGY,
                NeuralRobot.INPUT_HIT_ROBOT_HAS_EQUAL_OR_MORE_ENERGY,
                NeuralRobot.INPUT_HIT_WALL_FRONT,
                NeuralRobot.INPUT_HIT_WALL_RIGHT,
                NeuralRobot.INPUT_HIT_WALL_LEFT,
                NeuralRobot.INPUT_HIT_WALL_REAR,
                NeuralRobot.INPUT_NEAR_WALL_FRONT,
                NeuralRobot.INPUT_NEAR_WALL_RIGHT,
                NeuralRobot.INPUT_NEAR_WALL_LEFT,
                NeuralRobot.INPUT_NEAR_WALL_REAR,
                NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_GUN,
                NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_GUN,
                NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_RADAR,
                NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_RADAR,
                NeuralRobot.INPUT_SCANNED_ROBOT_REAR_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_FRONT_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_LEFT_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_RIGHT_TO_ROBOT,
                NeuralRobot.INPUT_SCANNED_ROBOT_HAS_LESS_ENERGY,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_FRONT,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_RIGHT,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_REAR,
                NeuralRobot.INPUT_HIT_BY_BULLET_ON_LEFT,
                NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_LEFT,
                NeuralRobot.INPUT_PREVIOUS_RADAR_TURNED_RIGHT,
                NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_LEFT,
                NeuralRobot.INPUT_PREVIOUS_BODY_TURNED_RIGHT,
                NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_LEFT,
                NeuralRobot.INPUT_PREVIOUS_GUN_TURNED_RIGHT,
                NeuralRobot.INPUT_ROBOT_MOVING_BACKWARD,
                NeuralRobot.INPUT_ROBOT_DECELERATED,
                NeuralRobot.INPUT_ROBOT_MOVING_AHEAD,
                NeuralRobot.INPUT_ROBOT_ACCELERATED,
                NeuralRobot.INPUT_GUN_IS_READY,
                NeuralRobot.INPUT_ENOUGH_HEALTH_TO_FIRE,
                NeuralRobot.INPUT_SCANNED_ROBOT_AHEAD_OF_GUN,
                NeuralRobot.INPUT_NEAR_ROBOT_FRONT,
                NeuralRobot.INPUT_NEAR_ROBOT_RIGHT,
                NeuralRobot.INPUT_NEAR_ROBOT_LEFT,
                NeuralRobot.INPUT_NEAR_ROBOT_REAR
        };
        Collections.addAll(inputs, inputKeys);

        assertThat(NeuralRobot.getInputKeys(), is(inputs));
    }

    @Test
    public void testStaticMethodGetOutputs() {
        Set<String> outputs = new HashSet<String>();
        String[] outputKeys = new String[]{
                NeuralRobot.OUTPUT_RADAR_TURN_LEFT,
                NeuralRobot.OUTPUT_RADAR_TURN_RIGHT,
                NeuralRobot.OUTPUT_GUN_TURN_LEFT,
                NeuralRobot.OUTPUT_GUN_TURN_RIGHT,
                NeuralRobot.OUTPUT_ROBOT_TURN_LEFT,
                NeuralRobot.OUTPUT_ROBOT_TURN_RIGHT,
                NeuralRobot.OUTPUT_DRIVE_AHEAD,
                NeuralRobot.OUTPUT_DRIVE_BACKWARDS,
                NeuralRobot.OUTPUT_SHOULD_ACCELERATE,
                NeuralRobot.OUTPUT_SHOULD_DECELERATE,
                NeuralRobot.OUTPUT_FIRE
        };
        Collections.addAll(outputs, outputKeys);

        assertThat(NeuralRobot.getOutputKeys(), is(outputs));
    }

}
