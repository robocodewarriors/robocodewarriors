package nl.robocodewarriors.robot.network;

import nl.robocodewarriors.neuralnetwork.*;
import nl.robocodewarriors.neuralnetwork.HiddenLayerImpl;
import nl.robocodewarriors.neuralnetwork.OutputLayerImpl;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class NetworkDeserializerTest {

    private NetworkDeserializer networkDeserializer;

    @Before
    public void before() {
        networkDeserializer = new NetworkDeserializer("{\"l\":[{\"@\":\".InputLayerImpl\",\"n\":[{\"@\":\".NeuronImpl\",\"k\":\"InputA\",\"t\":0.425627888835304,\"w\":[]},{\"@\":\".NeuronImpl\",\"k\":\"InputB\",\"t\":0.14079391620558002,\"w\":[]}]},{\"@\":\".HiddenLayerImpl\",\"n\":[{\"@\":\".NeuronImpl\",\"k\":null,\"t\":-0.8483031331386712,\"w\":[-0.18074558875575808,0.4386106221945727]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":-0.4731495627489626,\"w\":[-0.9442509085197306,-0.38475845594170743]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":0.7447237988267357,\"w\":[-0.8615444692974754,0.716153214690799]}]},{\"@\":\".HiddenLayerImpl\",\"n\":[{\"@\":\".NeuronImpl\",\"k\":null,\"t\":0.2702254844195304,\"w\":[0.3931092463281758,-0.638806667261018,-0.6696911238964098]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":0.11207675943493545,\"w\":[-0.127114414904413,0.7404352966202619,0.2958254565518763]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":0.5384963247490195,\"w\":[-0.8211157942862146,-0.5061375271157755,0.44318940148258856]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":-0.05001324656148909,\"w\":[0.3454080341638548,-0.18330458479614897,0.6321655106077353]}]},{\"@\":\".HiddenLayerImpl\",\"n\":[{\"@\":\".NeuronImpl\",\"k\":null,\"t\":-0.9288770416757992,\"w\":[-0.981956231460146,0.7680423287803448,-0.7549645821793722,0.7349604301596127]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":-0.09040722153900993,\"w\":[0.18016822308784808,-0.23674206582248059,-0.5557851825510767,0.9592703356462531]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":-0.11772694677102824,\"w\":[0.3192133045043679,0.5650799586209623,-0.8043218229172624,-0.18662001833215514]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":0.6866272637769777,\"w\":[0.7717642062146781,0.8985742674624762,0.4609334059658594,0.568513275117035]},{\"@\":\".NeuronImpl\",\"k\":null,\"t\":0.05427137650924729,\"w\":[0.9586783371315855,0.7810656210448448,0.4559688460433251,-0.09218755085847863]}]},{\"@\":\".OutputLayerImpl\",\"n\":[{\"@\":\".NeuronImpl\",\"k\":\"OutputA\",\"t\":0.10015030117036816,\"w\":[0.5960632831035042,0.2424037233378984,0.34187358294009695,-0.5231713082950895,-0.691078756694893]},{\"@\":\".NeuronImpl\",\"k\":\"OutputB\",\"t\":0.11802165167104417,\"w\":[0.7257347953719699,-0.47100332547874424,0.8868743817923477,0.16264930972110925,-0.3694750998627112]}]}]}\n");
    }

    @Test
    public void testLayers() throws Exception {
        Network neuralNetwork = networkDeserializer.getNetwork();
        List<Layer> layers = neuralNetwork.getLayers();
        assertThat(layers.size(), is(5));

        assertThat(layers.get(0), instanceOf(InputLayerImpl.class));
        assertThat(layers.get(1), instanceOf(HiddenLayerImpl.class));
        assertThat(layers.get(2), instanceOf(HiddenLayerImpl.class));
        assertThat(layers.get(3), instanceOf(HiddenLayerImpl.class));
        assertThat(layers.get(4), instanceOf(OutputLayerImpl.class));
    }

    @Test
    public void testInputLayer() throws Exception {
        Network neuralNetwork = networkDeserializer.getNetwork();
        List<Layer> layers = neuralNetwork.getLayers();

        Layer inputLayer = layers.get(0);

        assertThat(inputLayer.getNeurons().size(), is(2));

        Neuron inputA = inputLayer.getNeurons().get(0);
        assertThat(inputA.getKey(), is("InputA"));
        assertThat(inputA.getThreshold(), is(0.43));
        assertThat(inputA.getWeights().size(), is(0));

        Neuron inputB = inputLayer.getNeurons().get(1);
        assertThat(inputB.getKey(), is("InputB"));
        assertThat(inputB.getThreshold(), is(0.14));
        assertThat(inputB.getWeights().size(), is(0));
    }

    @Test
    public void testHiddenLayer1() throws Exception {
        Network neuralNetwork = networkDeserializer.getNetwork();
        List<Layer> layers = neuralNetwork.getLayers();

        Layer hiddenLayer = layers.get(1);

        assertThat(hiddenLayer.getNeurons().size(), is(3));

        Neuron neuron1 = hiddenLayer.getNeurons().get(0);
        assertThat(neuron1.getKey(), is(nullValue()));
        assertThat(neuron1.getThreshold(), is(-0.85));

        List<Double> weights = neuron1.getWeights();
        assertThat(weights.size(), is(2));

        assertThat(weights.get(0), is(-0.18));
        assertThat(weights.get(1), is(0.44));

        Neuron neuron2 = hiddenLayer.getNeurons().get(1);
        assertThat(neuron2.getKey(), is(nullValue()));
        assertThat(neuron2.getThreshold(), is(-0.47));

        weights = neuron2.getWeights();
        assertThat(weights.size(), is(2));

        assertThat(weights.get(0), is(-0.94));
        assertThat(weights.get(1), is(-0.38));

        Neuron neuron3 = hiddenLayer.getNeurons().get(2);
        assertThat(neuron3.getKey(), is(nullValue()));
        assertThat(neuron3.getThreshold(), is(0.74));

        weights = neuron3.getWeights();
        assertThat(weights.size(), is(2));

        assertThat(weights.get(0), is(-0.86));
        assertThat(weights.get(1), is(0.72));
    }

    @Test
    public void testHiddenLayer2() throws Exception {
        Network neuralNetwork = networkDeserializer.getNetwork();
        List<Layer> layers = neuralNetwork.getLayers();

        Layer hiddenLayer = layers.get(2);

        assertThat(hiddenLayer.getNeurons().size(), is(4));

        Neuron neuron1 = hiddenLayer.getNeurons().get(0);
        assertThat(neuron1.getKey(), is(nullValue()));
        assertThat(neuron1.getThreshold(), is(0.27));
        List<Double> weights = neuron1.getWeights();
        assertThat(weights.size(), is(3));
        assertThat(weights.get(0), is(0.39));
        assertThat(weights.get(1), is(-0.64));
        assertThat(weights.get(2), is(-0.67));

        Neuron neuron2 = hiddenLayer.getNeurons().get(1);
        assertThat(neuron2.getKey(), is(nullValue()));
        assertThat(neuron2.getThreshold(), is(0.11));
        weights = neuron2.getWeights();
        assertThat(weights.size(), is(3));
        assertThat(weights.get(0), is(-0.13));
        assertThat(weights.get(1), is(0.74));
        assertThat(weights.get(2), is(0.3));

        Neuron neuron3 = hiddenLayer.getNeurons().get(2);
        assertThat(neuron3.getKey(), is(nullValue()));
        assertThat(neuron3.getThreshold(), is(0.54));
        weights = neuron3.getWeights();
        assertThat(weights.size(), is(3));
        assertThat(weights.get(0), is(-0.82));
        assertThat(weights.get(1), is(-0.51));
        assertThat(weights.get(2), is(0.44));


        Neuron neuron4 = hiddenLayer.getNeurons().get(3);
        assertThat(neuron4.getKey(), is(nullValue()));
        assertThat(neuron4.getThreshold(), is(-0.05));
        weights = neuron4.getWeights();
        assertThat(weights.size(), is(3));
        assertThat(weights.get(0), is(0.35));
        assertThat(weights.get(1), is(-0.18));
        assertThat(weights.get(2), is(0.63));

    }

    @Test
    public void testHiddenLayer3() throws Exception {
        Network neuralNetwork = networkDeserializer.getNetwork();
        List<Layer> layers = neuralNetwork.getLayers();

        Layer hiddenLayer = layers.get(3);

        assertThat(hiddenLayer.getNeurons().size(), is(5));

        Neuron neuron1 = hiddenLayer.getNeurons().get(0);
        assertThat(neuron1.getKey(), is(nullValue()));
        assertThat(neuron1.getThreshold(), is(-0.93));
        List<Double> weights = neuron1.getWeights();
        assertThat(weights.size(), is(4));
        assertThat(weights.get(0), is(-0.98));
        assertThat(weights.get(1), is(0.77));
        assertThat(weights.get(2), is(-0.75));
        assertThat(weights.get(3), is(0.73));

        Neuron neuron2 = hiddenLayer.getNeurons().get(1);
        assertThat(neuron2.getKey(), is(nullValue()));
        assertThat(neuron2.getThreshold(), is(-0.09));
        weights = neuron2.getWeights();
        assertThat(weights.size(), is(4));
        assertThat(weights.get(0), is(0.18));
        assertThat(weights.get(1), is(-0.24));
        assertThat(weights.get(2), is(-0.56));
        assertThat(weights.get(3), is(0.96));

        Neuron neuron3 = hiddenLayer.getNeurons().get(2);
        assertThat(neuron3.getKey(), is(nullValue()));
        assertThat(neuron3.getThreshold(), is(-0.12));
        weights = neuron3.getWeights();
        assertThat(weights.size(), is(4));
        assertThat(weights.get(0), is(0.32));
        assertThat(weights.get(1), is(0.57));
        assertThat(weights.get(2), is(-0.80));
        assertThat(weights.get(3), is(-0.19));

        Neuron neuron4 = hiddenLayer.getNeurons().get(3);
        assertThat(neuron4.getKey(), is(nullValue()));
        assertThat(neuron4.getThreshold(), is(0.69));
        weights = neuron4.getWeights();
        assertThat(weights.size(), is(4));
        assertThat(weights.get(0), is(0.77));
        assertThat(weights.get(1), is(0.90));
        assertThat(weights.get(2), is(0.46));
        assertThat(weights.get(3), is(0.57));

        Neuron neuron5 = hiddenLayer.getNeurons().get(4);
        assertThat(neuron5.getKey(), is(nullValue()));
        assertThat(neuron5.getThreshold(), is(0.05));
        weights = neuron5.getWeights();
        assertThat(weights.size(), is(4));
        assertThat(weights.get(0), is(0.96));
        assertThat(weights.get(1), is(0.78));
        assertThat(weights.get(2), is(0.46));
        assertThat(weights.get(3), is(-0.09));
    }

    @Test
    public void testOutputLayer() throws Exception {
        Network neuralNetwork = networkDeserializer.getNetwork();
        List<Layer> layers = neuralNetwork.getLayers();

        Layer outputLayer = layers.get(4);
        assertThat(outputLayer.getNeurons().size(), is(2));

        Neuron outputA = outputLayer.getNeurons().get(0);
        assertThat(outputA.getKey(), is("OutputA"));
        assertThat(outputA.getThreshold(), is(0.1));

        List<Double> weights = outputA.getWeights();
        assertThat(weights.size(), is(5));

        assertThat(weights.get(0), is(0.60));
        assertThat(weights.get(1), is(0.24));
        assertThat(weights.get(2), is(0.34));
        assertThat(weights.get(3), is(-0.52));
        assertThat(weights.get(4), is(-0.69));

        Neuron outputB = outputLayer.getNeurons().get(1);
        assertThat(outputB.getKey(), is("OutputB"));
        assertThat(outputB.getThreshold(), is(0.12));
        assertThat(outputB.getWeights().size(), is(5));

        weights = outputB.getWeights();
        assertThat(weights.size(), is(5));

        assertThat(weights.get(0), is(0.73));
        assertThat(weights.get(1), is(-0.47));
        assertThat(weights.get(2), is(0.89));
        assertThat(weights.get(3), is(0.16));
        assertThat(weights.get(4), is(-0.37));
    }
}
