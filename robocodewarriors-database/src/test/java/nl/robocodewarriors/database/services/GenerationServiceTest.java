package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.repository.GenerationRepository;
import nl.robocodewarriors.utils.DateManager;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class GenerationServiceTest {

    private GenerationService service;
    private DateManager dateManager;
    private Calendar now;
    private GenerationRepository generationRepository;

    @Before
    public void before() {
        generationRepository = mock(GenerationRepository.class);
        now = mock(Calendar.class);
        dateManager = mock(DateManager.class);
        service = new GenerationService(generationRepository, dateManager);
        when(dateManager.now()).thenReturn(now);
    }

    @Test
    public void testCreateGeneration() {
        Training training = mock(Training.class);
        Generation generation = service.createGeneration(training, 5);
        assertThat(generation.getEndDate(), is(nullValue()));
        assertThat(generation.getStartDate(), is(nullValue()));
        assertThat(generation.getGenerationIndex(), is(5));
        assertThat(generation.getTraining(), is(training));
        assertThat(generation.getExecutionGroups(), is(nullValue()));
        verify(generationRepository).save(generation);
    }
}
