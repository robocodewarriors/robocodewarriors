package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.repository.TrainingRepository;
import nl.robocodewarriors.utils.DateManager;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TrainingServiceTest {

    private TrainingService trainingService;
    private TrainingRepository repository;
    private DateManager dateManager;

    @Before
    public void before() {
        repository = mock(TrainingRepository.class);
        dateManager = mock(DateManager.class);
        trainingService = new TrainingService(repository, dateManager);
    }

    @Test
    public void testSave() {
        Training training = mock(Training.class);
        trainingService.save(training);

        verify(repository).save(training);
    }
}
