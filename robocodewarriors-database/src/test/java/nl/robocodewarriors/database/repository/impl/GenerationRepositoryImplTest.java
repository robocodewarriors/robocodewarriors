package nl.robocodewarriors.database.repository.impl;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.Generation;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;

public class GenerationRepositoryImplTest {

    private GenerationRepositoryImpl repository;
    private Session session;

    @Before
    public void before() {
        SessionFactory sessionFactory = mock(SessionFactory.class);
        session = mock(Session.class);
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        repository = new GenerationRepositoryImpl(sessionFactory);
    }

    @Test
    public void testSaveList() {
        List<Generation> generationList = Lists.newArrayList(mock(Generation.class), mock(Generation.class));
        repository.saveAll(generationList);

        verify(session).saveOrUpdate(generationList.get(0));
        verify(session).saveOrUpdate(generationList.get(1));
    }
}
