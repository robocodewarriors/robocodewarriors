package nl.robocodewarriors.database.repository.impl;

import nl.robocodewarriors.database.domain.Training;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class TrainerRepositoryImplTest {

    private TrainingRepositoryImpl repository;
    private Session session;

    @Before
    public void before() {
        SessionFactory sessionFactory = mock(SessionFactory.class);
        session = mock(Session.class);
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        repository = new TrainingRepositoryImpl(sessionFactory);
    }

    @Test
    public void testSave() {
        Training training = mock(Training.class);
        repository.save(training);
        verify(session).saveOrUpdate(training);
    }

}
