package nl.robocodewarriors.database.repository.impl;

import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ExecutionGroupIndividualRepositorImplTest {

    private ExecutionGroupIndividualRepositoryImpl repository;
    private Session session;


    @Before
    public void before() {
        SessionFactory sessionFactory = mock(SessionFactory.class);
        session = mock(Session.class);
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        repository = new ExecutionGroupIndividualRepositoryImpl(sessionFactory);
    }

    @Test
    public void testSave() {
        ExecutionGroupIndividual executionGroupIndividual = mock(ExecutionGroupIndividual.class);
        repository.save(executionGroupIndividual);
        verify(session).saveOrUpdate(executionGroupIndividual);
    }
}
