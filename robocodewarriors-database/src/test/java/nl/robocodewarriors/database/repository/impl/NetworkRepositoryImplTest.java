package nl.robocodewarriors.database.repository.impl;

import com.google.common.collect.Lists;

import nl.robocodewarriors.database.domain.Individual;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;

public class NetworkRepositoryImplTest {

    private NetworkRepositoryImpl repository;
    private Session session;

    @Before
    public void before() {
        SessionFactory sessionFactory = mock(SessionFactory.class);
        session = mock(Session.class);
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        repository = new NetworkRepositoryImpl(sessionFactory);
    }

    @Test
    public void testSave() {
        List<Individual> habitats = Lists.newArrayList(mock(Individual.class), mock(Individual.class));
        repository.saveAll(habitats);
        verify(session).saveOrUpdate(habitats.get(0));
        verify(session).saveOrUpdate(habitats.get(1));
    }

}
