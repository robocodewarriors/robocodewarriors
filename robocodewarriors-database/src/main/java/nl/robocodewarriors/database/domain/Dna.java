package nl.robocodewarriors.database.domain;

public interface Dna {

    int getScore();

}
