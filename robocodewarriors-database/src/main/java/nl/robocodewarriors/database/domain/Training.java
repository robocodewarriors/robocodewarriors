package nl.robocodewarriors.database.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.util.Calendar;

@Table(name = "training")
@Entity
public class Training {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    @NotBlank
    @Column
    private String scoreCalculation;
    @Column
    private String learningAlgorithm;
    @NotNull
    @Min(1)
    @Column
    private Integer generationCount;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar startDateTime;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar endDateTime;
    @NotNull
    @Min(2)
    @Column
    private Integer populationSize;
    @NotNull
    @Min(1)
    @Max(12)
    @Column
    private Integer roundSize;
    @NotNull
    @Min(0)
    @Column
    private Integer hiddenLayerSize;
    @NotNull
    @Min(1)
    @Column
    private Integer neuronsPerLayer;
    @NotNull
    @Min(1)
    @Column
    private Integer roundOccurrences;

    @NotNull
    @Min(0)
    @Max(100)
    @Column
    private Integer mutationRate;

    @NotNull
    @Min(0)
    @Max(100)
    @Column
    private Integer crossoverRate;

    @Column
    private String otherRobots;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getScoreCalculation() {
        return scoreCalculation;
    }

    public void setScoreCalculation(String scoreCalculation) {
        this.scoreCalculation = scoreCalculation;
    }

    public Integer getGenerationCount() {
        return generationCount;
    }

    public void setGenerationCount(Integer generationCount) {
        this.generationCount = generationCount;
    }

    public String getLearningAlgorithm() {
        return learningAlgorithm;
    }

    public void setLearningAlgorithm(String learningAlgorithm) {
        this.learningAlgorithm = learningAlgorithm;
    }

    public Calendar getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Calendar startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Calendar getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Calendar endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Integer getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(Integer populationSize) {
        this.populationSize = populationSize;
    }

    public Integer getRoundSize() {
        return roundSize;
    }

    public void setRoundSize(Integer roundSize) {
        this.roundSize = roundSize;
    }

    public Integer getHiddenLayerSize() {
        return hiddenLayerSize;
    }

    public void setHiddenLayerSize(Integer hiddenLayerSize) {
        this.hiddenLayerSize = hiddenLayerSize;
    }

    public Integer getNeuronsPerLayer() {
        return neuronsPerLayer;
    }

    public void setNeuronsPerLayer(Integer neuronsPerLayer) {
        this.neuronsPerLayer = neuronsPerLayer;
    }

    public Integer getRoundOccurrences() {
        return roundOccurrences;
    }

    public void setRoundOccurrences(Integer roundOccurrences) {
        this.roundOccurrences = roundOccurrences;
    }

    public Integer getMutationRate() {
        return mutationRate;
    }

    public void setMutationRate(Integer mutationRate) {
        this.mutationRate = mutationRate;
    }

    public Integer getCrossoverRate() {
        return crossoverRate;
    }

    public void setCrossoverRate(Integer crossoverRate) {
        this.crossoverRate = crossoverRate;
    }

    public String getOtherRobots() {
        return otherRobots;
    }

    public void setOtherRobots(String otherRobots) {
        this.otherRobots = otherRobots;
    }
}
