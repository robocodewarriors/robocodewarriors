package nl.robocodewarriors.database.domain;

import java.util.List;

public interface GeneticAlgorithm<T extends Dna> {

    List<T> nextGeneration(List<T> oldGeneration, int crossoverRate, int mutationRate);

}
