package nl.robocodewarriors.database.domain;

public enum State {
    NOT_STARTED,
    PENDING,
    FINISHED
}
