package nl.robocodewarriors.database.domain;


import javax.persistence.*;

@Table(name = "execution_group_individual")
@Entity
public class ExecutionGroupIndividual {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "execution_group")
    private ExecutionGroup executionGroup;

    @ManyToOne
    @JoinColumn(name = "individual")
    private Individual individual;

    @Column
    private Integer rank;
    @Column
    private Integer score;
    @Column
    private Integer survival;
    @Column
    private Integer lastSurvivorBonus;
    @Column
    private Integer bulletDamage;
    @Column
    private Integer bulletDamageBonus;
    @Column
    private Integer ramDamage;
    @Column
    private Integer ramDamageBonus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ExecutionGroup getExecutionGroup() {
        return executionGroup;
    }

    public void setExecutionGroup(ExecutionGroup executionGroup) {
        this.executionGroup = executionGroup;
    }

    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getSurvival() {
        return survival;
    }

    public void setSurvival(Integer survival) {
        this.survival = survival;
    }

    public Integer getLastSurvivorBonus() {
        return lastSurvivorBonus;
    }

    public void setLastSurvivorBonus(Integer lastSurvivorBonus) {
        this.lastSurvivorBonus = lastSurvivorBonus;
    }

    public Integer getBulletDamage() {
        return bulletDamage;
    }

    public void setBulletDamage(Integer bulletDamage) {
        this.bulletDamage = bulletDamage;
    }

    public Integer getBulletDamageBonus() {
        return bulletDamageBonus;
    }

    public void setBulletDamageBonus(Integer bulletDamageBonus) {
        this.bulletDamageBonus = bulletDamageBonus;
    }

    public Integer getRamDamage() {
        return ramDamage;
    }

    public void setRamDamage(Integer ramDamage) {
        this.ramDamage = ramDamage;
    }

    public Integer getRamDamageBonus() {
        return ramDamageBonus;
    }

    public void setRamDamageBonus(Integer ramDamageBonus) {
        this.ramDamageBonus = ramDamageBonus;
    }
}
