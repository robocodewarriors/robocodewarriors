package nl.robocodewarriors.database.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.List;

@Table(name = "individual")
@Entity
public class Individual implements Serializable {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column
    private String networkString;

    @Column
    private Integer score;

    @OneToMany(mappedBy = "individual")
    private List<ExecutionGroupIndividual> executionGroupIndividuals;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNetworkString() {
        return networkString;
    }

    public void setNetworkString(String networkString) {
        this.networkString = networkString;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public List<ExecutionGroupIndividual> getExecutionGroupIndividuals() {
        return executionGroupIndividuals;
    }

    public void setExecutionGroupIndividuals(List<ExecutionGroupIndividual> executionGroupIndividuals) {
        this.executionGroupIndividuals = executionGroupIndividuals;
    }
}
