package nl.robocodewarriors.database.domain;

import javax.persistence.*;

import java.util.Calendar;
import java.util.List;

@Table(name = "generation")
@Entity
public class Generation {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "training")
    private Training training;

    @ManyToOne
    @JoinColumn(name = "parent")
    private Generation parentGeneration;

    @Column(name = "generationIndex")
    private Integer generationIndex;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDateTime")
    private Calendar startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDateTime")
    private Calendar endDate;

    @OneToMany
    private List<ExecutionGroup> executionGroups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Generation getParentGeneration() {
        return parentGeneration;
    }

    public void setParentGeneration(Generation parentGeneration) {
        this.parentGeneration = parentGeneration;
    }

    public Integer getGenerationIndex() {
        return generationIndex;
    }

    public void setGenerationIndex(Integer generationIndex) {
        this.generationIndex = generationIndex;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    public List<ExecutionGroup> getExecutionGroups() {
        return executionGroups;
    }

    public void setExecutionGroups(List<ExecutionGroup> executionGroups) {
        this.executionGroups = executionGroups;
    }
}
