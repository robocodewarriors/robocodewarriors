package nl.robocodewarriors.database.domain;


import javax.persistence.*;

import java.util.List;

@Table(name = "execution_group")
@Entity
public class ExecutionGroup {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Enumerated
    @Column(name = "state")
    private State state;

    @Column(name ="startMillis")
    private Long startMillis;

    @ManyToOne
    @JoinColumn(name = "generation")
    private Generation generation;

    @OneToMany(mappedBy = "executionGroup", fetch = FetchType.EAGER)
    private List<ExecutionGroupIndividual> executionGroupIndividuals;

    @Column(name = "otherRobots")
    private String otherRobots;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Generation getGeneration() {
        return generation;
    }

    public void setGeneration(Generation generation) {
        this.generation = generation;
    }

    public List<ExecutionGroupIndividual> getExecutionGroupIndividuals() {
        return executionGroupIndividuals;
    }

    public void setExecutionGroupIndividuals(List<ExecutionGroupIndividual> executionGroupIndividuals) {
        this.executionGroupIndividuals = executionGroupIndividuals;
    }

    public Long getStartMillis() {
        return startMillis;
    }

    public void setStartMillis(Long startMillis) {
        this.startMillis = startMillis;
    }

    public String getOtherRobots() {
        return otherRobots;
    }

    public void setOtherRobots(String otherRobots) {
        this.otherRobots = otherRobots;
    }
}
