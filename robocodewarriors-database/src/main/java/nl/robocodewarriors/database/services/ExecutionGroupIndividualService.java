package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.repository.ExecutionGroupIndividualRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExecutionGroupIndividualService {

    private final ExecutionGroupIndividualRepository executionGroupIndividualRepository;

    @Autowired
    public ExecutionGroupIndividualService(ExecutionGroupIndividualRepository executionGroupIndividualRepository) {
        this.executionGroupIndividualRepository = executionGroupIndividualRepository;
    }

    public void save(ExecutionGroupIndividual executionGroupRepository) {
        executionGroupIndividualRepository.save(executionGroupRepository);
    }

    public List<ExecutionGroupIndividual> getForIndividual(Individual individual) {
        return executionGroupIndividualRepository.getForIndividualId(individual.getId());
    }

    public void saveAll(List<ExecutionGroupIndividual> individuals) {
        executionGroupIndividualRepository.saveAll(individuals);
    }

}
