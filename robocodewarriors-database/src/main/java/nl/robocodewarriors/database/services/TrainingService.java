package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.repository.TrainingRepository;
import nl.robocodewarriors.utils.DateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {

    private TrainingRepository repository;
    private DateManager dateManager;

    @Autowired
    public TrainingService(TrainingRepository repository, DateManager dateManager) {
        this.repository = repository;
        this.dateManager = dateManager;
    }

    public void save(Training training) {
        repository.save(training);
    }

    public void finishTraining(Training training) {
        training.setEndDateTime(dateManager.now());
        save(training);
    }
}
