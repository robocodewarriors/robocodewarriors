package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.Generation;
import org.springframework.dao.CannotAcquireLockException;

import java.util.List;

public interface ExecutionGroupService {

    ExecutionGroup nextNotStarted() throws CannotAcquireLockException;

    void save(ExecutionGroup executionGroup);

    List<ExecutionGroup> allPending();

    void saveAll(List<ExecutionGroup> executionGroups);
    
    boolean hasFinishedGeneration(Generation generation);

    ExecutionGroup get(Long executionGroupId);
}
