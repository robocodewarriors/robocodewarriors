package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.repository.IndividualRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndividualService {

    private IndividualRepository individualRepository;

    @Autowired
    public IndividualService(IndividualRepository individualRepository) {
        this.individualRepository = individualRepository;
    }

    public void save(Individual individual) {
        individualRepository.save(individual);
    }

    public List<Individual> getByGeneration(Generation unfinishedGeneration) {
        return individualRepository.getAllByGenerationId(unfinishedGeneration.getId());
    }
}
