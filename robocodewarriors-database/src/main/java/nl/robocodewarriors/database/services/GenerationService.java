package nl.robocodewarriors.database.services;

import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.repository.GenerationRepository;
import nl.robocodewarriors.utils.DateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenerationService {

    private GenerationRepository generationRepository;
    private DateManager dateManager;

    @Autowired
    public GenerationService(GenerationRepository generationRepository, DateManager dateManager) {
        this.generationRepository = generationRepository;
        this.dateManager = dateManager;
    }

    public Generation createGeneration(Training training, int index) {
        Generation generation = new Generation();
        generation.setTraining(training);
        generation.setGenerationIndex(index);
        generationRepository.save(generation);
        return generation;
    }

    public List<Generation> getUnfinishedGenerations() {
        return generationRepository.getUnfinishedGenerations();
    }


    public void startGeneration(Generation generation) {
        generation.setStartDate(dateManager.now());
        generationRepository.save(generation);
    }

    public void finishGeneration(Generation generation) {
        generation.setEndDate(dateManager.now());
        generationRepository.save(generation);
    }

}
