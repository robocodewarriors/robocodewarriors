package nl.robocodewarriors.database.services.impl;

import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.domain.State;
import nl.robocodewarriors.database.repository.ExecutionGroupRepository;
import nl.robocodewarriors.database.services.ExecutionGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ExecutionGroupServiceImpl implements ExecutionGroupService {

    private ExecutionGroupRepository executionGroupRepository;

    @Autowired
    public ExecutionGroupServiceImpl(ExecutionGroupRepository executionGroupRepository) {
        this.executionGroupRepository = executionGroupRepository;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Override
    public ExecutionGroup nextNotStarted() throws CannotAcquireLockException {
        ExecutionGroup executionGroup = executionGroupRepository.getNextExecutionGroup();
        if (executionGroup != null) {
            executionGroup.setState(State.PENDING);
            executionGroup.setStartMillis(System.currentTimeMillis());
            executionGroupRepository.save(executionGroup);
        }
        return executionGroup;
    }

    @Transactional
    @Override
    public void save(ExecutionGroup executionGroup) {
        executionGroupRepository.save(executionGroup);
    }

    @Transactional
    public List<ExecutionGroup> allPending() {
        return executionGroupRepository.allPending();
    }

    @Transactional    
    public void saveAll(List<ExecutionGroup> executionGroups) {
        executionGroupRepository.saveAll(executionGroups);
    }

    @Transactional    
    public boolean hasFinishedGeneration(Generation generation) {
        List<ExecutionGroup> unfinishedGroups = executionGroupRepository.getUnfinishedExecutionGroups(generation.getId());
        return unfinishedGroups.isEmpty();
    }

    @Transactional    
    public ExecutionGroup get(Long executionGroupId) {
        return executionGroupRepository.get(executionGroupId);
    }
}
