package nl.robocodewarriors.database.repository;

import nl.robocodewarriors.database.domain.Individual;

import java.util.List;

public interface IndividualRepository {

    void saveAll(List<Individual> networks);

    void save(Individual individual);

    List<Individual> getAllByGenerationId(Long generationId);
}
