package nl.robocodewarriors.database.repository.impl;

import nl.robocodewarriors.database.domain.Individual;
import nl.robocodewarriors.database.repository.IndividualRepository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class NetworkRepositoryImpl implements IndividualRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public NetworkRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveAll(List<Individual> individuals) {
        for (Individual individual : individuals) {
            save(individual);
        }
    }

    @Override
    public void save(Individual individual) {
        sessionFactory.getCurrentSession().saveOrUpdate(individual);
    }

    @Override
    public List<Individual> getAllByGenerationId(Long generationId) {
//        String queryString = "SELECT H FROM Individual AS H "+
//        "LEFT OUTER JOIN ExecutionGroupIndividual as EGH " +
//        "ON  EGH.individual = H.id "+
//        "LEFT OUTER JOIN ExecutionGroup as EG " +
//        "ON  EGH.execution_group = EG.id " +
//        "WHERE EG.generation = :generationId " +
//        "GROUP BY H.id ";
//
//        Query query = sessionFactory.getCurrentSession().createQuery(queryString);
//        query.setParameter("generationId", generationId);

        Criteria individualCriteria = sessionFactory.getCurrentSession().createCriteria(Individual.class);
        individualCriteria.createCriteria("executionGroupIndividuals").
                createCriteria("executionGroup")
                .createCriteria("generation")
                    .add(Restrictions.eq("id", generationId));
        return individualCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }
}
