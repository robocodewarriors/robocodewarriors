package nl.robocodewarriors.database.repository.impl;

import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;
import nl.robocodewarriors.database.repository.ExecutionGroupIndividualRepository;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ExecutionGroupIndividualRepositoryImpl implements ExecutionGroupIndividualRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ExecutionGroupIndividualRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(ExecutionGroupIndividual executionGroupIndividual) {
        sessionFactory.getCurrentSession().saveOrUpdate(executionGroupIndividual);
    }

    @Override
    public List<ExecutionGroupIndividual> getForIndividualId(Long individualId) {
        return sessionFactory.getCurrentSession().createCriteria(ExecutionGroupIndividual.class)
                .createCriteria("individual").add(Restrictions.eq("id", individualId)).list();
    }

    @Override
    public void saveAll(List<ExecutionGroupIndividual> individuals) {
        for (ExecutionGroupIndividual individual : individuals) {
            sessionFactory.getCurrentSession().saveOrUpdate(individual);
        }
    }

}
