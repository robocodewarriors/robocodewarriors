package nl.robocodewarriors.database.repository;

import nl.robocodewarriors.database.domain.Training;

import java.util.List;

public interface TrainingRepository {

    void save(Training training);

    List<Integer> retrieveAllIds();

    Training find(Integer id);

    Integer size();

    Integer getIdOfFirstRecord();

    Integer getIdOfLastRecord();

    Integer getIdOfNextRecord(Integer id);

    Integer getIdOfPreviousRecord(Integer id);

    List<Training> findAll(Integer start, Integer pageSize);
}
