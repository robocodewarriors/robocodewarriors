package nl.robocodewarriors.database.repository.impl;

import org.hibernate.*;
import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.State;
import nl.robocodewarriors.database.repository.ExecutionGroupRepository;

import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ExecutionGroupRepositoryImpl implements ExecutionGroupRepository {

    private static final Logger logger = LoggerFactory.getLogger(ExecutionGroupRepositoryImpl.class);
    private SessionFactory sessionFactory;

    @Autowired
    public ExecutionGroupRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(ExecutionGroup executionGroup) {
        sessionFactory.getCurrentSession().saveOrUpdate(executionGroup);
    }

    @Override
    public ExecutionGroup getNextExecutionGroup() {
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("select * from (select id, generation, otherRobots, state, startMillis from execution_group where state = 0 order by generation, rand()) z limit 1");
        query.addEntity(ExecutionGroup.class);
        return (ExecutionGroup) query.uniqueResult();
    }

    @Override
    public List<ExecutionGroup> allPending() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select e from ExecutionGroup as e where e.state = :state");
        query.setParameter("state", State.PENDING);
        return (List<ExecutionGroup>) query.list();
    }

    @Override
    public void saveAll(List<ExecutionGroup> executionGroups) {
        for (ExecutionGroup executionGroup : executionGroups) {
            save(executionGroup);
        }
    }

    @Override
    public List<ExecutionGroup> getUnfinishedExecutionGroups(Long generationId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ExecutionGroup.class);
        criteria.add(Restrictions.not(Restrictions.eq("state", State.FINISHED)));
        criteria.createCriteria("generation").add(Restrictions.eq("id", generationId));
        return criteria.list();
    }

    @Override
    public ExecutionGroup get(Long executionGroupId) {
        return (ExecutionGroup) sessionFactory.getCurrentSession().createCriteria(ExecutionGroup.class).add(Restrictions.eq("id", executionGroupId)).uniqueResult();
    }
}