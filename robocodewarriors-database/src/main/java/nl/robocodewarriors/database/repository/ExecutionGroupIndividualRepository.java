package nl.robocodewarriors.database.repository;

import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;

import java.util.List;

public interface ExecutionGroupIndividualRepository {

    void save(ExecutionGroupIndividual executionGroupIndividual);

    List<ExecutionGroupIndividual> getForIndividualId(Long individualId);

    void saveAll(List<ExecutionGroupIndividual> individuals);
}
