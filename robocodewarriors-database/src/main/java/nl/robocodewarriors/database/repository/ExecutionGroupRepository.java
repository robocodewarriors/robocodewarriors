package nl.robocodewarriors.database.repository;

import nl.robocodewarriors.database.domain.ExecutionGroup;

import java.util.List;

public interface ExecutionGroupRepository {

    void save(ExecutionGroup executionGroup);

    ExecutionGroup getNextExecutionGroup();

    List<ExecutionGroup> allPending();

    void saveAll(List<ExecutionGroup> executionGroups);

    List<ExecutionGroup> getUnfinishedExecutionGroups(Long generationId);

    ExecutionGroup get(Long executionGroupId);
}
