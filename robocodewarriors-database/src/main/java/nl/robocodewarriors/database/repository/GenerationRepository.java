package nl.robocodewarriors.database.repository;

import nl.robocodewarriors.database.domain.Generation;

import java.util.List;

public interface GenerationRepository {

    void saveAll(List<Generation> generations);

    void save(Generation generation);

    List<Generation> getUnfinishedGenerations();
}
