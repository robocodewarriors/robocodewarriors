package nl.robocodewarriors.database.repository.impl;

import nl.robocodewarriors.database.domain.Training;
import nl.robocodewarriors.database.repository.TrainingRepository;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TrainingRepositoryImpl implements TrainingRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TrainingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    @Override
    public void save(Training training) {
        sessionFactory.getCurrentSession().saveOrUpdate(training);
    }

    @Override
    public List<Integer> retrieveAllIds() {
        return sessionFactory.getCurrentSession().createQuery("SELECT T.id FROM Training T").list();
    }

    @Override
    public Training find(Integer id) {
        return (Training) sessionFactory.getCurrentSession().get(Training.class, (long) id);
    }

    public List<Training> findAll() {
        return sessionFactory.getCurrentSession().createCriteria(Training.class).list();
    }

    @Override
    public Integer size() {
        return longToInteger((Long) sessionFactory.getCurrentSession().createCriteria(Training.class).setProjection(Projections.rowCount()).uniqueResult());
    }

    @Override
    public Integer getIdOfFirstRecord() {
        return longToInteger((Long) sessionFactory.getCurrentSession().createQuery("SELECT min(T.id) FROM Training T").uniqueResult());
    }

    @Override
    public Integer getIdOfLastRecord() {
        return longToInteger((Long) sessionFactory.getCurrentSession().createQuery("SELECT max(T.id) FROM Training T").uniqueResult());
    }

    @Override
    public Integer getIdOfNextRecord(Integer id) {
        return longToInteger((Long) sessionFactory.getCurrentSession().createQuery("SELECT min(T.id) FROM Training T WHERE T.id > " + id).uniqueResult());
    }

    @Override
    public Integer getIdOfPreviousRecord(Integer id) {
        return longToInteger((Long) sessionFactory.getCurrentSession().createQuery("SELECT max(T.id) FROM Training T WHERE T.id < " + id).uniqueResult());
    }

    @Override
    public List<Training> findAll(Integer start, Integer pageSize) {
        return sessionFactory.getCurrentSession().createCriteria(Training.class).setFirstResult(start).setMaxResults(pageSize).list();
    }

    private Integer longToInteger(Long value) {
        return value != null ? value.intValue() : null;
    }
}
