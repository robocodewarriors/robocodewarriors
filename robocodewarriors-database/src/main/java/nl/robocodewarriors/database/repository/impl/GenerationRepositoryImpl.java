package nl.robocodewarriors.database.repository.impl;

import nl.robocodewarriors.database.domain.Generation;
import nl.robocodewarriors.database.repository.GenerationRepository;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class GenerationRepositoryImpl implements GenerationRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public GenerationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveAll(List<Generation> generations) {
        for (Generation generation : generations) {
            save(generation);
        }
    }

    @Override
    public void save(Generation generation) {
        sessionFactory.getCurrentSession().saveOrUpdate(generation);
    }

    @Override
    public List<Generation> getUnfinishedGenerations() {
        Query query = sessionFactory.getCurrentSession().createQuery("SELECT g FROM Generation AS g WHERE startDateTime IS NOT NULL AND endDateTime IS NULL");
        return query.list();
    }

//    @Override
//    public Generation getNextGeneration(Long generationId, Long trainingId) {
//        Query query = sessionFactory.getCurrentSession().createQuery("SELECT G, min(G.id) FROM Generation G WHERE G.training = :trainingId and G.id > :generationId");
//        query.setParameter("trainingId", trainingId);
//        query.setParameter("generationId", generationId);
//        return (Generation) query.uniqueResult();
//    }

}
