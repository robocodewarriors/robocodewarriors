DROP TABLE IF EXISTS execution_group_individual;
DROP TABLE IF EXISTS execution_group;
DROP TABLE IF EXISTS individual;
DROP TABLE IF EXISTS generation;
DROP TABLE IF EXISTS training;

CREATE TABLE training (
    id INT NOT NULL AUTO_INCREMENT,
    scoreCalculation TEXT NOT NULL,
    learningAlgorithm VARCHAR(255) NOT NULL,
    generationCount INT NOT NULL,
    startDateTime TIMESTAMP NOT NULL,
    endDateTime TIMESTAMP NULL,
    populationSize INT NOT NULL,
    roundSize INT NOT NULL,
    hiddenLayerSize INT NOT NULL,
    neuronsPerLayer INT NOT NULL,
    roundOccurrences INT NOT NULL,
    mutationRate INT NOT NULL,
    crossoverRate INT NOT NULL,
    otherRobots TEXT NULL,
    CONSTRAINT pk_training PRIMARY KEY(id)
);

CREATE TABLE generation (
    id INT NOT NULL AUTO_INCREMENT,
    training INT NOT NULL,
    generationIndex INT NOT NULL,
    parent INT NULL,
    startDateTime TIMESTAMP NOT NULL,
    endDateTime TIMESTAMP NULL,
    CONSTRAINT pk_generation PRIMARY KEY(id),
    CONSTRAINT ak_generation UNIQUE(training, generationIndex),
    CONSTRAINT fk_generation_training FOREIGN KEY (training) REFERENCES training(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_generation_parent FOREIGN KEY (parent) REFERENCES generation(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE individual (
    id INT NOT NULL AUTO_INCREMENT,
    networkString BLOB NOT NULL,
    score INT NULL,
    CONSTRAINT pk_individual PRIMARY KEY(id)
);

CREATE TABLE execution_group (
    id INT NOT NULL AUTO_INCREMENT,
    generation INT NOT NULL,
    state INT NOT NULL,
    startMillis BIGINT NULL,
    otherRobots TEXT NULL,
    CONSTRAINT pk_network PRIMARY KEY(id),
    CONSTRAINT fk_execution_group_generation FOREIGN KEY (generation) REFERENCES generation(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE execution_group_individual (
    id INT NOT NULL AUTO_INCREMENT,
    execution_group INT NOT NULL,
    individual INT NOT NULL,
    rank INT NULL,
    score INT NULL,
    survival INT NULL,
    lastSurvivorBonus INT NULL,
    bulletDamage INT NULL,
    bulletDamageBonus INT NULL,
    ramDamage INT NULL,
    ramDamageBonus INT NULL,
    CONSTRAINT pk_network PRIMARY KEY(id),
    CONSTRAINT fk_execution_group_individual_individual FOREIGN KEY (individual) REFERENCES individual(id)
            ON UPDATE CASCADE
            ON DELETE CASCADE,
    CONSTRAINT fk_execution_group_individual_execution_group FOREIGN KEY (execution_group) REFERENCES execution_group(id)
            ON UPDATE CASCADE
            ON DELETE CASCADE
);

