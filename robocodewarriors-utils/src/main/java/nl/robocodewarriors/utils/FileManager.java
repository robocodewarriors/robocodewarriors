package nl.robocodewarriors.utils;

import com.google.common.io.Files;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class FileManager {

    public File getFile(String location) {
        return new File(location);
    }

    public File getFile(File parent, String location) {
        return new File(parent, location);
    }

    public OutputStreamWriter getOutputStreamWriter(File file) throws FileNotFoundException {
        return new OutputStreamWriter(new FileOutputStream(file));
    }

    /**
     * Create all directories above this file in the file structure.
     * @param file
     */
    public void createDirectories(File file) {
        file.getParentFile().mkdirs();
    }

    /**
     * Moves a file to a new location.
     * @param from File to move.
     * @param to Location to move to.
     * @return The created file, same as to.
     */
    public File move(File from, File to) throws IOException {
        Files.move(from, to);

        return to;
    }
}
