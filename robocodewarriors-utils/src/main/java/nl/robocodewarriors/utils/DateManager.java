package nl.robocodewarriors.utils;

import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.GregorianCalendar;

@Component
public class DateManager {

    public Calendar now() {
        return new GregorianCalendar();
    }

}