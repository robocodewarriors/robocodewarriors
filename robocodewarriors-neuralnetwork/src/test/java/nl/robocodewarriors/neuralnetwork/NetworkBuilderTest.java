package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.*;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class NetworkBuilderTest {

    private NetworkBuilder networkBuilder;

    @Before
    public void before() {
        networkBuilder = new NetworkBuilderImpl();
    }

    @Test
    public void testBuildingEasyNetwork() {
        Network network = networkBuilder
                .inputs("InputA", "InputB")
                .hiddenLayers(4)
                .outputs("OutputA", "OutputB", "OutputC")
                .build();

        assertThat(network, is(notNullValue()));
        assertThat(network.getLayers().size(), is(3));


        Layer inputLayer = network.getLayers().get(0);
        assertThat(inputLayer, is(instanceOf(InputLayerImpl.class)));
        assertThat(inputLayer.getNeurons().size(), is(2));

        Neuron inputNeuron1 = inputLayer.getNeurons().get(0);
        assertThat(inputNeuron1, is(instanceOf(NeuronImpl.class)));
        assertThat((inputNeuron1).getKey(), is("InputA"));
        assertThat(inputNeuron1.getThreshold(), is(notNullValue()));

        Neuron inputNeuron2 = inputLayer.getNeurons().get(1);
        assertThat(inputNeuron2, is(instanceOf(NeuronImpl.class)));
        assertThat(inputNeuron2.getKey(), is("InputB"));
        assertThat(inputNeuron2.getThreshold(), is(notNullValue()));


        Layer hiddenLayer = network.getLayers().get(1);
        assertThat(hiddenLayer , is(instanceOf(HiddenLayerImpl.class)));
        assertThat(hiddenLayer.getNeurons().size(), is(4));

        Neuron hiddenNeuron1 = hiddenLayer.getNeurons().get(0);
        assertThat(hiddenNeuron1, is(instanceOf(NeuronImpl.class)));
        assertThat(hiddenNeuron1.getThreshold(), is(notNullValue()));
        assertThat(hiddenNeuron1.getWeights(), is(notNullValue()));
        assertThat(hiddenNeuron1.getWeights().size(), is(2));

        Neuron hiddenNeuron2 = hiddenLayer.getNeurons().get(0);
        assertThat(hiddenNeuron2, is(instanceOf(NeuronImpl.class)));
        assertThat(hiddenNeuron2.getThreshold(), is(notNullValue()));
        assertThat(hiddenNeuron2.getWeights(), is(notNullValue()));
        assertThat(hiddenNeuron2.getWeights().size(), is(2));

        Neuron hiddenNeuron3 = hiddenLayer.getNeurons().get(0);
        assertThat(hiddenNeuron3, is(instanceOf(NeuronImpl.class)));
        assertThat(hiddenNeuron3.getThreshold(), is(notNullValue()));
        assertThat(hiddenNeuron3.getWeights(), is(notNullValue()));
        assertThat(hiddenNeuron3.getWeights().size(), is(2));

        Neuron hiddenNeuron4 = hiddenLayer.getNeurons().get(0);
        assertThat(hiddenNeuron4, is(instanceOf(NeuronImpl.class)));
        assertThat(hiddenNeuron4.getThreshold(), is(notNullValue()));
        assertThat(hiddenNeuron4.getWeights(), is(notNullValue()));
        assertThat(hiddenNeuron4.getWeights().size(), is(2));


        Layer outputLayer = network.getLayers().get(2);
        assertThat(outputLayer, is(instanceOf(OutputLayerImpl.class)));
        assertThat(outputLayer.getNeurons().size(), is(3));

        Neuron outputNeuron1 = outputLayer.getNeurons().get(0);
        assertThat(outputNeuron1, is(instanceOf(NeuronImpl.class)));
        assertThat(outputNeuron1.getThreshold(), is(notNullValue()));
        assertThat(outputNeuron1.getWeights(), is(notNullValue()));
        assertThat(outputNeuron1.getWeights().size(), is(4));
        assertThat(outputNeuron1.getKey(), is("OutputA"));

        Neuron outputNeuron2 = outputLayer.getNeurons().get(1);
        assertThat(outputNeuron2, is(instanceOf(NeuronImpl.class)));
        assertThat(outputNeuron2.getThreshold(), is(notNullValue()));
        assertThat(outputNeuron2.getWeights(), is(notNullValue()));
        assertThat(outputNeuron2.getWeights().size(), is(4));
        assertThat(outputNeuron2.getKey(), is("OutputB"));

        Neuron outputNeuron3 = outputLayer.getNeurons().get(2);
        assertThat(outputNeuron3, is(instanceOf(NeuronImpl.class)));
        assertThat(outputNeuron3.getThreshold(), is(notNullValue()));
        assertThat(outputNeuron3.getWeights(), is(notNullValue()));
        assertThat(outputNeuron3.getWeights().size(), is(4));
        assertThat(outputNeuron3.getKey(), is("OutputC"));
    }
}
