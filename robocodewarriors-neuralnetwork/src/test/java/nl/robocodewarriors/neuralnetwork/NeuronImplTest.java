package nl.robocodewarriors.neuralnetwork;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class NeuronImplTest {

    @Test
    public void testGetWeights() {
        List<Double> weights = Arrays.asList(1.53423, 2.567);
        NeuronImpl neuron = new NeuronImpl(null, 0.0, weights);
        assertEquals((Double) 1.53, neuron.getWeights().get(0));
        assertEquals((Double) 2.57, neuron.getWeights().get(1));
    }

    @Test
    public void testGetThreshold() {
        Double threshold = 0.6789;
        NeuronImpl neuron = new NeuronImpl(null, threshold, null);
        assertEquals((Double) 0.68, neuron.getThreshold());
    }

    @Test
    public void testGetKey() {
        NeuronImpl neuron = new NeuronImpl("test", 0.0, null);
        assertEquals("test", neuron.getKey());
    }

    @Test
    public void testCreateNeuronImpl() {
        String key = "key";
        Double threshold = 2.0;
        List<Double> weights = new ArrayList<Double>();

        NeuronImpl neuron = new NeuronImpl(key, threshold, weights);
        assertEquals(threshold, neuron.getThreshold());
        assertEquals(weights, neuron.getWeights());
        assertEquals(key, neuron.getKey());
    }

    @Test
    public void testProcessInputNeuronWithNullInput() {
        List<Double> weights = getDoublesAsList();
        List<Double> input = new ArrayList<Double>();
        input.add(null);

        NeuronImpl neuron = new NeuronImpl("test", 0.65, weights);
        Double value = neuron.process(input);
        assertThat(value, is(0.0));
    }

    @Test
    public void testProcessInputNeuronWithLowValue() {
        List<Double> weights = getDoublesAsList();
        List<Double> input = getDoublesAsList(-0.1);

        NeuronImpl neuron = new NeuronImpl("test", 0.65, weights);
        Double value = neuron.process(input);
        assertThat(value, is(0.0));
    }

    @Test
    public void testProcessInputNeuronWithHighValue() {
        List<Double> weights = getDoublesAsList();
        List<Double> input = getDoublesAsList(0.8);

        NeuronImpl neuron = new NeuronImpl("test", 0.65, weights);
        Double value = neuron.process(input);
        assertThat(value, is(1.0));
    }

    @Test
    public void testProcessInputNeuronWithInvalidInput() {
        List<Double> weights = getDoublesAsList();
        List<Double> input = getDoublesAsList(1.0, -0.35, 0.61);

        NeuronImpl neuron = new NeuronImpl("test", -0.65, weights);

        try {
            neuron.process(input);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid Input Values: More then one for neuron without weights"));
        }
    }

    @Test
    public void testNotProcessMultiInputLowValue() {
        List<Double> weights = getDoublesAsList(0.5, 0.25);
        List<Double> input = getDoublesAsList(0.8, -0.6);

        NeuronImpl neuron = new NeuronImpl(null, 0.3, weights);
        assertThat(neuron.process(input), is(0.0));
    }

    @Test
    public void testNotProcessMultiInputHighValue() {
        List<Double> weights = getDoublesAsList(0.5, 0.25);
        List<Double> input = getDoublesAsList(0.8, 0.6);

        NeuronImpl neuron = new NeuronImpl(null, 0.25, weights);
        assertThat(neuron.process(input), is(1.0));
    }

    @Test
    public void testInvalidInputs() {
        List<Double> weights = getDoublesAsList(0.5, 0.25, 0.8);
        List<Double> input = getDoublesAsList(0.8, 0.6);

        NeuronImpl neuron = new NeuronImpl("test", 0.3, weights);
        try {
            neuron.process(input);
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Number of inputvalues does not match the number of weights."));
        }
    }

    @Test
    public void testProcess_AboveThreshold() {
        List<Double> weights = getDoublesAsList(0.52, -0.63, -0.74);
        List<Double> input = getDoublesAsList(1.0, -0.35, 0.61);

        NeuronImpl neuron = new NeuronImpl("test", -0.65, weights);

        Double result = neuron.process(input);
        //((0.52 * 1.0) + (-0.63 * -0.35) + (-0.74 * 0.61))/3) = (0.52 + 0.22 + -0.45)/3 = 0.29 /3 = 0.1
        assertEquals((Double) (0.41), result);
    }

    @Test
    public void testProcess_AboveThresholdNegativeValues() {
        List<Double> weights = getDoublesAsList(0.5, 0.6, 0.7);

        NeuronImpl neuron = new NeuronImpl("test", -1.0, weights);

        List<Double> input = getDoublesAsList(-1.1, 1.2, -1.3);

        Double result = neuron.process(input);
        //((0.5 * -1.1) + (0.6 * 1.2) + (0.7 * -1.3))/3) = (-0.55 + 0.72 + -0.91)/3 = -0.74/3 = -0.25
        assertEquals((Double) (0.27), result);
    }

    @Test
    public void testProcess_BelowThreshold() {
        List<Double> weights = getDoublesAsList(0.52, -0.63, -0.74);
        List<Double> input = getDoublesAsList(1.0, -0.35, 0.61);

        NeuronImpl neuron = new NeuronImpl("test", 0.65, weights);

        Double result = neuron.process(input);
        //((0.5 * 1.1) + (0.6 * 1.2) + (0.7 * 1.3))/3) = (0.55 + 0.72 + 0.91)/3 = 2.18 /3 = 0.727
        assertEquals((Double) 0.00, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProcess_InvalidValues() {
        List<Double> weights = new ArrayList<Double>();
        weights.add(0.5);

        NeuronImpl neuron = new NeuronImpl("test", 0.65, weights);

        List<Double> input = new ArrayList<Double>();

        neuron.process(input);
    }

    @Test
    public void testSerialization() throws IOException {
        Neuron neuron = new NeuronImpl("test", 2.0, Arrays.asList(2.0, 3.0));

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(neuron);
        Neuron result = mapper.readValue(json, NeuronImpl.class);
        assertThat(result, is(neuron));
    }

    private List<Double> getDoublesAsList(Double... arguments) {
        return Arrays.asList(arguments);
    }
}
