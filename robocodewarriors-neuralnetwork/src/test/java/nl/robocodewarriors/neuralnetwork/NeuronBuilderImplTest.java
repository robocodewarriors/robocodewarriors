package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.Neuron;
import nl.robocodewarriors.neuralnetwork.NeuronBuilder;

import nl.robocodewarriors.neuralnetwork.NeuronBuilderImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class NeuronBuilderImplTest {
    
    @Test
    public void testBuild() {
        NeuronBuilder neuronBuilder = new NeuronBuilderImpl();
        Neuron result = neuronBuilder.build("test", 2.2, null);
        assertEquals("test", result.getKey());
        assertEquals((Double) 2.2, result.getThreshold());
        assertNull(result.getWeights());
    }
}
