package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NetworkImplTest {

    @Test
    public void testCreateNetworkImpl1() {
        Network network = new NetworkImpl();
        assertNull(network.getLayers());
    }

    @Test
    public void testCreateNetworkImpl2() {
        List<Layer> layers = new ArrayList<Layer>();
        Network network = new NetworkImpl(layers);
        assertEquals(layers, network.getLayers());
    }

    @Test
    public void testSetAndGetLayers() {
        List<Layer> input = new ArrayList<Layer>();
        InputLayer inputLayer = mock(InputLayer.class);
        HiddenLayer hiddenLayer = mock(HiddenLayer.class);
        OutputLayer outputLayer = mock(OutputLayer.class);

        input.add(inputLayer);
        input.add(hiddenLayer);
        input.add(hiddenLayer);
        input.add(outputLayer);

        Network network = new NetworkImpl();
        network.setLayers(input);

        assertEquals(4, network.getLayers().size());
        assertEquals(inputLayer, network.getLayers().get(0));
        assertEquals(hiddenLayer, network.getLayers().get(1));
        assertEquals(hiddenLayer, network.getLayers().get(2));
        assertEquals(outputLayer, network.getLayers().get(3));
    }

    @Test
    public void testEvaluate() {
        Map<String, Double> input = new HashMap<String, Double>();
        List<Double> midResult = new ArrayList<Double>();

        List<Layer> layers = new ArrayList<Layer>();
        InputLayer inputLayer = mock(InputLayer.class);
        HiddenLayer hiddenLayer = mock(HiddenLayer.class);
        OutputLayer outputLayer = mock(OutputLayer.class);
        layers.add(inputLayer);
        layers.add(hiddenLayer);
        layers.add(outputLayer);

        when(inputLayer.process(input)).thenReturn(midResult);
        when(hiddenLayer.process(midResult)).thenReturn(midResult);
        when(outputLayer.process(midResult)).thenReturn(input);

        assertEquals(input, new NetworkImpl(layers).evaluate(input));
    }

}
