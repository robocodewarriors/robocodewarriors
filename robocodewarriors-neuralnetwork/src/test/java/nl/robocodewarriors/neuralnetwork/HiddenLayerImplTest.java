package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.HiddenLayer;
import nl.robocodewarriors.neuralnetwork.HiddenLayerImpl;
import nl.robocodewarriors.neuralnetwork.Neuron;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HiddenLayerImplTest {

    @Test
    public void testCreateHiddenLayerImpl1() {
        HiddenLayer hiddenLayer = new HiddenLayerImpl();
        assertNull(hiddenLayer.getNeurons());
    }

    @Test
    public void testCreateHiddenLayerImpl2() {
        List<Neuron> neurons = new ArrayList<Neuron>();
        HiddenLayer hiddenLayer = new HiddenLayerImpl(neurons);
        assertEquals(neurons, hiddenLayer.getNeurons());
    }

    @Test
    public void testProcess() {
        List<Double> input = new ArrayList<Double>();
        input.add(1.1);
        input.add(1.2);
        input.add(1.3);

        List<Neuron> neurons = new ArrayList<Neuron>();
        createAndAddNeuronMockToList(0.123, neurons);
        createAndAddNeuronMockToList(0.28, neurons);
        createAndAddNeuronMockToList(0.234, neurons);

        HiddenLayer hiddenLayer = new HiddenLayerImpl(neurons);
        List<Double> result = hiddenLayer.process(input);

        assertEquals(3, result.size());
        assertEquals((Double) 0.123, result.get(0));
        assertEquals((Double) 0.28, result.get(1));
        assertEquals((Double) 0.234, result.get(2));
    }

    @Test
    public void testSetAndGetNeurons() {
        List<Neuron> neurons = new ArrayList<Neuron>();
        HiddenLayer hiddenLayer = new HiddenLayerImpl(null);
        assertNull(hiddenLayer.getNeurons());
        hiddenLayer.setNeurons(neurons);
        assertEquals(neurons, hiddenLayer.getNeurons());
    }

    private void createAndAddNeuronMockToList(Double result, List<Neuron> list) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.process(any(List.class))).thenReturn(result);
        list.add(neuron);
    }
}
