package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.Neuron;
import nl.robocodewarriors.neuralnetwork.OutputLayer;

import nl.robocodewarriors.neuralnetwork.OutputLayerImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OutputLayerImplTest {
    
    @Test
    public void testCreateOutputLayerImpl1() {
        OutputLayer outputLayer = new OutputLayerImpl();
        assertNull(outputLayer.getNeurons());
    }

    @Test
    public void testCreateOutputLayerImpl2() {
        List<Neuron> neurons = new ArrayList<Neuron>();
        OutputLayer outputLayer = new OutputLayerImpl(neurons);
        assertEquals(neurons, outputLayer.getNeurons());
    }

    @Test
    public void testProcess() {
        List<Double> input = new ArrayList<Double>();
        input.add(0.10);
        input.add(0.20);
        input.add(0.30);

        List<Neuron> neurons = new ArrayList<Neuron>();

        createAndAddNeuronMockToList("key1", 0.121, neurons);
        createAndAddNeuronMockToList("key2", 0.284, neurons);
        createAndAddNeuronMockToList("key3", 0.231, neurons);

        OutputLayer layer = new OutputLayerImpl(neurons);
        Map<String, Double> result = layer.process(input);

        assertEquals(3, result.size());
        assertEquals((Double) 0.12, result.get("key1"));
        assertEquals((Double) 0.28, result.get("key2"));
        assertEquals((Double) 0.23, result.get("key3"));
    }

    @Test
    public void testSetAndGetNeurons() {
        List<Neuron> neurons = new ArrayList<Neuron>();

        OutputLayer layer = new OutputLayerImpl(neurons);
        assertEquals(neurons, layer.getNeurons());

        neurons = new ArrayList<Neuron>();
        Neuron neuron = mock(Neuron.class);
        when(neuron.getKey()).thenReturn("key");
        neurons.add(neuron);

        layer.setNeurons(neurons);

        assertEquals(neurons, layer.getNeurons());
        assertEquals("key", layer.getNeurons().get(0).getKey());
    }

    private void createAndAddNeuronMockToList(String key, Double result, List<Neuron> list) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.process(any(List.class))).thenReturn(result);
        when(neuron.getKey()).thenReturn(key);
        list.add(neuron);
    }
}
