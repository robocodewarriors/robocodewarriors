package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.InputLayer;
import nl.robocodewarriors.neuralnetwork.InputLayerImpl;
import nl.robocodewarriors.neuralnetwork.Neuron;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class InputLayerImplTest {

    @Test
    public void testCreateInputLayerImpl1() {
        InputLayer inputLayer = new InputLayerImpl();
        assertNull(inputLayer.getNeurons());
    }

    @Test
    public void testCreateInputLayerImpl2() {
        List<Neuron> neurons = new ArrayList<Neuron>();
        InputLayer inputLayer = new InputLayerImpl(neurons);
        assertEquals(neurons, inputLayer.getNeurons());
    }

    @Test
    public void testProcess() {
        Map<String, Double> input = new HashMap<String, Double>();
        input.put("key1", 0.1);
        input.put("key2", 0.2);
        input.put("key3", 0.3);

        List<Neuron> neurons = new ArrayList<Neuron>();
        Neuron neuron1 = createAndAddNeuronMockToList("key1", 0.123, neurons);
        Neuron neuron2 = createAndAddNeuronMockToList("key2", 0.28, neurons);
        Neuron neuron3 = createAndAddNeuronMockToList("key3", 0.234, neurons);

        InputLayer layer = new InputLayerImpl(neurons);
        List<Double> result = layer.process(input);

        assertEquals(3, result.size());
        assertEquals((Double) 0.123, result.get(0));
        assertEquals((Double) 0.28, result.get(1));
        assertEquals((Double) 0.234, result.get(2));

        assertNeuronArgument(neuron1, 0.1);
        assertNeuronArgument(neuron2, 0.2);
        assertNeuronArgument(neuron3, 0.3);
    }

    @Test
    public void testSetAndGetNeurons() {
        List<Neuron> neurons = new ArrayList<Neuron>();

        InputLayer layer = new InputLayerImpl(neurons);
        assertEquals(neurons, layer.getNeurons());

        neurons = new ArrayList<Neuron>();
        Neuron neuron = mock(Neuron.class);
        when(neuron.getKey()).thenReturn("key");
        neurons.add(neuron);

        layer.setNeurons(neurons);

        assertEquals(neurons, layer.getNeurons());
        assertEquals("key", layer.getNeurons().get(0).getKey());
    }

    private Neuron createAndAddNeuronMockToList(String key, Double result, List<Neuron> list) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.process(any(List.class))).thenReturn(new Double(result));
        when(neuron.getKey()).thenReturn(key);
        list.add(neuron);

        return neuron;
    }

    private void assertNeuronArgument(Neuron neuron, Double expectedValue) {
        ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
        verify(neuron).process(argument.capture());
        assertEquals(expectedValue, argument.getValue().get(0));
    }
}
