package nl.robocodewarriors.neuralnetwork.validator;

import nl.robocodewarriors.neuralnetwork.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NetworkValidatorTest {

    private List<Double> weights;
    private NetworkValidator instance;

    @Before
    public void setUp() {
        weights = new ArrayList<Double>();
        weights.add(1.0);
        instance = new NetworkValidator();
    }

    @Test
    public void testValidate() {
        List<Layer> layers = new ArrayList<Layer>();
        List<Neuron> inputNeurons = new ArrayList<Neuron>();
        inputNeurons.add(mockNeuron("key", new ArrayList<Double>(), 1.0));
        inputNeurons.add(mockNeuron("key", new ArrayList<Double>(), 1.0));
        layers.add(mockLayer(InputLayer.class, inputNeurons));

        List<Neuron> hiddenNeurons = new ArrayList<Neuron>();
        hiddenNeurons.add(mockNeuron(null, weights, 1.0));
        hiddenNeurons.add(mockNeuron(null, weights, 1.0));
        layers.add(mockLayer(HiddenLayer.class, hiddenNeurons));

        List<Neuron> outputNeurons = new ArrayList<Neuron>();
        outputNeurons.add(mockNeuron("key", weights, 1.0));
        outputNeurons.add(mockNeuron("key", weights, 1.0));
        layers.add(mockLayer(OutputLayer.class, outputNeurons));

        assertThat(instance.validate(mockNetwork(layers)), is(true));
    }

    @Test
    public void testValidate_invalidLayerSize() {
        List<Layer> layers = new ArrayList<Layer>();

        assertThat(instance.validate(mockNetwork(layers)), is(false));
    }

    @Test
    public void testValidate_firstLayerInvalid() {
        List<Layer> layers = new ArrayList<Layer>();
        layers.add(mockLayer(HiddenLayer.class, new ArrayList<Neuron>()));
        layers.add(mockLayer(HiddenLayer.class, new ArrayList<Neuron>()));
        layers.add(mockLayer(OutputLayer.class, new ArrayList<Neuron>()));

        assertThat(new NetworkValidator().validate(mockNetwork(layers)), is(false));
    }

    @Test
    public void testValidate_InvalidNeurons() {
        List<Layer> layers = new ArrayList<Layer>();
        List<Neuron> neurons = new ArrayList<Neuron>();
        neurons.add(mockNeuron(null, weights, null));

        layers.add(mockLayer(InputLayer.class, neurons));
        layers.add(mockLayer(HiddenLayer.class, new ArrayList<Neuron>()));
        layers.add(mockLayer(OutputLayer.class, new ArrayList<Neuron>()));

        assertThat(instance.validate(mockNetwork(layers)), is(false));
    }

    @Test
    public void testValidate_midLayerInvalid() {
        List<Layer> layers = new ArrayList<Layer>();
        List<Neuron> inputNeurons = new ArrayList<Neuron>();
        inputNeurons.add(mockNeuron("key", new ArrayList<Double>(), 1.0));
        inputNeurons.add(mockNeuron("key", new ArrayList<Double>(), 1.0));
        layers.add(mockLayer(InputLayer.class, inputNeurons));

        layers.add(mockLayer(OutputLayer.class, new ArrayList<Neuron>()));
        layers.add(mockLayer(OutputLayer.class, new ArrayList<Neuron>()));

        assertThat(instance.validate(mockNetwork(layers)), is(false));
    }

    @Test
    public void testValidate_lastLayerInvalid() {
        List<Layer> layers = new ArrayList<Layer>();
        List<Neuron> inputNeurons = new ArrayList<Neuron>();
        inputNeurons.add(mockNeuron("key", new ArrayList<Double>(), 1.0));
        inputNeurons.add(mockNeuron("key", new ArrayList<Double>(), 1.0));
        layers.add(mockLayer(InputLayer.class, inputNeurons));

        List<Neuron> hiddenNeurons = new ArrayList<Neuron>();
        hiddenNeurons.add(mockNeuron(null, weights, 1.0));
        hiddenNeurons.add(mockNeuron(null, weights, 1.0));
        layers.add(mockLayer(HiddenLayer.class, hiddenNeurons));

        layers.add(mockLayer(HiddenLayer.class, new ArrayList<Neuron>()));

        assertThat(instance.validate(mockNetwork(layers)), is(false));
    }

    private Neuron mockNeuron(String key, List<Double> weights, Double threshold) {
        Neuron neuron = mock(Neuron.class);
        when(neuron.getKey()).thenReturn(key);
        when(neuron.getThreshold()).thenReturn(threshold);
        when(neuron.getWeights()).thenReturn(weights);

        return neuron;
    }

    private <T extends Layer> Layer mockLayer(Class<T> type, List<Neuron> neurons) {
        Layer layer = mock(type);
        when(layer.getNeurons()).thenReturn(neurons);
        return layer;
    }

    private Network mockNetwork(List<Layer> layers) {
        Network network = mock(Network.class);
        when(network.getLayers()).thenReturn(layers);

        return network;
    }
}
