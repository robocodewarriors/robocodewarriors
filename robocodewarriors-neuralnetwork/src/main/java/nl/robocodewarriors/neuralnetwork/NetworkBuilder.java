package nl.robocodewarriors.neuralnetwork;

public interface NetworkBuilder {

    NetworkBuilder inputs(String... inputNamesEllipsis);

    NetworkBuilder outputs(String... outputNamesEllipsis);

    NetworkBuilder hiddenLayers(Integer... layerSizesEllipsis);

    Network build();
}
