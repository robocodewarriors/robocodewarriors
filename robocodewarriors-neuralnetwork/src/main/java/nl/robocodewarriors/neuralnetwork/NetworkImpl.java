package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.*;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;
import java.util.Map;

public class NetworkImpl implements Network {

    @JsonProperty("l")
    private List<Layer> layers;

    public NetworkImpl() {
    }

    public NetworkImpl(List<Layer> layers) {
        this.layers = layers;
    }

    @Override
    public Map<String, Double> evaluate(Map<String, Double> inputMapping) {
        List<Double> result = ((InputLayer) layers.get(0)).process(inputMapping);
        for (int i = 1; i < layers.size() - 1; i++) {
            result = ((HiddenLayer) layers.get(i)).process(result);
        }

        return ((OutputLayer) layers.get(layers.size() - 1)).process(result);
    }

    @Override
    public List<Layer> getLayers() {
        return this.layers;
    }

    @Override
    public void setLayers(List<Layer> layers) {
        this.layers = layers;
    }
}
