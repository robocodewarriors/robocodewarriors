package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.Neuron;
import nl.robocodewarriors.neuralnetwork.OutputLayer;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OutputLayerImpl implements OutputLayer {

    @JsonProperty("n")
    private List<Neuron> neurons;

    public OutputLayerImpl() {
    }

    public OutputLayerImpl(List<Neuron> neurons) {
        this.neurons = neurons;
    }

    @Override
    public Map<String, Double> process(List<Double> inputValues) {
        Map<String, Double> result = new HashMap<String, Double>();
        for (Neuron neuron : neurons) {
            result.put(neuron.getKey(), round(neuron.process(inputValues), NUMBER_OF_DECIMALS));
        }

        return result;
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }

    @Override
    public final void setNeurons(List<Neuron> neurons) {
        this.neurons = neurons;
    }

    private Double round(Double number, int decimals) {
        return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
    }
}
