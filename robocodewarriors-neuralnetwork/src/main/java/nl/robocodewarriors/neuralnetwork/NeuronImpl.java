package nl.robocodewarriors.neuralnetwork;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class NeuronImpl implements Neuron {

    private static final int NUMBER_OF_DECIMALS = 2;
    @JsonProperty("k")
    private final String key;
    @JsonProperty("t")
    private final Double threshold;
    @JsonProperty("w")
    private final List<Double> weights;

    @JsonCreator
    public NeuronImpl(@JsonProperty("k") String key, @JsonProperty("t") Double threshold, @JsonProperty("w") List<Double> weights) {
        this.key = key;
        this.threshold = roundDouble(threshold);
        this.weights = weights;

        if (this.weights != null) {
            roundListOfDoubles(this.weights);
        }
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Double getThreshold() {
        return threshold;
    }

    @Override
    public List<Double> getWeights() {
        return weights;
    }

    @Override
    public Double process(List<Double> inputValues) {
        Double total = 0.00;

        if (weights.size() > 0) {
            if (weights.size() != inputValues.size()) {
                throw new IllegalArgumentException("Number of inputvalues does not match the number of weights.");
            }

            for (int i = 0; i < inputValues.size(); i++) {
                total += weights.get(i) * inputValues.get(i);
            }
            total /= inputValues.size();

        } else if (inputValues.size() == 1) {
            total = (inputValues.get(0) != null) ? inputValues.get(0) : 0.0;
        } else {
            throw new IllegalArgumentException("Invalid Input Values: More then one for neuron without weights");
        }

        if (total.compareTo(threshold) < 0) {
            return 0.00;
        }

        if (isOutputNeuron()) {
            total = getSigmoidFor(total);
            total = scaleOutput(total);
            return roundDouble(total);
        }

        total = scaleOutput(total);
        return (double) Math.round(total);
    }

    private double scaleOutputForSigmoid(Double activation) {
        return (4 * ((activation - threshold) / (2 - (threshold + 1)))) - 2;
    }

    private double getSigmoidFor(Double activation) {
        return Math.tanh(scaleOutputForSigmoid(activation));
    }

    private double scaleOutput(double d) {
        return (d + 1.0) / 2.0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NeuronImpl other = (NeuronImpl) obj;
        if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
            return false;
        }
        if (this.threshold != other.threshold && (this.threshold == null || !this.threshold.equals(other.threshold))) {
            return false;
        }
        if (this.weights != other.weights && (this.weights == null || !this.weights.equals(other.weights))) {
            return false;
        }
        return true;
    }

    private Double roundDouble(Double d) {
        return (double) Math.round(d * Math.pow(10, NUMBER_OF_DECIMALS)) / Math.pow(10, NUMBER_OF_DECIMALS);
    }

    private void roundListOfDoubles(List<Double> doubles) {
        for (int i = 0; i < doubles.size(); i++) {
            roundDouble(doubles.set(i, roundDouble(doubles.get(i))));
        }
    }

    private boolean isOutputNeuron() {
        return (weights.size() > 0 && key != null);
    }
}
