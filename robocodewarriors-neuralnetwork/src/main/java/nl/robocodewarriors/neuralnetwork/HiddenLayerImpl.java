package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.HiddenLayer;
import nl.robocodewarriors.neuralnetwork.Neuron;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class HiddenLayerImpl implements HiddenLayer {

    @JsonProperty("n")
    private List<Neuron> neurons;

    public HiddenLayerImpl() {
    }

    public HiddenLayerImpl(List<Neuron> neurons) {
        this.neurons = neurons;
    }

    @Override
    public List<Double> process(List<Double> inputValues) {
        List<Double> result = new ArrayList<Double>();
        for (Neuron neuron : neurons) {
            result.add(neuron.process(inputValues));
        }

        return result;
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }

    @Override
    public void setNeurons(List<Neuron> neurons) {
        this.neurons = neurons;
    }
}
