package nl.robocodewarriors.neuralnetwork;

import java.util.List;

public interface NeuronBuilder {

    Neuron build(String key, Double threshold, List<Double> weights);
}
