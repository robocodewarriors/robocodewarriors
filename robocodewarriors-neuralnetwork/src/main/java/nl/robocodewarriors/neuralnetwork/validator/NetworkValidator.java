package nl.robocodewarriors.neuralnetwork.validator;

import nl.robocodewarriors.neuralnetwork.*;

import java.util.List;

public class NetworkValidator {

    public static final int MINIMUM_LAYERS = 3;

    public boolean validate(Network network) {
        return validateLayers(network.getLayers());
    }

    private boolean validateLayers(List<Layer> layers) {
        if (layers.size() < MINIMUM_LAYERS) {
            return false;
        }

        if (!(layers.get(0) instanceof InputLayer) || !validateNeurons(layers.get(0).getNeurons(), true, false)) {
            return false;
        }

        for (int i = 1; i < layers.size() - 1; i++) {
            if (!(layers.get(i) instanceof HiddenLayer || !validateNeurons(layers.get(i).getNeurons(), false, true))) {
                return false;
            }
        }

        if (!(layers.get(layers.size() - 1) instanceof OutputLayer) || !validateNeurons(layers.get(layers.size() - 1).getNeurons(), true, true)) {
            return false;
        }

        return true;
    }

    private boolean validateNeurons(List<Neuron> neurons, Boolean requiresKey, Boolean requiresWeights) {
        for (Neuron neuron : neurons) {
            if ((neuron.getWeights().isEmpty()) == requiresWeights || neuron.getThreshold() == null || (neuron.getKey() == null) == requiresKey) {
                return false;
            }
        }

        return true;
    }
}
