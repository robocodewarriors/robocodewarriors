package nl.robocodewarriors.neuralnetwork;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@")
public interface Network {
    
    /**
     * @return A list of the Layer objects of the current network.
     */
    List<Layer> getLayers();

    /**n
     * Set the the list of Layer objects with the given value.
     *
     * @param layers The given value.
     */
    void setLayers(List<Layer> layers);

    /**
     * Process the output state for the given input.
     *
     * @param inputMapping The mapping between the input Neurons and the given values.
     * @return The evaluated output.
     */
    Map<String, Double> evaluate(Map<String, Double> inputMapping);
}
