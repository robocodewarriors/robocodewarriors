package nl.robocodewarriors.neuralnetwork;

import java.util.List;

public interface HiddenLayer extends Layer {

    /**
     * Processes the input from the previous HiddenLayer.
     *
     * @param inputValues The output values of the previous HiddenLayer.
     * @return The result.
     */
    List<Double> process(List<Double> inputValues);
}
