package nl.robocodewarriors.neuralnetwork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class NetworkBuilderImpl implements NetworkBuilder {

    private List<Integer> hiddenLayerSizes;
    private List<String> inputNames;
    private List<String> outputNames;
    private Random random;

    public NetworkBuilderImpl() {
        random = new Random();
    }

    @Override
    public NetworkBuilder inputs(String... inputNames) {
        this.inputNames = new ArrayList<String>();
        Collections.addAll(this.inputNames, inputNames);
        return this;
    }

    @Override
    public NetworkBuilder outputs(String... outputNames) {
        this.outputNames = new ArrayList<String>();
        Collections.addAll(this.outputNames, outputNames);
        return this;
    }

    @Override
    public NetworkBuilder hiddenLayers(Integer... hiddenLayerSizes) {
        this.hiddenLayerSizes = new ArrayList<Integer>();
        Collections.addAll(this.hiddenLayerSizes, hiddenLayerSizes);
        return this;
    }

    @Override
    public Network build() {
        List<Layer> layers = new ArrayList<Layer>();

        Layer inputLayer = buildInputLayer();
        layers.add(inputLayer);
        layers.addAll(buildHiddenLayers(inputLayer));
        layers.add(buildOutputLayer());
        return new NetworkImpl(layers);
    }

    private Layer buildOutputLayer() {
        Integer lastValue = 0;
        if (hiddenLayerSizes == null) {
            lastValue = inputNames.size();
        } else {
            lastValue = (!hiddenLayerSizes.isEmpty()) ? hiddenLayerSizes.get(hiddenLayerSizes.size() - 1) : inputNames.size();
        }
        List<Neuron> neurons = new ArrayList<Neuron>();
        List<Double> weights;

        for (String key : outputNames) {
            weights = new ArrayList<Double>();
            for (int i = 0; i < lastValue; i++) {
                weights.add(randomValue());
            }
            neurons.add(new NeuronImpl(key, randomValue(), weights));
        }
        return new OutputLayerImpl(neurons);
    }

    private List<Layer> buildHiddenLayers(Layer previousLayer) {
        if (hiddenLayerSizes != null) {
            Layer lastLayer = previousLayer;
            List<Layer> hiddenLayers = new ArrayList<Layer>();

            for (Integer hiddenLayerSize : hiddenLayerSizes) {
                lastLayer = buildHiddenLayer(hiddenLayerSize, lastLayer);
                hiddenLayers.add(lastLayer);
            }
            return hiddenLayers;
        } else {
            return new ArrayList<Layer>();
        }
    }

    private Layer buildHiddenLayer(Integer hiddenLayerSize, Layer lastLayer) {
        List<Neuron> neurons = new ArrayList<Neuron>();
        List<Double> weights;
        for (int j = 0; j < hiddenLayerSize; j++) {
            weights = new ArrayList<Double>();
            for (int i = 0; i < lastLayer.getNeurons().size(); i++) {
                weights.add(randomValue());
            }
            neurons.add(new NeuronImpl(null, random.nextDouble() - 1.0, weights));
        }
        return new HiddenLayerImpl(neurons);
    }

    private Layer buildInputLayer() {
        List<Neuron> neurons = new ArrayList<Neuron>();
        for (String key : inputNames) {
            neurons.add(new NeuronImpl(key, random.nextDouble() - 1.0, new ArrayList<Double>()));
        }
        return new InputLayerImpl(neurons);
    }

    private Double randomValue() {
        return (2.0 * random.nextDouble()) - 1.0;
    }
}
