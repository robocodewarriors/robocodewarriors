package nl.robocodewarriors.neuralnetwork;

import java.util.List;
import java.util.Map;

public interface OutputLayer extends Layer {

    final int NUMBER_OF_DECIMALS = 2;

    /**
     * Processes the input from the previous Layer.
     *
     * @param inputValues The output values of the previous Layer.
     * @return The mapped result.
     */
    Map<String, Double> process(List<Double> inputValues);
}
