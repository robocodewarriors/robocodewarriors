package nl.robocodewarriors.neuralnetwork;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@")
public interface Layer {

    /**
     * Sets the list of Neuron objects with the given value.
     *
     * @param neurons The given value.
     */
    void setNeurons(List<Neuron> neurons);

    /**
     * @return A list of Neuron objects in the current Layer.
     */
    List<Neuron> getNeurons();

}
