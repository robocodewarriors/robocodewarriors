package nl.robocodewarriors.neuralnetwork;

import nl.robocodewarriors.neuralnetwork.InputLayer;
import nl.robocodewarriors.neuralnetwork.Neuron;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InputLayerImpl implements InputLayer {

    @JsonProperty("n")
    private List<Neuron> neurons;

    public InputLayerImpl() {
    }

    public InputLayerImpl(List<Neuron> neurons) {
        this.neurons = neurons;
    }

    @Override
    public List<Double> process(Map<String, Double> inputValues) {
        List<Double> result = new ArrayList<Double>();
        List<Double> tempList;

        for (Neuron neuron : neurons) {
            tempList = new ArrayList<Double>();
            tempList.add(inputValues.get(neuron.getKey()));
            result.add(neuron.process(tempList));
        }

        return result;
    }

    @Override
    public List<Neuron> getNeurons() {
        return neurons;
    }

    @Override
    public final void setNeurons(List<Neuron> neurons) {
        this.neurons = neurons;
    }
}
