package nl.robocodewarriors.neuralnetwork;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@")
public interface Neuron extends Cloneable {

    /**
     * @return The threshold
     */
    Double getThreshold();

    /**
     * @return A list of weights for each of the underlying Neuron.
     */
    List<Double> getWeights();

    /**
     * @return The key.
     */
    String getKey();

    /**
     * Processes the list of input values.
     *
     * @param inputValues The list of input values.
     * @return The calculated value.
     */
    Double process(List<Double> inputValues);
}
