package nl.robocodewarriors.neuralnetwork;

import java.util.List;

public class NeuronBuilderImpl implements NeuronBuilder {

    @Override
    public Neuron build(String key, Double threshold, List<Double> weights) {
        return new NeuronImpl(key, threshold, weights);
    }
}
