package nl.robocodewarriors.neuralnetwork;

import java.util.List;
import java.util.Map;

public interface InputLayer extends Layer {

    /**
     * Processes the normalized input.
     *
     * @param inputValues The mapped input.
     * @return The result.
     */
    List<Double> process(Map<String, Double> inputValues);
}
