package nl.robocodewarriors.robot.neuralrobot.impl;

import nl.robocodewarriors.robot.neuralrobot.NeuralRobot;

public class NeuralRobotImpl${identifier} extends NeuralRobot {

    @Override
    public String getNetworkJson() {
        return "${json}";
    }
}