#Robocode Robot
robot.description=Neural Robot ${identifier}
robot.include.source=true
robocode.version=1.7.4.2
robot.version=1.0
robot.author.name=Robocode Warriors
robot.classname=nl.robocodewarriors.robot.neuralrobot.impl.NeuralRobotImpl${identifier}