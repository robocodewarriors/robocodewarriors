package nl.robocodewarriors.charles;

import nl.robocodewarriors.charles.execution.ExecutionException;
import nl.robocodewarriors.charles.robotbuilder.RobotCompileException;
import nl.robocodewarriors.charles.training.TrainerException;
import nl.robocodewarriors.charles.worker.PersistingExecutionGroupWorker;
import nl.robocodewarriors.charles.worker.SingleExecutionGroupWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

import freemarker.template.TemplateException;
import robocode.control.RobocodeEngine;

class Executor {

    private static final Logger logger = LoggerFactory.getLogger(Executor.class);

    public static void main(String... args) throws IOException, TemplateException, RobotCompileException, ExecutionException, TrainerException, InterruptedException {
        ApplicationContext context = new AnnotationConfigApplicationContext("nl.robocodewarriors");

        String executionGroupParameter = System.getProperty("executor.execution.group");
        if (executionGroupParameter != null && !executionGroupParameter.equals("-1")) {
            SingleExecutionGroupWorker singleExecutionGroupWorker = context.getBean(SingleExecutionGroupWorker.class);
            RobocodeEngine engine = context.getBean(RobocodeEngine.class);
            engine.setVisible(true);
            singleExecutionGroupWorker.run(Long.parseLong(executionGroupParameter));
            return;
        }
        PersistingExecutionGroupWorker worker = context.getBean(PersistingExecutionGroupWorker.class);
        while (true) {
            worker.run();
        }
    }
}
