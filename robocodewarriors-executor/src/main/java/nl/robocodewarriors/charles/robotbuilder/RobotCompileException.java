package nl.robocodewarriors.charles.robotbuilder;

public class RobotCompileException extends Exception {
    public RobotCompileException(String message) {
        super(message);
    }

    public RobotCompileException(String message, Exception cause) {
        super(message, cause);
    }
}
