package nl.robocodewarriors.charles.domain;

import nl.robocodewarriors.neuralnetwork.Network;

public class NetworkScore {
    private final Network network;
    private final Integer score;

    public NetworkScore(Network network, Integer score) {
        this.network = network;
        this.score = score;
    }

    public Network getNetwork() {
        return network;
    }

    public Integer getScore() {
        return score;
    }

    @Override
    public boolean equals(Object other) {
        NetworkScore otherNetworkScore = (NetworkScore) other;
        return otherNetworkScore.network.equals(network) && otherNetworkScore.score == score;
    }
}
