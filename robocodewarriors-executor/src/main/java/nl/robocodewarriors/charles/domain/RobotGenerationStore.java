package nl.robocodewarriors.charles.domain;

import robocode.control.RobotResults;
import robocode.control.RobotSpecification;

public class RobotGenerationStore {

    private final RobotSpecification robotSpecification;
    private RobotResults result;

    public RobotGenerationStore(RobotSpecification robotSpecification) {
        this.robotSpecification = robotSpecification;
    }


    public RobotSpecification getRobotSpecification() {
        return robotSpecification;
    }

    public void setResult(RobotResults result) {
        this.result = result;
    }

    public RobotResults getResult() {
        return result;
    }

}
