package nl.robocodewarriors.charles.worker;

import nl.robocodewarriors.charles.training.RobocodeTrainer;
import nl.robocodewarriors.charles.training.TrainerException;
import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;
import nl.robocodewarriors.database.domain.State;
import nl.robocodewarriors.database.services.ExecutionGroupIndividualService;
import nl.robocodewarriors.database.services.ExecutionGroupService;

import org.hibernate.exception.LockTimeoutException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PersistingExecutionGroupWorker {
    private static final int NAP_TIME = 10000;

    private static final Logger logger = LoggerFactory.getLogger(PersistingExecutionGroupWorker.class);
    private final RobocodeTrainer trainer;
    private final ExecutionGroupService executionGroupService;
    private final ExecutionGroupIndividualService executionGroupIndividualService;
	
	private int numberOfSuccesFullExecutions = 0 ;

    @Autowired
    public PersistingExecutionGroupWorker(RobocodeTrainer trainer, ExecutionGroupService executionGroupService, ExecutionGroupIndividualService executionGroupIndividualService) {
        this.trainer = trainer;
        this.executionGroupService = executionGroupService;
        this.executionGroupIndividualService = executionGroupIndividualService;
    }

    public void run() throws TrainerException, InterruptedException {
        DateTime dateTime = DateTime.now();
        ExecutionGroup executionGroup = null;
        try {
            logger.info("Fetching execution group...");
            executionGroup = executionGroupService.nextNotStarted();
        } catch (CannotAcquireLockException e) {
            logger.info("Deadlock");
            return;
        } catch (LockTimeoutException e) {
            logger.info("Lock timeout");
            return;
        } catch (Throwable e) {
            logger.warn("Could not acquire next execution group...");
            e.printStackTrace();;
        }

        if (executionGroup == null) {
            logger.info("No execution group. Taking a nap...");
            Thread.sleep(NAP_TIME);
            return;
        }

        logger.info("Starting execution group: " + executionGroup.getId());
        try {
            List<ExecutionGroupIndividual> executionGroupIndividuals = trainer.handleGenerations(executionGroup);

            boolean failed = false;
            for (ExecutionGroupIndividual individual : executionGroup.getExecutionGroupIndividuals()) {
                if (individual.getScore() == null) {
                    failed = true;
                }
            }
            if (!failed) {
                executionGroupIndividualService.saveAll(executionGroup.getExecutionGroupIndividuals());
                executionGroup.setState(State.FINISHED);
                long difference = DateTime.now().getMillis() - dateTime.getMillis();

                executionGroupService.save(executionGroup);
                logger.info("Finished execution group: " + executionGroup.getId()
                        + ", saved: " + executionGroup.getExecutionGroupIndividuals().size() + "/" + executionGroupIndividuals.size() + " individuals"
                        + ", took: " + difference / 1000.0f + " seconds"
						+ ", total executed: " +  (++numberOfSuccesFullExecutions));

            } else {
                executionGroup.setStartMillis(null);
                executionGroup.setState(State.NOT_STARTED);
                executionGroupService.save(executionGroup);
                logger.info("Failed execution group BY UNSET DATA: " + executionGroup.getId());
            }
        } catch (Throwable e) {
            logger.info("Failed execution group: " + executionGroup.getId());
        }
    }
}
