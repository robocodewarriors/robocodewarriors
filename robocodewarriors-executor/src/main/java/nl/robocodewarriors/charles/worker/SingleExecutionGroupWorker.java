package nl.robocodewarriors.charles.worker;

import com.google.common.collect.Lists;
import nl.robocodewarriors.charles.execution.ExecutionException;
import nl.robocodewarriors.charles.execution.RobocodeExecutor;
import nl.robocodewarriors.charles.training.TrainerException;
import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;
import nl.robocodewarriors.database.services.ExecutionGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SingleExecutionGroupWorker {

    private static final Logger logger = LoggerFactory.getLogger(SingleExecutionGroupWorker.class);
    private final RobocodeExecutor robocodeExecutor;
    private final ExecutionGroupService executionGroupService;

    @Autowired
    public SingleExecutionGroupWorker(RobocodeExecutor robocodeExecutor, ExecutionGroupService executionGroupService) {
        this.robocodeExecutor = robocodeExecutor;
        this.executionGroupService = executionGroupService;
    }

    public void run(Long executionGroupId) throws TrainerException, InterruptedException {
        ExecutionGroup executionGroup = executionGroupService.get(executionGroupId);
        logger.info("Starting Single Execution Group: " + executionGroup.getId());

        List<String> networkList = new ArrayList<String>();
        for (ExecutionGroupIndividual executionGroupHabitatat : executionGroup.getExecutionGroupIndividuals()) {
            networkList.add(executionGroupHabitatat.getIndividual().getNetworkString());
        }

        List<String> otherRobots = Lists.newArrayList();
        if (executionGroup.getOtherRobots() != null) {
            String[] otherRobotsArray= executionGroup.getOtherRobots().split(",");

            for (String otherRobotString : otherRobotsArray) {
                if (otherRobotString.length() > 0) {
                    otherRobots.add(otherRobotString);
                }
            }
        }

        try {
            robocodeExecutor.start(networkList, otherRobots);
            logger.info("Finished execution group: " + executionGroup.getId());
        } catch (ExecutionException e) {
            logger.error("Failed execution group: " + executionGroup.getId());
            logger.error(e.getMessage(), e);
        }

        robocodeExecutor.close();
    }

}
