package nl.robocodewarriors.charles.context;

import com.google.doclava.JarUtils;
import nl.robocodewarriors.charles.execution.BattleLogger;
import nl.robocodewarriors.neuralnetwork.NeuronBuilder;
import nl.robocodewarriors.neuralnetwork.NeuronBuilderImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import robocode.control.RobocodeEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarFile;

@Configuration
public class RobocodeContext {

    private static final String MANIFEST_PATH = "META-INF/MANIFEST.MF";
    private final Properties properties = new Properties();

    public RobocodeContext() {
        loadProperties();
    }

    @Bean
    public NeuronBuilder neuronBuilder() {
        return new NeuronBuilderImpl();
    }

    @Bean
    public RobocodeEngine robocodeEngine(@Qualifier("robocodeExecutionDirectory") File executionDir, BattleLogger battleLogger) throws URISyntaxException, IOException {
        ManifestReader manifestReader = new ManifestReader(getClass().getClassLoader().getResources(MANIFEST_PATH), new ManifestFactory());

        String classPath = manifestReader.readManifestClassPath();
        System.setProperty("java.class.path", classPath);
        RobocodeEngine robocodeEngine = new RobocodeEngine(executionDir);
        robocodeEngine.addBattleListener(battleLogger);
        robocodeEngine.setVisible(false);

        return robocodeEngine;
    }

    @Bean(name = "robocodeExecutionDirectory")
    public File robocodeExecutionDirectory() throws IOException {
        String jarFile = RobocodeContext.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        JarUtils.copyResourcesToDirectory(new JarFile(jarFile), "robocode", System.getProperty("executor.execution.directory"));
        File file = new File(System.getProperty("executor.execution.directory"));
        file.mkdirs();
        return file;
    }

    @Bean
    public BattleLogger battleLogger() {
        return new BattleLogger(false);
    }

    private void checkProperties() {
        String[] requiredProperties = {"charles.battle.field.width", "charles.battle.field.height", "charles.battle.rounds"};

        List propertiesList = new ArrayList(Arrays.asList(requiredProperties));
        propertiesList.removeAll(properties.stringPropertyNames());

        if (!propertiesList.isEmpty()) {
            throw new ConfigurationException("Properties file does not contain all required properties, missing: " + propertiesList.toString());
        }
    }

    private void loadProperties() {
        try {
            System.out.println(System.getProperty("executor.config"));
            properties.load(new FileInputStream(new File(System.getProperty("executor.config"))));
        } catch (IOException ex) {
            throw new ConfigurationException("Could not load properties file. Is the system property 'executor.config' set well?", ex);
        }
    }
}
