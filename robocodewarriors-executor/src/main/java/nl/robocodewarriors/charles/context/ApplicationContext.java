package nl.robocodewarriors.charles.context;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import java.io.IOException;
import java.net.URISyntaxException;

import freemarker.template.DefaultObjectWrapper;

@EnableTransactionManagement
@Configuration
@PropertySource("classpath:datasource.properties")
public class ApplicationContext {

    @Autowired
    private Environment environment;

    @Bean
    public freemarker.template.Configuration freemarker() throws IOException, URISyntaxException {
        freemarker.template.Configuration freemarker = new freemarker.template.Configuration();
        freemarker.setObjectWrapper(new DefaultObjectWrapper());
        freemarker.setClassForTemplateLoading(ApplicationContext.class, "/");
        return freemarker;
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public Runtime runtime() {
        return Runtime.getRuntime();
    }

    @Bean
    public DataSource dataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();

        dataSource.setUrl(environment.getProperty("datasource.url"));
        dataSource.setUser(environment.getProperty("datasource.username"));
        dataSource.setPassword(environment.getProperty("datasource.password"));

        return dataSource;
    }

    @Bean
    public SessionFactory sessionFactory(DataSource dataSource) {
        return new LocalSessionFactoryBuilder(dataSource)
                .scanPackages("nl.robocodewarriors.database.domain")
//                .setProperty("hibernate.show_sql", "true")
                .buildSessionFactory();
    }

    @Bean
    public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }
}
