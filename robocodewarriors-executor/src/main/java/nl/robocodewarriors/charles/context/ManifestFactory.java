package nl.robocodewarriors.charles.context;

import java.io.IOException;
import java.net.URL;
import java.util.jar.Manifest;

public class ManifestFactory {

    public Manifest createManifest(URL manifestLocation) throws IOException {
        return new Manifest(manifestLocation.openStream());
    }
}
