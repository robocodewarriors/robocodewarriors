package nl.robocodewarriors.charles.context;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class ManifestReader {

    private static final String MAIN_CLASS_PATH = "nl/robocodewarriors/charles/Executor";
    
    private final Enumeration<URL> manifestResources;
    private final ManifestFactory manifestFactory;
    
    public ManifestReader(Enumeration<URL> manifestResources, ManifestFactory manifestFactory) {
        this.manifestResources = manifestResources;
        this.manifestFactory = manifestFactory;
    }

    public String readManifestClassPath() throws IOException {
        while (manifestResources.hasMoreElements()) {
            Manifest manifest = manifestFactory.createManifest(manifestResources.nextElement());
            
            if (MAIN_CLASS_PATH.equals(manifest.getMainAttributes().getValue(Attributes.Name.MAIN_CLASS))) {
                String entry = manifest.getMainAttributes().getValue(Attributes.Name.CLASS_PATH);
                entry = entry.replace("file:///", "");
                return entry.replace(" ", File.pathSeparator);
            }
        }

        throw new ConfigurationException("Manifest file not found.");
    }

}
