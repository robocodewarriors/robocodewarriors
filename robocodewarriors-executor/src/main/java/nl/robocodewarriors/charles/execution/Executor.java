package nl.robocodewarriors.charles.execution;

import nl.robocodewarriors.charles.domain.RobotGenerationStore;

import java.util.List;

public interface Executor {

    List<RobotGenerationStore> start(List<String> networkList, List<String> otherRobots) throws ExecutionException;
}
