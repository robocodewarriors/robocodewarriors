package nl.robocodewarriors.charles.execution;

import com.google.common.collect.Lists;
import nl.robocodewarriors.charles.domain.RobotGenerationStore;
import robocode.control.RobotResults;
import robocode.control.RobotSpecification;
import robocode.control.events.BattleAdaptor;
import robocode.control.events.BattleCompletedEvent;
import robocode.control.events.BattleMessageEvent;

import java.util.List;

public class RobocodeResultMapper extends BattleAdaptor {

    private List<RobotSpecification> robotSpecifications;
    private final List<RobotGenerationStore> generationStores;

    public RobocodeResultMapper(List<RobotGenerationStore> generationStores) {
        this.generationStores = generationStores;
        this.robotSpecifications = Lists.newArrayList();

        for (RobotGenerationStore store : generationStores) {
            robotSpecifications.add(store.getRobotSpecification());
        }
    }

    public List<RobotGenerationStore> getResults() {
        return generationStores;
    }

    @Override
    public void onBattleCompleted(BattleCompletedEvent e) {
        RobotResults[] robotResultList = RobotResults.convertResults(e.getSortedResults());
        for (RobotResults result : robotResultList) {
            int robotIndex = robotSpecifications.indexOf(result.getRobot());
            generationStores.get(robotIndex).setResult(result);
        }
    }

    @Override
    public void onBattleMessage(BattleMessageEvent e) {
    }
}
