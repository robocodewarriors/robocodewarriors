package nl.robocodewarriors.charles.execution;

import nl.robocodewarriors.charles.domain.RobotGenerationStore;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RobocodeResultMapperFactory {

    public RobocodeResultMapper getMapper(List<RobotGenerationStore> robotGenerationStoreList) {
        return new RobocodeResultMapper(robotGenerationStoreList);
    }
}
