package nl.robocodewarriors.charles.execution;

public class ExecutionException extends Exception {


    public ExecutionException(Throwable e) {
        super(e);
    }
}
