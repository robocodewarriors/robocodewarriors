package nl.robocodewarriors.charles.execution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import robocode.control.events.BattleAdaptor;
import robocode.control.events.BattleCompletedEvent;
import robocode.control.events.BattleErrorEvent;
import robocode.control.events.BattleMessageEvent;

public class BattleLogger extends BattleAdaptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(BattleLogger.class);
    private boolean loggingEnabled = true;

    public BattleLogger(boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
    }

    @Override
    public void onBattleCompleted(BattleCompletedEvent e) {
        if (loggingEnabled) {
            LOGGER.info("-- Battle has completed --");
        }
    }

    @Override
    public void onBattleMessage(BattleMessageEvent e) {
        if (loggingEnabled) {
            LOGGER.info("MSG> " + e.getMessage());
        }
    }

    @Override
    public void onBattleError(BattleErrorEvent e) {
        if (loggingEnabled) {
            LOGGER.info("ERR> " + e.getError());
        }
    }
}
