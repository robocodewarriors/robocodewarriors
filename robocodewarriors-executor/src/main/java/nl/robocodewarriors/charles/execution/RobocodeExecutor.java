package nl.robocodewarriors.charles.execution;


import com.google.common.collect.Lists;
import freemarker.template.TemplateException;
import nl.robocodewarriors.charles.domain.RobotGenerationStore;
import nl.robocodewarriors.charles.robotbuilder.RobotBuilder;
import nl.robocodewarriors.charles.robotbuilder.RobotCompileException;
import nl.robocodewarriors.charles.robotbuilder.RobotCompiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import robocode.control.BattleSpecification;
import robocode.control.BattlefieldSpecification;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

import java.io.IOException;
import java.util.List;

@Lazy
@Component
public class RobocodeExecutor implements Executor {

    private final RobocodeEngine robocodeEngine;
    private final RobocodeResultMapperFactory resultMapperFactory;
    private final RobotCompiler robotCompiler;
    private final RobotBuilder robotBuilder;

    public static final int FIELD_WIDTH = 800;
    public static final int FIELD_HEIGHT = 600;

    @Autowired
    public RobocodeExecutor(RobocodeEngine robocodeEngine, RobocodeResultMapperFactory resultMapperFactory, RobotCompiler robotCompiler, RobotBuilder robotBuilder) {
        this.robocodeEngine = robocodeEngine;
        this.resultMapperFactory = resultMapperFactory;
        this.robotCompiler = robotCompiler;
        this.robotBuilder = robotBuilder;
    }

    private RobocodeResultMapper startRobocodeBattle(List<String> networks, List<String> otherRobots) throws IOException, TemplateException, RobotCompileException {
        robotBuilder.buildRobotsSources(networks.toArray(new String[networks.size()]));
        robotCompiler.compileRobotProject();

        String[] robotNames = new String[networks.size() + otherRobots.size()];
        for (int i = 0; i < networks.size(); i++) {
            robotNames[i] = ("nl.robocodewarriors.robot.neuralrobot.impl.NeuralRobotImpl" + i);
        }
        for (int i = 0; i < otherRobots.size(); i++) {
            robotNames[networks.size() + i] = otherRobots.get(i);
        }

        List<RobotGenerationStore> robotGenerationStores = Lists.newArrayList();

        RobotSpecification[] selectedRobots = robocodeEngine.getLocalRepository(StringUtils.arrayToDelimitedString(robotNames, ","));
        for (int i = 0; i < selectedRobots.length; i++) {
            robotGenerationStores.add(new RobotGenerationStore(selectedRobots[i]));
        }
        RobocodeResultMapper resultMapper = resultMapperFactory.getMapper(robotGenerationStores);

        robocodeEngine.addBattleListener(resultMapper);
        BattleSpecification specification = new BattleSpecification(1, new BattlefieldSpecification(FIELD_WIDTH, FIELD_HEIGHT), selectedRobots);
        robocodeEngine.runBattle(specification, true);
        robocodeEngine.removeBattleListener(resultMapper);

        return resultMapper;
    }

    @Override
    public List<RobotGenerationStore> start(List<String> networkList, List<String> otherRobots) throws ExecutionException {
        try {
            RobocodeResultMapper resultMapper = startRobocodeBattle(networkList, otherRobots);
            return resultMapper.getResults();
        } catch (Exception e) {
            throw new ExecutionException(e);
        }
    }

    public void close() {
        robocodeEngine.close();
    }
}