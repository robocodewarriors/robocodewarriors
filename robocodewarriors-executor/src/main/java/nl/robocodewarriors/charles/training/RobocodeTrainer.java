package nl.robocodewarriors.charles.training;

import com.google.common.collect.Lists;
import nl.robocodewarriors.charles.domain.RobotGenerationStore;
import nl.robocodewarriors.charles.execution.ExecutionException;
import nl.robocodewarriors.charles.execution.RobocodeExecutor;
import nl.robocodewarriors.database.domain.ExecutionGroup;
import nl.robocodewarriors.database.domain.ExecutionGroupIndividual;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import robocode.control.RobotResults;

import java.util.ArrayList;
import java.util.List;

@Component
public class RobocodeTrainer implements Trainer<ExecutionGroupIndividual> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RobocodeTrainer.class);
    private final RobocodeExecutor robocodeExecutor;


    @Autowired
    public RobocodeTrainer(RobocodeExecutor robocodeExecutor) {
        this.robocodeExecutor = robocodeExecutor;
    }

    @Override
    public List<ExecutionGroupIndividual> handleGenerations(ExecutionGroup executionGroup) throws TrainerException {
        try {
            //Create networks from execution group
            List<String> networkList = new ArrayList<String>();
            for (ExecutionGroupIndividual executionGroupIndividual : executionGroup.getExecutionGroupIndividuals()) {
                networkList.add(executionGroupIndividual.getIndividual().getNetworkString());
            }

            //Start training.
            List<String> otherRobots = Lists.newArrayList();
            if (executionGroup.getOtherRobots() != null) {
                String[] otherRobotsArray= executionGroup.getOtherRobots().split(",");

                for (String otherRobotString : otherRobotsArray) {
                    if (otherRobotString.length() > 0) {
                        otherRobots.add(otherRobotString);
                    }
                }
            }

            List<RobotGenerationStore> robotGenerationStores = robocodeExecutor.start(networkList, otherRobots);


            for (int y = 0; y < executionGroup.getExecutionGroupIndividuals().size(); y++) {
                ExecutionGroupIndividual executionGroupIndividual = executionGroup.getExecutionGroupIndividuals().get(y);
                RobotGenerationStore robotGenerationStore = robotGenerationStores.get(y);
                RobotResults robotResult = robotGenerationStore.getResult();

                executionGroupIndividual.setBulletDamage(robotResult.getBulletDamage());
                executionGroupIndividual.setBulletDamageBonus(robotResult.getBulletDamageBonus());
                executionGroupIndividual.setRamDamage(robotResult.getRamDamage());
                executionGroupIndividual.setRamDamageBonus(robotResult.getRamDamageBonus());
                executionGroupIndividual.setLastSurvivorBonus(robotResult.getLastSurvivorBonus());
                executionGroupIndividual.setScore(robotResult.getScore());
                executionGroupIndividual.setRank(robotResult.getRank());
                executionGroupIndividual.setSurvival(robotResult.getSurvival());
            }

            return executionGroup.getExecutionGroupIndividuals();
        } catch (ExecutionException e) {
            throw new TrainerException(e);
        }
    }
}
