package nl.robocodewarriors.charles.training;

public class TrainerException extends Exception {

    public TrainerException(Throwable t) {
        super(t);
    }
}
