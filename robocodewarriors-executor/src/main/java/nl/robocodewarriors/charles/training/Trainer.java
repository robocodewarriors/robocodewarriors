package nl.robocodewarriors.charles.training;

import nl.robocodewarriors.database.domain.ExecutionGroup;

import java.util.List;

public interface Trainer<T> {

    List<T> handleGenerations(ExecutionGroup executionGroup) throws TrainerException;
}
