package nl.robocodewarriors.charles.context;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ManifestReaderTest {

    @Mock
    private ManifestFactory manifestFactory;
    @Mock
    private Enumeration<URL> manifests;
    @Mock
    private Manifest charlesManifest;
    private URL charlesUrl;
    @Mock
    private Manifest otherManifest;
    private URL otherUrl;
    @Mock
    private Attributes charlesAttributes;
    @Mock
    private Attributes otherAttributes;
    
    private ManifestReader manifestReader;
    private static int callCount = 0;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        callCount = 0;
    }

    /**
     * Test of readManifestClassPath method, of class ManifestReader.
     */
    @Test
    public void testReadManifestClassPath() throws Exception {
        manifestReader = new ManifestReader(manifests, manifestFactory);

        when(manifests.hasMoreElements()).thenReturn(true);
        when(manifests.nextElement()).thenAnswer(new Answer<URL>() {
            @Override
            public URL answer(InvocationOnMock invocation) throws Throwable {
                callCount++;
                if (callCount == 1) {
                    return otherUrl;
                } else if (callCount == 2) {
                    return charlesUrl;
                }
                throw new Exception();
            }
        });
        
        when(manifestFactory.createManifest(otherUrl)).thenReturn(otherManifest);
        when(otherManifest.getMainAttributes()).thenReturn(otherAttributes);
        when(otherAttributes.getValue(Attributes.Name.MAIN_CLASS)).thenReturn("some/other/MainClass");
        
        when(manifestFactory.createManifest(charlesUrl)).thenReturn(charlesManifest);
        when(charlesManifest.getMainAttributes()).thenReturn(charlesAttributes);
        when(charlesAttributes.getValue(Attributes.Name.MAIN_CLASS)).thenReturn("nl/robocodewarriors/charles/Executor");
        
        when(charlesAttributes.getValue(Attributes.Name.CLASS_PATH)).thenReturn("file:///some/jar.jar file:///other/jar.jar");
        
        assertEquals("some/jar.jar" + File.pathSeparator + "other/jar.jar", manifestReader.readManifestClassPath());
    }

    @Test(expected=ConfigurationException.class)
    public void testReadManifestClassPathNotFound() throws Exception {
        manifestReader = new ManifestReader(manifests, manifestFactory);

        when(manifests.hasMoreElements()).thenAnswer(new Answer<Boolean>() {

            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                callCount++;
                if(callCount == 1) {
                    return true;
                }
                
                return false;
            }
        });
        
        when(manifests.nextElement()).thenReturn(otherUrl);
        when(manifestFactory.createManifest(otherUrl)).thenReturn(otherManifest);
        when(otherManifest.getMainAttributes()).thenReturn(otherAttributes);
        when(otherAttributes.getValue(Attributes.Name.MAIN_CLASS)).thenReturn("some/other/MainClass");
               
        manifestReader.readManifestClassPath();
    }

}