package nl.robocodewarriors.charles.robotbuilder;

import freemarker.template.Configuration;
import freemarker.template.Template;
import nl.robocodewarriors.charles.context.RobotLocation;
import nl.robocodewarriors.neuralnetwork.Network;
import nl.robocodewarriors.utils.FileManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class RobotBuilderTest {

    private RobotBuilder robotBuilder;
    @Mock
    private Configuration freemarker;
    @Mock
    private FileManager fileManager;
    @Mock
    private Network networkA, networkB;
    @Mock
    private File propertiesFileA, propertiesFileB, classFileA, classFileB;
    @Mock
    private OutputStreamWriter propertiesWriterA, propertiesWriterB, classWriterA, classWriterB;
    @Mock
    private Template javaTemplate, propertiesTemplate;
    private String[] networks;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        RobotLocation robotLocation = mock(RobotLocation.class);
        when(robotLocation.getRobotLocation()).thenReturn("robotLocation");
        robotBuilder = new RobotBuilder(freemarker, fileManager, robotLocation);
        networks = new String[]{"a", "b"};
    }

    @Test
    public void testBuildRobotsSources() throws Exception {
        when(fileManager.getFile("robotLocation/src/main/resources/nl/robocodewarriors/robot/neuralrobot/impl/NeuralRobotImpl0.properties")).thenReturn(propertiesFileA);
        when(fileManager.getFile("robotLocation/src/main/resources/nl/robocodewarriors/robot/neuralrobot/impl/NeuralRobotImpl1.properties")).thenReturn(propertiesFileB);
        when(fileManager.getFile("robotLocation/src/main/java/nl/robocodewarriors/robot/neuralrobot/impl/NeuralRobotImpl0.java")).thenReturn(classFileA);
        when(fileManager.getFile("robotLocation/src/main/java/nl/robocodewarriors/robot/neuralrobot/impl/NeuralRobotImpl1.java")).thenReturn(classFileB);

        when(fileManager.getOutputStreamWriter(classFileA)).thenReturn(classWriterA);
        when(fileManager.getOutputStreamWriter(classFileB)).thenReturn(classWriterB);
        when(fileManager.getOutputStreamWriter(propertiesFileA)).thenReturn(propertiesWriterA);
        when(fileManager.getOutputStreamWriter(propertiesFileB)).thenReturn(propertiesWriterB);

        when(freemarker.getTemplate("templates/NeuralRobotImpl.java.ftl")).thenReturn(javaTemplate);
        when(freemarker.getTemplate("templates/NeuralRobotImpl.properties.ftl")).thenReturn(propertiesTemplate);

        robotBuilder.buildRobotsSources(networks);

        verify(fileManager).createDirectories(propertiesFileA);
        verify(fileManager).createDirectories(propertiesFileB);
        verify(fileManager).createDirectories(classFileA);
        verify(fileManager).createDirectories(classFileB);

        verify(propertiesFileA).createNewFile();
        verify(propertiesFileB).createNewFile();
        verify(classFileA).createNewFile();
        verify(classFileB).createNewFile();

        Map<String, Object> resultA = new HashMap<String, Object>();
        resultA.put("identifier", 0);
        resultA.put("json", "a");
        Map<String, Object> resultB = new HashMap<String, Object>();
        resultB.put("identifier", 1);
        resultB.put("json", "b");

        verify(javaTemplate).process(eq(resultA), eq(classWriterA));
        verify(javaTemplate).process(eq(resultB), eq(classWriterB));
        verify(propertiesTemplate).process(eq(resultA), eq(propertiesWriterA));
        verify(propertiesTemplate).process(eq(resultB), eq(propertiesWriterB));
    }
}
