package nl.robocodewarriors.charles.robotbuilder;

import nl.robocodewarriors.charles.context.RobotLocation;
import nl.robocodewarriors.utils.FileManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RobotCompilerTest {

    @Mock
    private File executionDir;
    @Mock
    private FileManager fileManager;
    @Mock
    private Runtime runtime;
    @Mock
    private File robotLocationFile;
    @Mock
    private Process process;
    @Mock
    private File mavenCreatedFile;
    private RobotCompiler instance;
    @Mock
    private File resultFile;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RobotLocation robotLocation = mock(RobotLocation.class);
        when(robotLocation.getRobotLocation()).thenReturn("robotlocation");
        instance = new RobotCompiler(executionDir, robotLocation, fileManager, runtime);
    }

    @Test
    public void compileRobotProjectTest() throws IOException, RobotCompileException {
        InputStream inputStream = new ByteArrayInputStream("BUILD SUCCESS".getBytes());

        when(fileManager.getFile("robotlocation")).thenReturn(robotLocationFile);

        when(runtime.exec(argThat(new ArgumentMatcher<String[]>() {
            @Override
            public boolean matches(Object argument) {
                String[] args = (String[]) argument;
                return (args[0].startsWith("mvn") && args[1].equals("install") && args[2].equals("-DskipTests"));
            }
        }), isNull(String[].class), eq(robotLocationFile))).thenReturn(process);

        when(process.getInputStream()).thenReturn(inputStream);
        when(fileManager.getFile("robotlocation/target/neural-robot.jar")).thenReturn(mavenCreatedFile);
        when(fileManager.getFile(executionDir, "robots" + File.separator + "neural-robot.jar")).thenReturn(resultFile);
        when(fileManager.move(mavenCreatedFile, resultFile)).thenReturn(resultFile);

        assertEquals(resultFile, instance.compileRobotProject());
        verify(fileManager).move(mavenCreatedFile, resultFile);
    }

    @Test(expected = RobotCompileException.class)
    public void compileRobotProjectBuildFailureTest() throws IOException, RobotCompileException {
        InputStream inputStream = new ByteArrayInputStream("BUILD FAILURE".getBytes());

        when(fileManager.getFile("robotlocation")).thenReturn(robotLocationFile);

        when(runtime.exec(argThat(new ArgumentMatcher<String[]>() {
            @Override
            public boolean matches(Object argument) {
                String[] args = (String[]) argument;
                return (args[0].startsWith("mvn") && args[1].equals("install") && args[2].equals("-DskipTests"));
            }
        }), isNull(String[].class), eq(robotLocationFile))).thenReturn(process);

        when(process.getInputStream()).thenReturn(inputStream);

        instance.compileRobotProject();
    }
}
