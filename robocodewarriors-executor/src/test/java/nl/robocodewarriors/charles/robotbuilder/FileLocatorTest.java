package nl.robocodewarriors.charles.robotbuilder;


import nl.robocodewarriors.utils.FileManager;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class FileLocatorTest {

    @Test
    public void testGetFile() {
        FileManager locator = new FileManager();
        File file = locator.getFile("location");
        assertThat(file.getPath(), is("location"));
    }
}
