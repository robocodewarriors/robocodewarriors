package nl.robocodewarriors.charles.training;

import nl.robocodewarriors.charles.execution.ExecutionException;
import nl.robocodewarriors.charles.execution.RobocodeExecutor;
import nl.robocodewarriors.database.repository.GenerationRepository;
import nl.robocodewarriors.database.repository.TrainingRepository;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class RobocodeTrainerTest {

    private RobocodeExecutor robocodeExecutor;
    private RobocodeTrainer trainer;
    private TrainingRepository trainingRepository;

    private GenerationRepository generationRepository;

    @Before
    public void before() {
        robocodeExecutor = mock(RobocodeExecutor.class);
        trainingRepository = mock(TrainingRepository.class);
        generationRepository = mock(GenerationRepository.class);
//        trainer = new RobocodeTrainer(robocodeExecutor, converter, trainingRepository, generationRepository, networkRepository, networkSerializer);
    }

    //    @Test(expected = TrainerException.class)
    public void testTrainerException() throws ExecutionException, TrainerException {
//        List<Network> networks = Lists.newArrayList(mock(Network.class), mock(Network.class));
//        when(robocodeExecutor.start(networks, isA(ScoreCalculator.class))).thenThrow(ExecutionException.class);
//        trainer.handleGenerations(null, networks);
    }

    @Test
    public void testHandleGenerations() throws ExecutionException, TrainerException {
//        List<Dna> originalDna1 = Lists.newArrayList(mock(Dna.class), mock(Dna.class));
//        List<Dna> newDna1 = Lists.newArrayList(mock(Dna.class), mock(Dna.class));
//        List<Dna> originalDna2 = Lists.newArrayList(mock(Dna.class), mock(Dna.class));
//        List<Dna> newDna2 = Lists.newArrayList(mock(Dna.class), mock(Dna.class));
//        List<Dna> originalDna3 = Lists.newArrayList(mock(Dna.class), mock(Dna.class));
//        List<Dna> newDna3 = Lists.newArrayList(mock(Dna.class), mock(Dna.class));
//
//        List<Network> networks1 = Lists.newArrayList(mock(Network.class), mock(Network.class));
//        List<Network> networks2 = Lists.newArrayList(mock(Network.class), mock(Network.class));
//        List<Network> networks3 = Lists.newArrayList(mock(Network.class), mock(Network.class));
//        List<Network> networks4 = Lists.newArrayList(mock(Network.class), mock(Network.class));
//
//        List<NetworkScore> networkScores1 = Lists.newArrayList(mock(NetworkScore.class), mock(NetworkScore.class));
//        List<NetworkScore> networkScores2 = Lists.newArrayList(mock(NetworkScore.class), mock(NetworkScore.class));
//        List<NetworkScore> networkScores3 = Lists.newArrayList(mock(NetworkScore.class), mock(NetworkScore.class));
//
//        when(robocodeExecutor.start(networks1, isA(ScoreCalculator.class))).thenReturn(networkScores1);
//        when(converter.convertToDna(networkScores1)).thenReturn(originalDna1);
//        when(converter.convertToNetwork(newDna1)).thenReturn(networks2);
//
//        when(robocodeExecutor.start(networks2, isA(ScoreCalculator.class))).thenReturn(networkScores2);
//        when(converter.convertToDna(networkScores2)).thenReturn(originalDna2);
//        when(converter.convertToNetwork(newDna2)).thenReturn(networks3);
//
//        when(robocodeExecutor.start(networks3, isA(ScoreCalculator.class))).thenReturn(networkScores3);
//        when(converter.convertToDna(networkScores3)).thenReturn(originalDna3);
//        when(converter.convertToNetwork(newDna3)).thenReturn(networks4);
//
//        List<Network> result = trainer.handleGenerations(null, networks1);
//
//        verify(robocodeExecutor, times(1)).start(networks1, isA(ScoreCalculator.class));
//        verify(robocodeExecutor, times(1)).start(networks2, isA(ScoreCalculator.class));
//        verify(robocodeExecutor, times(1)).start(networks3, isA(ScoreCalculator.class));
//        verify(robocodeExecutor, never()).start(networks4, isA(ScoreCalculator.class));
//
//        assertThat(result, is(networks4));

    }
}
