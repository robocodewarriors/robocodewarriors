package nl.robocodewarriors.charles.execution;


import com.google.common.collect.Lists;
import nl.robocodewarriors.charles.domain.RobotGenerationStore;
import nl.robocodewarriors.charles.robotbuilder.RobotBuilder;
import nl.robocodewarriors.charles.robotbuilder.RobotCompiler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import robocode.control.RobocodeEngine;
import robocode.control.RobotSpecification;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class RobocodeExecutorTest {


    private RobocodeExecutor robocodeExecutor;
    private List<String> networks;
    private RobotSpecification[] specifications;

    @Mock
    private RobocodeEngine robocodeEngine;
    @Mock
    private RobocodeResultMapperFactory robocodeResultMapperFactory;
    @Mock
    private RobotCompiler robotCompiler;

    @Mock
    private RobotBuilder robotBuilder;

    @Mock
    private RobotSpecification specificationA, specificationB;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        robocodeExecutor = new RobocodeExecutor(robocodeEngine, robocodeResultMapperFactory, robotCompiler, robotBuilder);

        specifications = new RobotSpecification[]{specificationA, specificationB};
        networks = new ArrayList<String>();
        networks.add("a");
        networks.add("b");
    }

    @Test
    public void testStartRobocodeBattle() throws Exception {
        RobocodeResultMapper mapper = mock(RobocodeResultMapper.class);
        when(robocodeResultMapperFactory.getMapper(isA(List.class))).thenReturn(mapper);
        List<RobotGenerationStore> resultsList = Lists.newArrayList(mock(RobotGenerationStore.class), mock(RobotGenerationStore.class));
        when(mapper.getResults()).thenReturn(resultsList);
        when(robocodeEngine.getLocalRepository(anyString())).thenReturn(specifications);

        List<RobotGenerationStore> result = robocodeExecutor.start(networks, Lists.newArrayList("other"));
        assertThat(result, is(resultsList));
    }
}
