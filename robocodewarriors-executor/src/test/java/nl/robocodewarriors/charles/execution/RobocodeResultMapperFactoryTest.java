package nl.robocodewarriors.charles.execution;

import com.google.common.collect.Lists;

import nl.robocodewarriors.charles.domain.RobotGenerationStore;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class RobocodeResultMapperFactoryTest {

    @Test
    public void testGetMapper() {
        List<RobotGenerationStore> stores = Lists.newArrayList(mock(RobotGenerationStore.class), mock(RobotGenerationStore.class));

        RobocodeResultMapper result = new RobocodeResultMapperFactory().getMapper(stores);
        assertThat(result, is(notNullValue()));
    }
}
