package nl.robocodewarriors.charles.domain;

import nl.robocodewarriors.neuralnetwork.Network;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class NetworkScoreTest {

    @Test
    public void testConstructor() {
        Network network = mock(Network.class);
        Integer score = 50;
        NetworkScore networkScore = new NetworkScore(network, score);
        assertThat(networkScore.getNetwork(),is(network));
        assertThat(networkScore.getScore(),is(score));
    }
}
